.PHONY: tesi abstract-tesi slides-tesi slides-colloquio


all: tesi abstract-tesi slides-tesi slides-colloquio

tesi:
	make -C tesi

abstract-tesi:
	make -C abstract-tesi

slides-tesi:
	make -C slides-tesi

slides-colloquio:
	make -C slides-colloquio


clean: clean_tesi clean_abstract-tesi clean_slides-tesi clean_slides-colloquio

clean_tesi:
	make -C tesi clean

clean_abstract-tesi:
	make -C abstract-tesi clean

clean_slides-tesi:
	make -C slides-tesi clean

clean_slides-colloquio:
	make -C slides-colloquio clean
