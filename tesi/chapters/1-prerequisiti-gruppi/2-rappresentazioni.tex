Questa sezione tratta principalmente di rappresentazioni di gruppi.
Inizialmente definiremo un concetto più generale che è quello di $G$-spazio, il quale non è strettamente necessario per i risultati
che andremo qui ad enunciare, ma tornerà utile per la successiva trattazione dello spazio quoziente e per altre future
proposizioni e dimostrazioni.
Mostreremo come con le rappresentazioni si riescono a estrapolare alcune proprietà di un gruppo algebrico lineare,
fra cui l'importante teorema che stabilisce un isomorfismo fra tale gruppo e un sottogruppo chiuso di matrici
e la decomposizione di Jordan nella quale ogni elemento del gruppo viene visto come prodotto di un elemento semi-semplice
e di uno unipotente, esattamente come per le matrici.

% 2.3.1
\begin{definition}[$G$-spazio]\label{gspazio}{\ \\}
Un \textit{$G$-spazio}, detto anche \textit{$G$-varietà}, è una varietà $X$ sulla quale è definita un'azione sinistra di $G$,
dove tale azione è un morfismo tra le varietà $G \times X$ e $X$.
\end{definition}

Un $G$-spazio $X$ si dice \textit{omogeneo} per $G$ se la relativa azione è transitiva.

Dati due $G$-spazi $X$ e $Y$ e un morfismo $\phi : X \to Y$, questo si dice \textit{equivariante} se
$\phi(g.x) = g.\phi(x)$ per ogni $g \in G, x \in X$.

\medskip
Utilizzando alcuni fatti topologici legati ai morfismi e applicati all'azione di $G$ su $X$ si riesce a mostrare il seguente:

% 2.3.3
\begin{theorem}\label{OrbOpen}
Sia $X$ un $G$-spazio e sia $x \in X$, allora l'orbita $G.x$ è aperta nella sua chiusura.
\end{theorem}

\medskip
Concentriamoci da ora in poi solo sulle rappresentazioni.

% 2.3.2
\begin{definition}[Rappresentazione e $G$-Modulo]\label{gmodulo}{\ \\}
Sia $M$ un $k$-spazio vettoriale su cui è definita un'azione sinistra di $G$ costituita da mappe lineari.
Diciamo che $M$ è un \textit{$G$-modulo razionale}, o semplicemente un \textit{$G$-modulo}, se per ogni $m \in M$
esiste un sottospazio vettoriale $M' \subset M$ di dimensione finita stabile per l'azione di $G$ e contenente $m$,
sul quale l'azione ristretta $G \times M' \to M'$ è un morfismo di varietà.

Chiamiamo \textit{rappresentazione razionale} di $G$ su $M$, o semplicemente \textit{rappresentazione},
l'azione associata a un qualche $G$-modulo.
Nel caso $M$ sia finito, le rappresentazioni di $G$ su $M$ si possono identificare
con gli omomorfismi di gruppi algebrici $G \to \GLb(M)$.
\end{definition}

Notiamo inoltre che l'azione $r : G \times M \to M$ induce un'azione sullo spazio proiettivo $\Pb(M)$,
dotando quest'ultimo di una struttura di $G$-spazio.

\medskip
Supponiamo per il resto della sezione che $G$ sia un gruppo algebrico lineare.

% 2.3.5
\subsection\label{coaction}
Sia $X$ un $G$-spazio affine, con un'azione $a: G \times X \to X$.
In modo simile a quanto visto per la comoltiplicazione, l'azione $a$ induce un omomorfismo di $k$-algebre
$a^* : k[X] \to k[G] \otimes k[X]$.
Definiamo $s : G \times k[X] \to k[X]$ come
\[
	s(g)(f)(x) = f(g^{-1}x) \qquad (g \in G, x \in X, f \in k[X])
\]
in modo che, se $a^*(f) = \sum_{i=1}^n u_i \otimes f_i$, allora
\[
	s(g)(f) = \sum_{i=1}^n u_i(g^{-1}) f_i \qquad (g \in G, f \in k[X])
\]
Dalla definizione vediamo che $s(g)$ è una mappa lineare e invertibile su $k[X]$, quindi una rappresentazione
di gruppi astratti (senza richiedere la proprietà di morfismo) di $G$ su $k[X]$.

\medskip
Il prossimo risultato mostra che $k[X]$ è un $G$-modulo, quindi $s$ si può costruire come unione di rappresentazioni finite.
La dimostrazione si basa sul fatto che $s(\cdot)(f)$ è somma di un numero finito di elementi di $u_i \otimes f_i \in k[G] \otimes k[X]$
e lo span dei vari $f_i$ andrà a formare un sottospazio di dimensione finita dentro cui trovarne uno $G$-stabile.

% 2.3.6
\begin{proposition}\label{GlocfinkX}
Se $V$ è un sottospazio di $k[X]$ di dimensione finita, allora esiste un sottospazio $W$ di $k[X]$
di dimensione finita contenente $V$ e stabile per l'azione di ogni $s(g)$ ($g \in G$).
\end{proposition}


\subsection
Nel caso particolare in cui $X = G$, denotiamo con $\lambda$ la mappa $s$ e notiamo che agisce per traslazione sinistra:
\[
	\lambda(g)(f)(x) = f(g^{-1}x) \qquad (g, x \in G, f \in k[G])
\]
In modo simile definiamo la mappa $\rho : G \times k[G] \to k[G]$ che agisce per traslazione destra:
\[
	\rho(g)(f)(x) = f(xg) \qquad (g, x \in G, f \in k[G])
\]
I risultati trovati per $s$, quindi per $\lambda$, valgono in modo simmetrico anche per $\rho$,
quindi $\lambda$ e $\rho$ sono rappresentazioni di $G$ su $k[G]$.

\medskip
Notando che $k[G]$ è generato da un numero finito di elementi, si può trovare un sottospazio finito
stabile per l'azione di $\rho$ che li contiene tutti. Questa idea conduce al seguente teorema:

% 2.3.7
\begin{theorem}\label{GisoSubGLn}
Esiste un isomorfismo di $G$ su un sottogruppo chiuso di $\GLb_n$ per qualche $n$.
\end{theorem}

\medskip
Introduciamo ora i concetti di semi-semplice e unipotente e vediamo come la decomposizione di Jordan,
già nota per le matrici, si applica nel caso dei gruppi.

% 2.4.1
\subsection
Sia $V$ un $k$-spazio vettoriale di dimensione finita.
Un endomorfismo $a$ di $V$ si dice \textit{semi-semplice} se esiste una base di $V$ costituita
da autovettori di $a$, cioè se $a$, rappresentato come matrice, è diagonalizzabile.
Un endomorfismo $a$ di $V$ si dice \textit{nilpotente} se $a^s = 0$ per qualche $s \ge 1$
e si dice \textit{unipotente} se $a-\Id$ è nilpotente.

% 2.4.5
\begin{proposition}[Decomposizione di Jordan moltiplicativa]{\ \\}
Dato $a \in GL(V)$, esistono unici $a_s, a_u \in GL(V)$ tali che $a_s$ è semi-semplice,
$a_u$ è unipotente e $a = a_s a_u = a_u a_s$.
\end{proposition}

% 2.4.7
\subsection
Sia $V$ un $k$-spazio vettoriale, di dimensione non necessariamente finita,
e sia $a \in End(V)$ un suo endomorfismo.
Diciamo che $a$ è \textit{localmente finito} se $V$ è unione di sottospazi di dimensione finita
stabili per l'azione di $a$.
Diciamo che $a$ è \textit{semi-semplice} se la sua restrizione ad ogni sottospazio stabile per
la sua azione è semi-semplice, ed è \textit{localmente nilpotente} se tale restrizione è nilpotente.
Infine diciamo che $a$ è \textit{localmente unipotente} se $a - \Id$ è localmente nilpotente.
Notiamo che queste definizioni sono compatibili con quelle fornite per spazi di dimensione finita.

Se $a \in GL(V)$ è localmente finito, riconducendoci al caso finito-dimensionale, possiamo trovare univocamente
$a_s, a_u \in GL(V)$ tali che $a_s$ è semi-semplice, $a_u$ è localmente unipotente e $a = a_s a_u = a_u a_s$.

Per quanto appena visto, la traslazione destra $\rho(g)$ è un elemento localmente finito di $GL(k[G])$ per ogni $g \in G$,
quindi esiste una sua decomposizione di Jordan $\rho(g) = \rho(g)_s \rho(g)_u$.

% 2.4.8
\begin{theorem}[Decomposizione di Jordan per gruppi]{\ }
\begin{enumerate}[(i)]
	\item Per ogni $g \in G$ esistono unici $g_s, g_u \in G$ tali che $\rho(g_s) = \rho(g)_s$, $\rho(g_u) = \rho(g)_u$
		e $g = g_s g_u = g_u g_s$;
	\item Se $\phi : G \to G'$ è un omomorfismo di gruppi algebrici, allora $\phi(g_s) = \phi(g)_s$ e $\phi(g_u) = \phi(g)_u$;
	\item Se $G = \GLb_n$, allora $g_s$ e $g_u$ sono le parti semi-semplici e unipotenti della decomposizione di Jordan moltiplicativa di $g$.
\end{enumerate}
\end{theorem}

\begin{definition}[Elemento Semi-Semplice e Unipotente]{\ \\}
Sia $G$ un gruppo algebrico lineare.
Diciamo che un elemento $g \in G$ è \textit{semi-semplice} se $g = g_s$ e che è \textit{unipotente} se $g = g_u$.
\end{definition}

% 2.4.9
\begin{corollary}\label{SsUniIffRappr}
Un elemento $g \in G$ è semi-semplice se e solo se per ogni isomorfismo $\phi$ di $G$ su un sottogruppo chiuso di qualche $\GLb_n$
si ha che $\phi(g)$ è semi-semplice.
Allo stesso modo $g$ è unipotente se e solo se lo è $\phi(g)$ per ogni $\phi$.
\end{corollary}

\begin{definition}[Gruppo Unipotente]{\ \\}
Sia $G$ un gruppo algebrico lineare.
Diciamo che $G$ è \textit{unipotente} se costituito da elementi unipotenti.
\end{definition}

% 2.4.12
\begin{theorem}\label{MatUniconjUn}
Sia $G$ un sottogruppo di $\GLb_n$ costituito da matrici unipotenti, allora esiste $x \in \GLb_n$ tale che $xGx^{-1} \subset \Ub_n$.
\end{theorem}

\medskip
Diamo ora un'ultima definizione, quella di gruppo nilpotente.
Dati due elementi $x,y$ di un gruppo, sia $(x,y) = xyx^{-1}y^{-1}$ il loro commutatore.

\begin{definition}[Gruppo Nilpotente]{\ \\}
Un gruppo $H$ si dice \textit{nilpotente} se esiste un intero $n$ per cui, per ogni $n$-upla di elementi
$x_1, \dots, x_n \in H$, i loro commutatori iterati sono banali, cioè si ha
$(x_1, (x_2, \dots (x_{n-2},(x_{n-1},x_n)) \dots )) = e$.
\end{definition}

% 2.4.13
\begin{corollary}\label{UniNilRis}
Un gruppo algebrico lineare unipotente è nilpotente, quindi risolubile.
\end{corollary}
