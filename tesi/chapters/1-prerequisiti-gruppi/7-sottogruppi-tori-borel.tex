Questa sezione e ancora di più le successive segnano un primo punto di svolta nella
teoria dei gruppi algebrici lineari che abbiamo presentato fino a questo momento.
D'ora in poi ci focalizzeremo sui gruppi nella loro interezza e utilizzeremo gli strumenti
introdotti per proseguire nel loro studio.

In questa sezione presenteremo i tori massimali e i sottogruppi di Borel, enunceremo
alcune loro proprietà ed infine presenteremo il radicale e il radicale unipotente di un gruppo.

\medskip
Sia $G$ un gruppo algebrico lineare connesso.

\begin{definition}[Sottogruppo di Borel]{\ \\}
Un \textit{sottogruppo di Borel} di $G$ è un sottogruppo di $G$ chiuso, connesso, risolubile e massimale rispetto queste tre proprietà.
\end{definition}

Un sottogruppo di Borel di $G$ esiste sempre, infatti l'insieme dei sottogruppi chiusi, risolubili e massimali è non vuoto,
dato che contiene $\{e\} < G$, e ammette un massimo per ragioni di dimensione.

Un sottogruppo di Borel non è unico, tuttavia il prossimo teorema ci mostra come la scelta di un particolare sottogruppo
non comporti una perdita di generalità.

% 6.2.7
\begin{theorem}
Due sottogruppi di Borel di $G$ sono coniugati.
\end{theorem}

\medskip
Il prossimo è un lemma sui gruppi risolubili che verrà utilizzato in una successiva dimostrazione.
Pur non essendo utile all'attuale sviluppo della teoria e apparentemente estraneo agli argomenti
trattati in questa sezione, viene posto qui per alcuni legami più profondi che non espliciteremo.

Si può mostrare che se $G$ è un gruppo risolubile, allora l'insieme degli elementi unipotenti,
indicato con $G_u$, costituisce un sottogruppo chiuso, connesso e normale.

% 6.3.4
\begin{lemma}\label{GainGu}
Supponiamo $G$ sia risolubile.
Se $G$ non è un toro, allora esiste un sottogruppo $N$ di $G$ chiuso e normale, isomorfo a $\Gb_a$ e contenuto nel centro di $G_u$.
\end{lemma}

\medskip
Introduciamo ora la nozione di toro massimale.

\begin{definition}[Toro massimale]{\ \\}
Un sottogruppo $T$ di $G$ è un \textit{toro massimale} se è un toro e non è strettamente contenuto in nessun altro sottogruppo di $G$ che sia un toro.
\end{definition}

Il seguente teorema è un analogo a quello riguardante i sottogruppi di Borel.

% 6.3.5 + 6.4.1
\begin{theorem}\label{Textconj}{\ }
\begin{enumerate}[(i)]
	\item Esiste un toro massimale;
	\item Due tori massimali di $G$ sono coniugati.
\end{enumerate}
\end{theorem}

Essendo un toro chiuso, connesso e risolubile, è sempre contenuto in un sottogruppo di Borel.
Il prossimo teorema mostra che si può scrivere tale sottogruppo di Borel come prodotto del toro massimale
e di una parte unipotente.

% 6.3.5
\begin{theorem}\label{TGuisoG}
Se $G$ è risolubile e $T$ è un toro massimale, allora la mappa $T \times G_u \to G$ indotta dal prodotto
è un isomorfismo di varietà.
In particolare, se $G$ è un gruppo algebrico lineare, $B$ è un suo sottogruppo di Borel
e $B_u$ è la relativa parte unipotente, il prodotto $T \times B_u \to B$ è un isomorfismo di varietà.
\end{theorem}

\medskip
I seguenti risultati descrivono l'azione per coniugio di gruppo sui suoi sottogruppi di Borel e tori massimali.

% 6.4.7
\begin{theorem}\label{ZGScapB}
Sia $S$ un toro contenuto in $G$.
\begin{enumerate}[(i)]
	\item Il centralizzatore $Z_G(S)$ è connesso;
	\item Dato un sottogruppo di Borel $B$ contenente $S$, $Z_G(S) \cap B$ è un sottogruppo di Borel di $Z_G(S)$.
		Inoltre ogni sottogruppo di Borel di $Z_G(S)$ può essere ottenuto in questo modo.
\end{enumerate}
\end{theorem}

% 6.4.9
\begin{theorem}
Sia $B$ un sottogruppo di Borel di $G$, allora $N_G(B) = B$.
\end{theorem}

% 6.4.12
\begin{corollary}\label{NTmodZTtoBT}
Sia $T$ un toro massimale di $G$ e $B$ un sottogruppo di Borel di $G$ contenente $T$.
La mappa $x \mapsto xQx^{-1}$ induce una bigezione di $N_G(T)/Z_G(T)$ sull'insieme
dei sottogruppi di Borel contenenti $T$.
\end{corollary}

\medskip
Introduciamo ora le nozioni di radicale, radicale unipotente, gruppo semi-semplice e gruppo riduttivo.

% 6.4.14
\subsection
Ricordiamo che, se $N$ e $N'$ sono due sottogruppi normali di $G$, allora $N.N'$ è a sua volta normale.
In modo simile, per~\ref{GroupProd}, se $N$ e $N'$ sono chiusi e connessi, allora anche $N.N'$ è chiuso e connesso.
Infine, se $N$ e $N'$ sono normali e risolubili, anche $N.N'$ è risolubile:
infatti $G$ è risolubile se e solo se lo sono sia $N$ che $G/N$
(questo è un fatto generale vero per qualsiasi gruppo $G$ e sottogruppo normale $N$),
quindi ci basta mostrare che $N.N'/N$ è risolubile;
ma $N.N'/N \simeq N'/(N \cap N')$, dove $N \cap N'$ è normale in $N'$, e, sempre per il precedente fatto,
dato che $N'$ è risolubile, anche $N'/(N \cap N')$ è risolubile.

Segue che esiste un unico sottogruppo di $G$ chiuso, connesso, normale e risolubile, massimale per queste quattro proprietà.
Chiamiamo tale sottogruppo \textit{radicale} di $G$ e indichiamolo con $R(G)$.

\subsection
Dato un sottogruppo di $G$ chiuso, connesso, normale e unipotente, per~\ref{UniNilRis} è anche risolubile, quindi è contenuto in $R(G)$.
D'altro canto di può verificare come l'insieme $R(G)_u$ degli elementi unipotenti di $R(G)$ formi un gruppo chiuso e connesso.
Tale gruppo è anche normale in quanto i coniugati di elementi unipotenti sono unipotenti e $R(G)$ è normale.

Segue che esiste un unico sottogruppo di $G$ chiuso, connesso, normale e unipotente, massimale per queste quattro proprietà,
che coincide con $R(G)_u$.
Chiamiamo tale sottogruppo \textit{radicale unipotente} e indichiamolo con $R_u(G)$.

\begin{definition}[Gruppo semi-semplice e riduttivo]{\ \\}
Sia $G$ un gruppo algebrico lineare.
Diciamo che $G$ è \textit{semi-semplice} se $R(G) = \{e\}$ ed che è \textit{riduttivo} se $R_u(G) = \{e\}$.
\end{definition}
