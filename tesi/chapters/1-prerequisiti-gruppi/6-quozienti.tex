In questa sezione parleremo di quozienti di gruppi algebrici lineari, in particolare dato un gruppo $G$ e un suo sottogruppo $H$
vogliamo costruire il quoziente $G/H$ in modo che abbia anche la struttura di varietà algebrica quasi-proiettiva.
Illustreremo i principali passaggi di tale costruzione, infine mostreremo risultati analoghi per spazi più generali.
Gli argomenti trattati sono necessari principalmente per la costruzione dei rappresentanti alternativi nel teorema di Borel-Weil,
la quale viene illustrata alla fine dell'ultimo capitolo, ma verranno ripresi anche nel terzo e nel quarto capitolo,
dove saranno utili per svolgere alcune dimostrazioni.

\medskip
Siano $G$ un gruppo algebrico lineare e $H$ un suo sottogruppo chiuso, con le rispettive algebre di Lie $\gf$ e $\hf$.

Iniziamo definendo in modo astratto lo spazio quoziente.

\begin{definition}\label{quoz}
Un \textit{quoziente} di $G$ rispetto $H$ è una coppia $(G/H,a)$ consistente in uno spazio $G/H$ omogeneo per $G$
e un punto $a \in G/H$ il cui stabilizzatore contiene $H$ tale che valga la seguente proprietà universale:
per ogni coppia $(Y,b)$ di un $G$-spazio $Y$ e un punto $b \in Y$ il cui stabilizzatore contiene $H$,
esiste un unico morfismo $G$-equivariante $\phi : G/H \to Y$ tale che $\phi(a) = b$.
\end{definition}

Notiamo che l'unicità di uno spazio quoziente si può ottenere dalla proprietà universale con un argomento categoriale.
Costruiamo esplicitamente un modello di spazio quoziente per mostrarne l'esistenza.

\begin{lemma}
Esiste un sottospazio $V$ di $k[G]$ di dimensione finita e un sottospazio $W$ di $V$ tali che
\begin{enumerate}[(a)]
	\item V è stabile rispetto tutte le azioni destre $\rho(x)$ ($x \in G$);
	\item $H = \{ x \in G \mid \rho(x)W = W\}$;
	\item $\hf = \{ X \in \gf \mid X.W \subset W\}$.
\end{enumerate}
\end{lemma}

La dimostrazione di questo fatto si basa sul costruire in modo semi-esplicito gli spazi $V$ e $W$:
detto $I$ l'ideale di $k[G]$ associato al chiuso $H$, consideriamo un insieme finito di generatori di $I$;
a questo punto ci basta prendere come $V$ un sottospazio di $k[G]$ di dimensione finita stabile per l'azione di $\rho$
e contenente i generatori di $I$ come in~\ref{GlocfinkX} e porre $W = V \cap I$.

\medskip
Ottenuti i due sottospazi $V$ e $W$ come nel precedente lemma, sia $d = \dim(W)$.
Vorremmo ricondurci al caso di dimensione $1$, per questo motivo in consideriamo il prodotto esterno $\bigwedge^d V$
e il suo sottospazio $1$-dimensionale $L = \bigwedge^d W$.

Sia $\phi$ la rappresentazione canonica di $GL(V)$ su $\bigwedge^d V$.

\begin{lemma}{\ }
\begin{enumerate}[(i)]
	\item Sia $x \in GL(V)$. Allora $x.W = W$ se e solo se $\phi(x).L = L$;
	\item sia $X \in \glf(V)$. Allora $X.W \subset W$ se e solo se $d\phi(X).L \subset L$.
\end{enumerate}
\end{lemma}

Mettendo assieme i due risultati troviamo il seguente:

\begin{theorem}\label{RapprStab}
Esiste una rappresentazione razionale $\phi : G \to GL(V)$ e un vettore $v \in V$ non nullo tali che
\begin{align*}
	H &= \{x \in G \mid \phi(x)(v) \in kv\}\\
	\hf &= \{X \in \gf \mid d\phi(X)(v) \in kv\}
\end{align*}
\end{theorem}

La rappresentazione $\phi$ induce un'azione di $G$ su $\Pb(V)$.
Restringendo lo spazio all'orbita di $[v]$ si arriva al seguente risultato:

\begin{corollary}
Esiste una varietà $X$ quasi proiettiva omogenea per $G$ e un punto $x \in X$ tali che:
\begin{enumerate}[(a)]
	\item lo stabilizzatore di $x$ in $G$ è $H$;
	\item le fibre di $\psi$ sono le classi laterali $gH$ ($g \in G$).
\end{enumerate}
\end{corollary}

Lo spazio $(X,x)$ è il modello di spazio quoziente che stavamo cercando,
quindi rimane solo da verificare che soddisfi la proprietà universale.

Per quest'ultima parte, si considera il quoziente di gruppi astratti $G/H$ e si definisce su di esso il fascio di funzioni $\Oc_{G/H}$ indotto da $\Oc_G$.
Per costruzione tale spazio rispetta la proprietà universale, ad eccezione del fatto che non ha ancora la struttura di varietà algebrica.
Si conclude esibendo un isomorfismo $G$-equivariante di spazi anellati $\phi : G/H \to X$, in modo da rendere il quoziente costruito una varietà.

In questo modo si giunge al seguente teorema:

\begin{theorem}\label{GquozHexun}
Un quoziente $(G/H,a)$ esiste ed è unico a meno di $G$-isomorfismo.
La coppia $(X,x)$ costruita nel precedente corollario costituisce un rappresentante di tale quoziente.
\end{theorem}

\medskip
Supponiamo ora che $H$ sia anche normale come sottogruppo.
In questo caso si può definire il quoziente $G/H$ sia come varietà che come gruppo astratto,
e la seguente proposizione mostra come le due strutture sono compatibili.

\begin{proposition}\label{GquozHNormGAL}
Sia $G$ un gruppo algebrico lineare e sia $H$ un sottogruppo chiuso e normale di $G$,
allora $G/H$ è un gruppo algebrico lineare con la struttura di gruppo indotta da $G$.
\end{proposition}

\subsection\label{GquozHLie}
Nella costruzione del quoziente abbiamo richiesto $\hf = \{X \in \gf \mid d\phi(X)(v) \in kv\}$:
questa ipotesi ci dà un'importante condizione sul tangente di $G/H$, cioè $T_a (G/H) \simeq \gf / \hf$.
Nel caso in cui $H$ è un sottogruppo normale, si trova $L(G/H) \simeq \gf / \hf = L(G) / L(H)$.

\medskip
Trattiamo ora un caso più generale: sia $H$ un gruppo algebrico lineare e $X$ un $H$-spazio irriducibile,
vogliamo definire il quoziente $X/H$.
Come accennato nella precedente dimostrazione, possiamo costruire uno spazio anellato $(X/H, \Oc_{X/H})$
e una mappa $\phi : X \to X/H$ continua di spazi anellati.
Diciamo che il quoziente di $X$ rispetto ad $H$ esiste ed è $X/H$ se $\phi$ è una mappa aperta e $X/H$
ha la struttura di varietà algebrica.

\subsection\label{locsec}
La seguente condizione implica l'esistenza dello spazio quoziente in alcuni casi particolari.
Sia $G$ un gruppo algebrico lineare e $H$ un suo sottogruppo chiuso, e sia $\pi : G \to G/H$ la proiezione al quoziente.
Dato un aperto $U$ di $G/H$, definiamo una sezione di $\pi$ su $U$ come un morfismo $\sigma : U \to G$
tale che $\pi \circ \sigma = \Id_U$.
Diciamo che $\pi$ ammette sezioni locali se esiste un ricoprimento aperto finito di $G/H$ tale che su ognuno degli aperti
esiste una sezione di $\pi$.

Sia ora $X$ un $H$-spazio e definiamo un'azione destra di $H$ su $G \times X$ data da $(g,x).h = (gh, h^{-1}.x)$.

\begin{lemma}\label{GxHXexist}
Se $\pi$ ammette sezioni locali, allora esiste lo spazio quoziente $G \times_H X = (G \times X)/H$.
Tale quoziente è un $G$-spazio per l'azione $g.[g',x] = [gg',x]$.
\end{lemma}

L'idea alla base della dimostrazione è che, presa una sezione locale $\sigma : U \to G$, si può definire l'isomorfismo di varietà
$U \times H \to \pi^{-1}(U)$ dato da $(u,h) \mapsto \sigma(u) h$, quindi l'isomorfismo
$(U \times X) \times H \to \pi^{-1}(U) \times X$ dato da $(u,x;h) \mapsto (\sigma(u) h, h^{-1}.x)$.
Segue che $(\pi^{-1}(U) \times X)/H \simeq ((U \times X) \times H)/H \simeq U \times X$ è uno spazio quoziente,
e incollando opportunamente i quozienti $U \times X$ al variare di $U$ fra gli aperti del ricoprimento di $G/H$
otteniamo lo spazio quoziente $G \times_H X$.

Diciamo che $G \times_H X$ è il fibrato di $G/H$ associato ad $X$.
