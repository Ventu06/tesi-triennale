In questa sezione parleremo dei caratteri, una categoria di rappresentazioni particolarmente semplici in quanto di dimensione $1$,
e dei cocaratteri, una sorta di duale dei caratteri. Introdurremo anche i gruppi diagonalizzabili e i tori, gruppi sui quali i
caratteri e i cocaratteri assumeranno una certa importanza.

\medskip
Per tutta la sezione sia $G$ un gruppo algebrico lineare.

% 3.2.1
\begin{definition}[Carattere]{\ \\}
Un omomorfismo di gruppi algebrici $\chi : G \to \Gb_m$ si dice \textit{carattere razionale} o più semplicemente \textit{carattere}.
L'insieme dei caratteri si denota con $X^*(G)$ ed è dotato di una naturale struttura di gruppo abeliano indotta da quella di $\Gb_m$;
scriveremo l'operazione di gruppo additivamente.
\end{definition}

\begin{definition}[Cocarattere]{\ \\}
Un omomorfismo di gruppi algebrici $\lambda : \Gb_m \to G$ si dice \textit{cocarattere}.
L'insieme dei cocaratteri si denota con $X_*(G)$ e, se $G$ è commutativo, anch'esso è dotato di una naturale struttura di gruppo abeliano
indotta da quella di $G$; in tale caso scriveremo l'operazione di gruppo additivamente.
\end{definition}

\begin{definition}[Gruppi Diagonalizzabili e Tori]{\ \\}
Sia $\Db_n$ il gruppo delle matrici diagonali non singolari di rango $n$.
Un gruppo algebrico lineare $D$ è \textit{diagonalizzabile} se è isomorfo a un sottogruppo chiuso di $\Db_n$ per un qualche $n$.
Un gruppo algebrico lineare $T$ si dice \textit{toro} se è isomorfo a $\Db_n$ per un qualche $n$.
\end{definition}

Scriviamo un elemento $x \in \Db_n$ come $\diag(\chi_1(x), \dots, \chi_n(x))$ con $\chi_i$ caratteri di $\Db_n$.
Si vede facilmente che $k[\Db_n] = k[\chi_1, \dots, \chi_n, \chi_1^{-1}, \dots, \chi_n^{-1}]$.

% 3.2.2
\begin{theorem}\label{CharCocharDn}
I monomi $\chi_1^{a_1} \dots \chi_n^{a_n}$ con $(a_1, \dots, a_n) \in \Zb^n$ formano una base di $k[\Db_n]$ e
costituiscono tutti e soli i caratteri di $\Db_n$, da cui $X^*(\Db_n) \simeq \Zb^n$.
Inoltre i cocaratteri di $\Db_n$ sono tutti e soli gli omomorfismi della forma $x \mapsto \diag(x^{a_1}, \dots, x^{a_n})$
con $(a_1, \dots, a_n) \in \Zb^n$, da cui anche $X_*(\Db_n) \simeq \Zb^n$.
\end{theorem}

\medskip
I due seguenti teoremi mettono in stretta relazione la diagonalizzabilità e la proprietà di essere un toro
con il gruppo dei caratteri.
La terza condizione del prossimo teorema rivestirà in ruolo fondamentale nei successivi capitoli, dove,
dato un gruppo $G$ e un $G$-modulo $M$, troveremo un toro contenuto in $G$ che utilizzeremo
per decomporre $M$ in sottospazi $T$-stabili sui quali $T$ agisce secondo un certo carattere.

% 3.2.3
\begin{theorem}\label{GdiagiffXfiniffRonedim}
Le seguenti proprietà sono equivalenti:
\begin{enumerate}[(a)]
	\item $G$ è diagonalizzabile;
	\item $X^*(G)$ è un gruppo abeliano finitamente generato;
	\item Ogni rappresentazione di $G$ si può esprimere come somma diretta di rappresentazioni di dimensione $1$.
\end{enumerate}
\end{theorem}

% 3.2.7
\begin{theorem}\label{GtoriffconiffXGlib}
Sia $G$ un gruppo diagonalizzabile.
\begin{enumerate}[(i)]
	\item $G$ è un toro se e solo se è connesso;
	\item $G$ è un toro se e solo se $X^*(G)$ è un gruppo abeliano libero.
\end{enumerate}
\end{theorem}

\medskip
Il prossimo teorema è utile per descrivere le azioni per coniugio non banali di un gruppo su un suo sottogruppo diagonalizzabile.
Indichiamo con $N_G(H)$ il normalizzatore di $H$ in $G$, con $Z_G(H)$ il centralizzatore di $H$ in $G$ e con
$H^0$ la componente connessa di $H$ contenente $e$.

% 3.2.9
\begin{theorem}\label{NGdivZGfin}
Se $H$ è un sottogruppo diagonalizzabile di $G$ allora si ha $N_G(H)^0 = Z_G(H)^0$ e $N_G(H)/Z_G(H)$ è finito.
\end{theorem}
