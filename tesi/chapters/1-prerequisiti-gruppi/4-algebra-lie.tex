Questa sezione ha lo scopo di introdurre il concetto di spazio tangente per varietà algebriche affini
per poi specializzarlo al caso dei gruppi algebrici lineari, mostrandone l'isomorfismo con l'algebra di Lie
del gruppo e le varie proprietà indotte dalla struttura di gruppo.

% 4.1.1
\begin{definition}[Derivazione]{\ \\}
Sia $R$ un anello commutativo, $A$ una $R$-algebra e $M$ un $A$-modulo a sinistra.
Una \textit{$R$-derivazione} di $A$ in $M$ è una mappa $R$-lineare $D: A \to M$ tale che
\[
	D(ab) = a.D(b) + b.D(a) \qquad (a,b \in A)
\]
\end{definition}

Indichiamo con $\Der_R(A,M)$ l'insieme delle $R$-derivazioni di $A$ in $M$.
$\Der_R(A,M)$ è un $A$-modulo, la cui struttura è data da $(D+D')(a) = D(a) + D'(a)$, $(b.D)(a) = b.D(a)$ ($D, D' \in \Der_R(A,M)$, $a,b \in A$).

Sia $X$ una varietà affine.
Dato $x \in X$, possiamo costruire una struttura di $k[X]$-modulo su $k$ che indichiamo con $k_x$, dove l'azione di $k[X]$ è data da $f.t = f(x)t$.

% 4.1.3
\begin{definition}[Spazio Tangente]{\ \\}
	Dato $x \in X$, definiamo lo \textit{spazio tangente} $T_x X$ di $X$ nel punto $x$ come il $k[X]$-modulo $\Der_k(k[X],k_x)$.
\end{definition}

Dato un morfismo di varietà affini $\phi : X \to Y$, esso induce una mappa lineari di spazi tangenti $d\phi_x : T_x X \to T_{\phi(x)} Y$ detta \textit{differenziale} per ogni $x \in X$.
Si ha inoltre che $d \Id_X = \Id_{T_x X}$ e $d (\psi \circ \phi)_x = d \psi_{\phi(x)} \circ d \phi_x$.

\medskip
Restringiamoci ora al caso in cui $X = G$ è un gruppo algebrico lineare.

% 4.4.1
\subsection
Indichiamo con $\lambda$ e $\rho$ rispettivamente le rappresentazioni di $G$ date dalle azioni sinistra e destra su $k[G]$.

Sia $\Dc = \Dc_G = \Der_k(k[G],k[G])$.
$\Dc$ ha una struttura di algebra di Lie, in cui il bracket è dato da $[D,D'] = D \circ D' - D' \circ D$ ($D,D' \in \Dc$).

L'azione di $\lambda$ su $k[G]$ induce una rappresentazione di $G$ su $\Dc$, che indicheremo sempre con $\lambda$, data da
\[
	\lambda(x) D = \lambda(x) \circ D \circ \lambda(x)^{-1} \qquad (x \in G, D \in \Dc)
\]
In modo simile definiamo la rappresentazione $\rho$ di $G$ su $\Dc$ indotta da $\rho$.
Sia $L(G)$ il sottoinsieme di $\Dc$ contenente le derivazioni che commutano con $\lambda(x)$ per ogni $x \in G$ (cioè sulle quali la rappresentazione $\lambda$ agisce banalmente).
Notiamo che $L(G)$ è una sottoalgebra di Lie di $\Dc$, stabile sotto l'azione di $\rho(x)$ ($x \in G$) in quanto $\lambda(x)$ e $\rho(x)$ commutano.

Chiameremo $L(G)$ l'algebra di Lie del gruppo $G$.

\medskip
I tre seguenti enunciati mostrano che possiamo identificare $L(G)$ con lo spazio tangente nell'identità, andando a definire una struttura
di algebra di Lie e di $G$-modulo su tale spazio.

Sia $\alpha = \alpha_G : \Dc \to T_e G$ la mappa lineare data da $(\alpha D)(f) = (Df)(e)$.

% 4.4.5
\begin{proposition}
$\alpha$ induce un isomorfismo di spazi vettoriali $L(G) \simeq T_e G$;
\end{proposition}

Forti di questo risultato, possiamo definire la \textit{rappresentazione aggiunta} $\Ad(x)$ di $G$ su $T_e G$
come $\Ad(x) = \alpha \circ \rho(x) \circ \alpha^{-1}$.

\begin{proposition}
$\Ad$ è una rappresentazione razionale di $G$ su $T_e G$.
\end{proposition}

Sia $H$ un sottogruppo chiuso di $G$ e sia $J \subset k[G]$ l'ideale delle funzioni che si annullano in $H$, in modo che $k[H] = k[G]/J$.
Poniamo $\Dc_{G,H} = \{ D \in \Dc_G \mid D(J) \subset J \}$.
$\Dc_{G,H}$ è una sottoalgebra dell'algebra di Lie $\Dc_G$, che può immergersi in quest'ultima tramite un omomorfismo
iniettivo di algebre di Lie $\phi$.

% 4.4.7
\begin{lemma}
$\phi$ definisce un isomorfismo fra $\Dc_{G,H} \cap L(G)$ e $L(H)$.
\end{lemma}

\medskip
Indicheremo da ora in poi le algebre di Lie dei gruppi algebrici lineari $G, H, \dots$ con le lettere $\gf, \hf, \dots$.

Dato un omomorfismo di gruppi algebrici $\phi : G \to G'$, chiamiamo \textit{differenziale} e indichiamo
con $d\phi$ la mappa $d\phi_e : \gf \to \gf'$ precedentemente definita con lo stesso nome, senza specificare
che viene considerata nell'identità e omettendo l'identificazione fra tangente e algebra di Lie.

\medskip
Enunciamo un lemma che utilizzeremo in seguito per mettere in relazione sottogruppi chiusi e sottoalgebre.
\begin{lemma}\label{SubalgThenSubgrp}
Siano $H,K$ sottogruppi chiusi di $G$ con $H$ connesso.
Dette $\hf$ e $\kf$ le loro algebre di Lie, supponiamo che $\hf \subset \kf$, allora $H \subset K$.
\end{lemma}
La dimostrazione di questo risultato si basa sul fatto che $L(H \cap K) = L(H) \cap L(K)$, fatto vero solo in caratteristica $0$,
che non utilizzeremo in seguito ma che, in generale, riveste una certa importanza.
