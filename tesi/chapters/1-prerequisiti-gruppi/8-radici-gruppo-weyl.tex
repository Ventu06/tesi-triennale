Questa sezione è fondamentale per lo studio dei gruppi algebrici lineari, in particolar modo quelli riduttivi
ai quali, per semplicità, ci restringeremo.
Verranno qui introdotti le radici, le coradici e il gruppo di Weyl, tutti argomenti che si ripresenteranno
in continuazione nel resto della tesi, particolarmente importanti in quanto permetteranno
di ridurre lo studio dei gruppi allo studio di oggetti finiti e, in un certo senso, combinatorici.

\medskip
In questa sezione $G$ denota un gruppo algebrico lineare connesso riduttivo e $T$ un relativo toro massimale.
Sia $X = X^*(T)$ il gruppo dei caratteri di $T$.

\medskip
Introduciamo subito il concetto di radici.

% 7.1.1
\subsection
Sia $S$ un toro e $r : S \to GL(V)$ una rappresentazione razionale di $S$.
Sappiamo da~\ref{GdiagiffXfiniffRonedim} che $V$ si può scrivere come somma diretta di sottospazi $1$-dimensionali,
in ciascuno dei quali un elemento $s \in S$ agisce come moltiplicazione per $\chi(s)$, con $\chi$ carattere di $S$.
Chiamiamo i caratteri in questione \textit{pesi} di $S$ su $V$. I sottospazi non nulli associati a questi caratteri
\[
	V_{\chi} := \{ v \in V \mid r(s)v = \chi(s)v \quad \forall s \in S) \}
\]
sono detti \textit{spazi-peso}.

\begin{definition}[Radici]\label{rootDef}{\ \\}
Consideriamo l'azione di $T$ sull'algebra di Lie $\gf$ di $G$ data dalla rappresentazione aggiunta $\Ad$ e indichiamo con $R$ l'insieme dei relativi pesi non nulli.
$R$ è un sottoinsieme finito di $X$ e chiameremo i suoi elementi \textit{radici}.
\end{definition}

\medskip
I sottogruppi $G_{\alpha}$, che andremo ora a definire, sono particolarmente semplici e verranno analizzati più in dettaglio nella prossima sezione.
Allo stesso tempo si riveleranno utili in quanto vedremo che generano l'intero gruppo, quindi permettono di studiare a fondo
parti del gruppo prese singolarmente.

% 7.1.3 (prev)
\subsection\label{Galpha}
Dato $\alpha \in R$, denotiamo con $G_{\alpha}$ il centralizzatore del sotto-toro $(\Ker(\alpha))^0$ di $T$,
dove con quest'ultima notazione intendiamo la componente connessa di $\Ker(\alpha)$ contenente $e$.

% 7.2.1
\begin{definition}[Rango e Rango Semi-Semplice]{\ \\}
Sia $G$ un gruppo algebrico lineare e $T$ un suo toro massimale.
Definiamo il \textit{rango} di $G$ come la dimensione di $T$ e notiamo che
non dipende dalla scelta di $T$ in quanto i tori massimali sono tutti coniugati.
Definiamo il \textit{rango semi-semplice} di $G$ come il rango di $G/R(G)$.
\end{definition}

% 7.1.3 (prev) + 7.6.4
\begin{lemma}\label{GaProp}{\ }
\begin{enumerate}[(i)]
	\item $G_{\alpha}$ è un sottogruppo chiuso, connesso e non risolubile;
	\item $G_{\alpha}$ è un gruppo riduttivo;
	\item $R(G_{\alpha}) = \Ker(\alpha)$ e $G_{\alpha}$ ha rango semi-semplice pari ad $1$.
\end{enumerate}
\end{lemma}

% 7.1.3
\begin{lemma}\label{GagenG}
I gruppi $T$ e $G_{\alpha}$ al variare di $\alpha \in R$ generano $G$.
\end{lemma}

\medskip
Procediamo a definire il gruppo di Weyl, secondo oggetto fondamentale di questa sezione.

% 7.1.4
\begin{definition}[Gruppo di Weyl]{\ \\}
Definiamo il \textit{gruppo di Weyl} di $(G,T)$ come $W = W(G,T) = N_G(T) / Z_G(T)$,
cioè come il quoziente fra normalizzatore e centralizzatore di $T$ in $G$.
\end{definition}

Per~\ref{NGdivZGfin} $W$ ha ordine finito.
L'azione per coniugio di $W$ su $T$ induce un'azione di $W$ su $X$, in particolare identificando
quest'ultimo con un gruppo abeliano libero finitamente generato come in~\ref{GtoriffconiffXGlib} si ha che $W$
agisce come un gruppo di automorfismi di $X$.
Guardando l'azione sulla base di caratteri descritta in~\ref{CharCocharDn} si evince che tale azione è fedele,
quindi possiamo identificare $W$ con il gruppo di automorfismi che induce.
Inoltre con una breve verifica si può mostrare che $W$ permuta gli elementi di $R$.

\medskip
Il prossimo obbiettivo di questa trattazione sarà rafforzare il legame tra radici e gruppo di Weyl,
in particolar modo in vista della sezione sul dato di radici.
Inizieremo associando ad ogni radice un elemento del gruppo di Weyl detto riflessione,
associazione che non richiediamo essere iniettiva.

\medskip
Innanzitutto mostriamo qualche risultato aggiuntivo sul gruppo di Weyl.
Detto $S$ un sottotoro di $T$, notiamo che $W(Z_G(S),T)$ è un sottogruppo di $W(G,T)$, infatti
\begin{align*}
	W(Z_G(S),T)
	&=N_{Z_G(S)}(T) / Z_{Z_G(S)}(T)\\
	&= (N_G(T) \cap Z_G(S)) / (Z_G(T) \cap Z_G(S))\\
	&= (N_G(T) \cap Z_G(S)) / Z_G(T)\\
	&\subset N_G(T) / Z_G(T) = W(G,T)
\end{align*}

Se $S$ è centrale in $G$, allora $T/S$ è un toro massimale di $G/S$, inoltre si può mostrare con un breve conto che
$N_{G/S}(T/S) \simeq N_G(T)/S$ e $Z_{G/S}(T/S) \simeq Z_G(T)/S$, da cui $W(G,T) \simeq W(G/S, T/S)$.


Sia $W_{\alpha} = W(G_{\alpha},T)$, sia inoltre $S = (\Ker(\alpha))^0 \subset T$, sottotoro centrale di $G_{\alpha}$,
allora $W_{\alpha} \simeq W(G_{\alpha}/S, T/S)$.
Dato che $T/S \simeq \Gb_m$, si evince che $W_{\alpha}$ è un sottogruppo degli automorfismi di $\Gb_m$,
i quali sono costituiti solo da $\pm \Id$, quindi ha ordine al più $2$.

% 7.1.5
\begin{proposition}
Supponiamo $G$ sia non risolubile di rango $1$, allora $W$ ha ordine $2$.
\end{proposition}

% 7.1.6
\subsection
Per ogni $\alpha \in R$ applichiamo la precedente proposizione a $(G/S,T/S)$ ottenendo che $W_{\alpha} = W(G/S,T/S)$ ha ordine $2$.
Scegliamo $n_{\alpha} \in N_{G_{\alpha}}(T) \setminus Z_{G_{\alpha}}(T)$ e sia $s_{\alpha}$ l'immagine di
$n_{\alpha}$ in $W_{\alpha} \subset W$, ben definita in quanto è l'unico elemento diverso dall'identità.

$s_{\alpha}$ è la cosiddetta riflessione associata ad $\alpha$ e $n_{\alpha}$ è un suo rappresentante in $N_G(T)$.

\medskip
Procediamo definendo una struttura di spazio vettoriale reale su caratteri e cocaratteri, per poi vederne i legami
con le riflessioni appena definite.

\subsection\label{XVXvVv}
Denotiamo con $X^{\vee} := \Hom(X,\Zb)$ il duale di $X$ e con $\langle \cdot, \cdot \rangle$ l'accoppiamento tra $X$ e $X^{\vee}$.
$X^{\vee}$ è naturalmente isomorfo al gruppo $X_*(T)$ dei cocaratteri di $T$.
Identifichiamo $X$ e $X^{\vee}$ con i rispettivi sottogruppi di $V := \Rb \otimes_{\Zb} X$ e $V^{\vee} := \Rb \otimes_{\Zb} X^{\vee}$
e denotiamo l'accoppiamento indotto fra $V$ e $V^{\vee}$ sempre con $\langle \cdot, \cdot \rangle$.

% 7.1.7
\subsection\label{bilsymsa}
Consideriamo una forma bilineare simmetrica definita positiva $(\cdot, \cdot)$ su $V$ che sia invariante per l'azione di $W$.
Per mostrarne l'esistenza, basta prendere una qualsiasi forma bilineare simmetrica definita positiva $f$ su $V$ e porre
\[
	(x,y) = \sum_{w \in W} f(w.x, w.y) \qquad (x,y \in V)
\]
Si può dimostrare che $s_{\alpha}(x) = x - 2(\alpha, \alpha)^{-1}(x,\alpha)\alpha$ per ogni $\alpha \in R$, $x \in V$.
Quest'uguaglianza è vera per ogni forma che rispetta le proprietà precedentemente enunciate.

\medskip
Presentiamo ora il concetto di coradice:

% 7.1.8
\begin{lemma}\label{V-Vc_pairing}{\ }
\begin{enumerate}[(i)]
	\item Per ogni $\alpha \in R$ esiste un unico $\alpha^{\vee} \in X^{\vee}$ con $\langle \alpha, \alpha^{\vee} \rangle = 2$
		tale che per ogni $x \in X$ si ha $s_{\alpha}(x) = x - \langle x, \alpha^{\vee} \rangle \alpha$, cioè che
		$\langle x, \alpha^{\vee} \rangle = 2(\alpha, \alpha)^{-1}(x,\alpha)$;
	\item Se $\alpha,\beta \in R$ e $G_{\beta} = G_{\alpha}$ allora $s_{\beta} = s_{\alpha}$.
\end{enumerate}
\end{lemma}

\begin{definition}[Coradici]\label{corootDef}{\ \\}
Indichiamo con $R^{\vee}$ l'insieme contenente i vari $\alpha^{\vee}$ al variare di $\alpha \in R$.
Chiamiamo gli elementi di $R^{\vee}$ \textit{coradici}.
\end{definition}

\medskip
Enunciamo infine un teorema che suggerisce l'importanza delle riflessioni nella teoria che andremo a sviluppare.

% 7.1.9
\begin{theorem}
$W$ è generato dalle riflessioni $\{s_{\alpha}\}_{\alpha \in R}$
\end{theorem}
