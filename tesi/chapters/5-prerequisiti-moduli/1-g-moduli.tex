In questa sezione riprenderemo il concetto di $G$-modulo, già introdotto nel primo capitolo,
e lo amplieremo, andando a parlare del funtore punto fisso, di semplicità, di zoccoli e di
altre proprietà necessarie allo sviluppo della teoria.


\medskip
Sia $G$ un gruppo algebrico lineare.

Utilizzeremo spesso la nozione di \textit{modulo} intendendo $k$-modulo, cioè $k$-spazio vettoriale,
e di \textit{omomorfismo di moduli} intendendo mappa $k$-lineare.

\medskip
Ricordiamo la definizione di $G$-modulo data in~\ref{gmodulo}.

Un \textit{omomorfismo di $G$-moduli} è un omomorfismo di moduli $G$-equivariante.
Dati due $G$-moduli $M,M'$ indichiamo l'insieme degli omomorfismi con $\Hom_G(M,M')$.

% I.2.9
Dato un sottogruppo $H$ di $G$ e un $G$-modulo $M$, un \textit{$H$-sottomodulo} $N$ di $M$
è un sottospazio $N \subset M$ che è anche un $H$-modulo con l'azione indotta dall'azione di $M$.

% I.2.10
\subsection
Dato un $G$-modulo $M$, possiamo considerare il sottomodulo formato dai punti fissi
\[
	M^G = \{ m \in M \mid g.m = m \quad \forall g \in G \}
\]

Generalizziamo questa nozione ponendo per ogni carattere $\chi \in X(G)$
\[
	M_{\chi} = \{ m \in M \mid g.m = \chi(g)m \}
\]

Nei prossimi capitoli faremo ampio uso di questa notazione, dove al posto del gruppo $G$ ci sarà un toro massimale $T$ di $G$.

Si verifica facilmente che $M \mapsto M_{\chi}$ è un funtore esatto a sinistra fra $G$-moduli, quindi
preserva le immersioni, e, dati due $G$-moduli $E$ ed $F$, è immediato vedere che $(E \oplus F)_{\chi} = E_{\chi} \oplus F_{\chi}$.
Nel caso $\chi$ sia il carattere banale, il funtore in questione è detto punto fisso ed è dato da $M \mapsto M^G$.
Segue che $M \mapsto M^G$ è esatto a sinistra, quindi preserva le immersioni, e $(E \oplus F)^G = E^G \oplus F^G$.

\subsection
In modo simile a quanto fatto con la comoltiplicazione in~\ref{cogroup} e in~\ref{coaction},
si può introdurre la mappa di comodulo $\Delta_M : M \to M \otimes k[G]$.
Le proprietà dell'azione di $G$ su $M$ implicano che $\Delta_M$ soddisfi le relazioni
\begin{align*}
	(\Delta_M \otimes \Id_{k[G]}) \circ \Delta_M &= (\Id_M \otimes \Delta) \circ \Delta_M\\
	(\Id_M \ootimes \epsilon) \circ \Delta_M &= \Id_M
\end{align*}

\medskip
Utilizzando questa mappa si può dimostrare un risultato simile a~\ref{GlocfinkX} procedendo nello stesso modo:

\begin{lemma}\label{GlocfinM}
Sia $M$ un $G$-modulo, allora ogni $k$-sottomodulo di $M$ di dimensione finita è contenuto
in un $G$-sottomodulo di $M$ di dimensione finita.
\end{lemma}

\medskip
Introduciamo i concetti di $G$-modulo semplice e di zoccolo, poi presentiamo alcuni risultati riguardanti quest'ultimo.

% 2.14
\begin{definition}[$G$-Modulo Semplice e Semi-Semplice]{\ \\}
Un $G$-modulo $M$ si dice \textit{semplice} e la relativa rappresentazione \textit{irriducibile}
se $M \neq 0$ e $M$ non ha $G$-sottomoduli diversi da $0$ e $M$.
Un $G$-modulo si dice \textit{semi-semplice} se si può esprimere come somma diretta di moduli semplici.
\end{definition}

\begin{definition}[Zoccolo]{\ \\}
Dato un $G$-modulo $M$, chiamiamo \textit{zoccolo} di $M$ e denotiamo con $\soc_G(M)$ la
somma diretta di tutti i suoi $G$-sottomoduli semplici.
\end{definition}

Notiamo come lo zoccolo di un $G$-modulo sia il più grande $G$-sottomodulo semi-semplice di $M$.

\medskip
Da~\ref{GlocfinM} discende subito il seguente risultato:

\begin{lemma}\label{socMneq0}{\ }
\begin{enumerate}[(i)]
	\item Ogni $G$-modulo semplice ha dimensione finita;
	\item Sia $M$ un $G$-modulo con $M \neq 0$, allora anche $\soc_G M \neq 0$.
\end{enumerate}
\end{lemma}

\begin{definition}[Serie di Zoccoli]{\ \\}
Dato un $G$-modulo $M$ si può costruire iterativamente una successione di sottomoduli
$\soc_G^i M$ ($i \in \Nb$), detta \textit{serie di zoccoli}, in modo che
\[
	\soc_G^0 = 0 \text{ ,} \qquad \soc_G^i M / \soc_G^{i-1} M = \soc_G(M/\soc_G^{i-1} M) \quad \forall i > 0
\]
\end{definition}

Notiamo che $\soc_G^1 M = \soc_G M$ e che $0 = \soc_G^0 M \subset \soc_G^1 M \subset \soc_G^2 M \subset \dots$.
Inoltre da~\ref{GlocfinM} discende che $\bigcup_{i \in \Nb} \soc_G^i M = M$.

\medskip
Diciamo che un $G$-modulo semplice $E$ è un \textit{fattore di composizione} di un $G$-modulo $M$
se $E$ è un addendo nella somma diretta di $\soc_G(M/\soc_G^{i-1} M) = \soc_G^i M / \soc_G^{i-1} M$ per un qualche $i$.

Si può dimostrare che la molteplicità di $E$ come addendo in $\bigoplus_{i \in \Nb} \soc_G^i M / \soc_G^{i-1} M$
è indipendente dalla scelta della serie di zoccoli.

\medskip
Enunciamo infine due risultati sull'azione di gruppi unipotenti.

\begin{lemma}
$G$ è unipotente se e solo se l'unico $G$-modulo semplice è $k$,
cioè l'unico modulo di dimensione $1$ sul quale $G$ agisce banalmente.
\end{lemma}

Per il precedente lemma, se $G$ è unipotente allora $\soc_G M = M^G$.
Da questo risultato e da~\ref{socMneq0} (ii) si deduce il seguente:

\begin{corollary}\label{MfixUneq0}
$G$ è unipotente se e solo se per ogni $G$-modulo $M \neq 0$ si ha $M^G \neq 0$.
\end{corollary}
