Come da titolo, in questa sezione si parlerà dei funtori restrizione e induzione e della reciprocità di Frobenius.
Dati un gruppo algebrico lineare $G$ e un sottogruppo $H$, questi funtori ci permettono di costruire $H$-moduli
a partire da $G$-moduli e viceversa.
La reciprocità di Frobenius collega i due funtori mediante una bigezione fra gli spazi degli omomorfismi dei $G$-moduli e degli $H$-moduli.

\medskip
La definizione del funtore restrizione è intuitiva ed immediata:

% 3.1
\begin{definition}[Restrizione]
Sia $G$ un gruppo algebrico lineare e sia $H$ un sottogruppo di $G$.
Ogni $G$-modulo $M$ è un $H$-modulo in modo naturale restringendo l'azione di $G$ ad $H$.
Chiamiamo questo $H$-modulo \textit{modulo ristretto di $M$ da $G$ in $H$} e denotiamolo con $\Res_H^G M$.

In questo modo possiamo definire un funtore
\[
	\Res_H^G : \{ G \text{-moduli} \} \to \{ H \text{-moduli} \}
\]
\end{definition}

\medskip
Il funtore induzione invece è più complesso da definire.
Il seguente lemma sarà necessario per costruzione del modulo indotto.

% 3.2
\begin{lemma}
Sia $G$ un gruppo algebrico lineare e siano $H, H'$ sottogruppi di $G$ con $H'$ che normalizza $H$.
Se $M$ è un $G$-modulo, allora $M^H$ è un $H'$-sottomodulo di $M$.
\end{lemma}

% 3.3
\begin{definition}[Induzione]
Sia $G$ un gruppo algebrico lineare e sia $H$ un sottogruppo di $G$.
Per ogni $H$-modulo $M$ esiste una naturale struttura di $(G \times H)$-modulo su $M \otimes k[G]$ definita
prendendo il prodotto tensore delle seguenti rappresentazioni di $G \times H$ su $M$ e su $k[G]$:
si fa agire $G$ banalmente su $M$ e per traslazione sinistra su $k[G]$, mentre si mantiene la struttura di $H$-modulo di $M$
e si fa agire $H$ per traslazione destra su $k[G]$.
In altre parole, detta $\phi : H \times M \to M$ l'azione che definisce la struttura di $H$-modulo di $M$,
si costruisce la nuova azione $\psi : (G \times H) \times (M \otimes k[G]) \to (M \otimes k[G])$ data da
\[
	\psi((g,h))(m \otimes f)(x) = (\phi(h)(m) \otimes f)(g^{-1}xh) = \phi(h)(m) \otimes f(g^{-1}xh)
\]
Applicando il precedente lemma ai sottogruppi $H,G$ di $G \times H$ e al $(G \times H)$-modulo $M \otimes k[G]$
troviamo che $(M \otimes k[G])^H$ è un $G$-sottomodulo di $M \otimes k[G]$.
Chiamiamo questo $G$-modulo \textit{modulo indotto di $M$ da $H$ in $G$} e denotiamolo con $\Ind_H^G M$.

In questo modo possiamo definire un funtore
\[
	\Ind_H^G : \{ H \text{-moduli} \} \to \{ G \text{-moduli} \}
\]
\end{definition}

\medskip
Procediamo ad enunciare l'importante relazione fra i due funtori appena definiti.

% 3.4
\medskip
Per ogni modulo $M$ sia $\epsilon_M : M \otimes k[G] \to M$ la mappa lineare data da $\epsilon_M = \Id_M \ootimes \epsilon_G$

\begin{proposition}[Reciprocità di Frobenius]\label{recfrob}{\ \\}
Sia $H$ un sottogruppo di $G$ e sia $M$ un $H$-modulo.
\begin{enumerate}[(i)]
	\item $\epsilon_M : \Ind_H^G M \to M$ è un omomorfismo di $H$ moduli;
	\item Per ogni $G$-modulo $N$ la mappa $\phi \mapsto \epsilon_M \circ \phi$ definisce l'isomorfismo fra i seguenti spazi vettoriali:
		\[
			\Hom_G(N, \Ind_H^G M) \xrightarrow{\simeq} \Hom_H(\Res_H^G N, M)
		\]
\end{enumerate}
\end{proposition}
