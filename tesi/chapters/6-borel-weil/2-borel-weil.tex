Questa sezione è la più importante dell'intera tesi, in quanto qui si enuncia e dimostra il teorema di Borel-Weil.

Brevemente, fissato un gruppo algebrico lineare $G$, connesso e riduttivo, il teorema in questione ci fornisce
un sistema di rappresentanti per le classi di isomorfismo dei $G$-moduli semplici.
Tali classi sono in bigezione con i pesi dominanti, un sottoinsieme dei caratteri $X = X^*(T)$ costituito dagli elementi
il cui accoppiamento con ogni coradice è non-negativo.
Più in dettaglio, per ogni peso dominante $\chi$ costruiremo un $G$-modulo semplice $L(\chi)$ avente $\chi$ come peso massimale.

Nella prima parte della sezione dimostreremo che le classi di isomorfismo dei $G$-moduli semplici sono rappresentate
dai vari $L(\chi)$ non nulli al variare di $\chi \in X$.
Solo alla fine svolgeremo la dimostrazione principale, provando che $L(\chi)$ è non nullo se e solo se $\chi$ è dominante.
Tale dimostrazione farà estensivamente uso della teoria sviluppata nella prima parte della tesi, in particolare
si avvarrà della decomposizione di Bruhat al fine di applicare il teorema di Hartogs (che andremo ad enunciare)
per estendere una funzione e concludere che $L(\chi) \neq 0$.

\medskip
Iniziamo subito con una proposizione che riutilizzeremo più volte e che ci permetterà di affermare
che ogni $G$-modulo semplice è rappresentato da un qualche $L(\chi)$.

% II.2.1
\begin{proposition}\label{VUneq0}
Sia $V$ un $G$-modulo con $V \neq 0$.
\begin{enumerate}[(i)]
	\item $V^{U^+} \neq 0$ e $V^{U^-} \neq 0$;
	\item Se $\dim(V) < \infty$ esistono $\chi^+ \in X(T)$ tale che $\Hom_{G}(V,\Ind_{B^+}^G k_{\chi^+}) \neq 0$
		e $\chi^- \in X(T)$ tale che $\Hom_{G}(V,\Ind_{B^-}^G k_{\chi^-}) \neq 0$;
\end{enumerate}
\end{proposition}
\begin{proof}

Dato che $U^+$ e $U^-$ sono unipotenti, grazie a~\ref{MfixUneq0} si trova $V^{U^+} \neq 0$ e $V^{U^-} \neq 0$.

Notiamo che $T$ normalizza $U^+$, quindi stabilizza $V^{U^+}$, che di conseguenza è un $T$-sottomodulo di $V$.
Sappiamo che $V^{U^+}$ si decompone come somma diretta dei suoi spazi-peso, i quali corrispondono alle somme dirette di tutti i
$T$-sottomoduli semplici di $V^{U^+}$ isomorfi a $k_{\chi}$ al variare di $\chi$ fra i pesi di $V^{U^+}$.
Sia $\chi^+$ un peso di $V^{U^+}$, allora $\Hom_{T}(k_{\chi^+},V^{U^+}) \neq 0$.
Dato che $U^+$ agisce banalmente su $V^{U^+}$, l'azione di $G$ su $V$ induce un'azione di $B^+ = TU^+$ su $V^{U^+}$
e ogni $T$-sottomodulo di $V^{U^+}$ è anche un $B^+$-sottomodulo.
Segue che anche $\Hom_{B^+}(k_{\chi^+},V^{U^+}) \neq 0$ e perciò $\Hom_{B^+}(k_{\chi^+},V) \neq 0$.

Dato un modulo su cui è definita un'azione di gruppo, possiamo considerare il suo duale (come spazio vettoriale)
con la relativa azione naturalmente indotta.
Si verifica che l'isomorfismo canonico di uno spazio vettoriale di dimensione finita con il suo biduale
è anche un isomorfismo di moduli per azioni di gruppo.
Sapendo che $\dim(V) < \infty$, per quanto appena detto esiste $-\chi^+ \in X(T)$ con
$0 \neq \Hom_{B^+}(k_{-\chi^+},V^*) \simeq \Hom_{B^+}(V,k_{-\chi^+}^*)$,
e dato che $k_{-\chi^+}^* \simeq k_{\chi^+}$ si trova $\Hom_{B^+}(V,k_{\chi^+}) \neq 0$.

Applicando la reciprocità di Frobenius~\ref{recfrob} concludiamo che $\Hom_{G}(V,\Ind_{B^+}^G k_{\chi^+}) \neq 0$.
Con lo stesso ragionamento si dimostra che esiste $\chi^- \in X(T)$ tale che $\Hom_{G}(V,\Ind_{B^-}^G k_{\chi^-}) \neq 0$.

\end{proof}

\medskip
Procediamo definendo e studiando le proprietà di $H^0(\chi)$, $G$-modulo che di rivelerà fondamentale per la costruzione
dei rappresentanti $L(\chi)$.

\begin{definition}
Per ogni $\chi \in X(T)$ sia
\[
	H^0(\chi) = \Ind_{B^-}^G k_{\chi}
\]
\end{definition}

\begin{lemma}\label{h0chi}
Si ha
\[
	H^0(\chi) = \{ f \in k[G] \mid f(gb) = \chi(b)^{-1}f(g) \quad \forall g \in G, b \in B^- \}
\]
sul quale $G$ agisce per traslazione sinistra.
\end{lemma}
\begin{proof}
Ricordiamo che $H^0(\chi) = \Ind_{B^-}^G k_{\chi} = (k_{\chi} \otimes k[G])^{B^-}$,
dove l'azione di $G \times B^-$ su $k_{\chi} \otimes k[G]$ è data da
\begin{align*}
	\psi((h,b))(x \otimes f)(g)
	&= (b.x) \otimes f(h^{-1}gb)\\
	&= (\chi(b) x) \otimes f(h^{-1}gb)\\
	&= x \otimes (\chi(b) f(h^{-1}gb))
\end{align*}
Possiamo identificare $k_{\chi} \otimes k[G]$ con $k[G]$ sul quale è definita l'azione
$\widetilde{\psi}((h,b))(f)(g) = \chi(b) f(h^{-1}gb)$.
Segue che $f \in H^0(\chi)$ se e solo se $f(g) = \chi(b) f(gb)$ per ogni $g \in G$, $b \in B^-$.
Inoltre $G$ agisce su $H^0(\chi)$ come $(h.f)(g) = f(h^{-1}g)$, cioè per traslazione sinistra.
\end{proof}

% II.2.2
\begin{proposition}\label{HdimHUchar}
Sia $\chi \in X(T)$ tale che $H^0(\chi) \neq 0$.
\begin{enumerate}[(i)]
	\item Si ha $\dim(H^0(\chi)^{U^+}) = 1$ e $H^0(\chi)^{U^+} = H^0(\chi)_{\chi}$;
	\item Ogni peso $\tau$ di $H^0(\chi)$ soddisfa $w_0.\chi \le \tau \le \chi$.
\end{enumerate}
\end{proposition}
\begin{proof}

Ogni $f \in H^0(\chi)^{U^+}$ soddisfa $f(u_1 t u_2) = \chi(t)^{-1}f(e)$
per ogni $u_1 \in U^+$, $t \in T$, $u_2 \in U^-$, quindi $f(e)$ determina completamente la funzione $f$ ristretta a $U^+ B^-$,
e, vista la densità di $U^+ B^-$ in $G$ per~\ref{Cw0OpDen}, anche $f$ è completamente determinata da $f(e)$.
Da ciò segue che $\dim(H^0(\chi)^{U^+}) \le 1$ e, dato che $H^0(\chi) \neq 0$, per~\ref{VUneq0} (i)
$H^0(\chi)^{U^+} \neq 0$, da cui si evince che la dimensione è esattamente $1$.

Consideriamo la mappa di valutazione $\xi : H^0(\chi) \to k_{\chi}$ data da $\xi(f) = f(e)$:
si tratta di un omomorfismo di $B^-$ moduli, in quanto, per ogni $b \in B^-, f \in H^0(\chi)$
\[
	b.\xi(f) = \chi(b) f(e) = \chi(b^{-1})^{-1} f(e) = f(b^{-1}) = \xi(b.f)
\]
ed è iniettiva su $H^0(\chi)^{U^+}$ per quanto appena mostrato.
Dall'ultima equazione si ricava anche $\xi(b.f) = \xi(\chi(b)f)$, da cui
$b.f = \chi(b)f$ per $f \in H^0(\chi)^{U^+}$, dunque $H^0(\chi)^{U^+} \subset H^0(\chi)_{\chi}$.

Sia $M$ un $G$-sottomodulo finito-dimensionale di $H^0(\chi)$ e sia $\tau$ un suo peso massimale,
allora $M_{\tau} \subset M^{U^+} \subset H^0(\chi)^{U^+}$ per~\ref{MlsubMU}.
Combinando questa inclusione con quella mostrata nel precedente paragrafo si trova $M_{\tau} \subset H^0(\chi)_{\chi}$,
da cui $M_{\tau} \subset M \cap H^0(\chi)_{\chi} = M_{\chi}$.
Dato che due spazi-peso distinti hanno intersezione banale, troviamo $\tau = \chi$, cioè $\chi$ è l'unico peso massimale di $M$.

Dalla prima parte della dimostrazione sappiamo che $0 \neq H^0(\chi)^{U^+} \subset H^0(\chi)_{\chi}$, perciò $\chi$ è un peso di $H^0(\chi)$.
Se esistesse un altro peso $\tau$ di $H^0(\chi)$ con $\tau > \chi$, grazie a~\ref{GlocfinM}
potremmo trovare un $G$-sottomodulo finito-dimensionale $M$ con $M_{\tau} = M \cap H^0(\chi)_{\tau} \neq 0$
e raggiungere un assurdo visto che $\chi$ è un peso massimale di $M$ per quanto appena mostrato.
Segue che $\chi$ è l'unico peso massimale di $H^0(\chi)$, cioè $\tau \le \chi$ per ogni peso $\tau$ di $H^0(\chi)$.

Dalla doppia inclusione $H^0(\chi)_{\chi} \subset H^0(\chi)^{U^+} \subset H^0(\chi)_{\chi}$ ottenuta sempre combinando~\ref{MlsubMU}
con il risultato della prima parte della dimostrazione, troviamo $H^0(\chi)^{U^+} = H^0(\chi)_{\chi}$.

Se $\tau$ è un peso di $H^0(\chi)$, anche $w_0.\tau$ lo è per~\ref{SpPesoConj}, quindi $w_0.\tau \le \chi$ e dunque concludiamo
$\tau \ge w_0.\chi$ per~\ref{w0ord}.
\end{proof}

\medskip
A questo punto possiamo definire gli $L(\chi)$ e mostrare che, al variare di $\chi \in X(T)$ con $H^0(\chi) \neq 0$,
rappresentano i $G$-moduli semplici a meno di isomorfismo.

% II.2.4
\begin{definition}
Per ogni $\chi \in X(T)$ sia
\[
	L(\chi) = \soc_G H^0(\chi)
\]
\end{definition}

% II.2.3
\begin{corollary}\label{Hchismp}
Sia $\chi \in X(T)$ tale che $H^0(\chi) \neq 0$, allora $L(\chi)$ è semplice.
\end{corollary}
\begin{proof}
Per~\ref{socMneq0} (ii) sappiamo che $L(\chi) \neq 0$.
Se esistessero due sottomoduli $L_1, L_2$ di $H^0(\chi)$ semplici e distinti,
ricordando l'esattezza a sinistra del funtore punto fisso, da $L_1 \oplus L_2 \subset H^0(\chi)$
si avrebbe $L_1^{U^+} \oplus L_2^{U^+} = (L_1 \oplus L_2)^{U^+} \subset H^0(\chi)^{U^+}$,
ma $L_1^{U^+}$ e $L_2^{U^+}$ sono non nulli per~\ref{VUneq0} (i) e quindi $\dim(H^0(\chi)^{U^+}) \ge 2$,
assurdo per~\ref{HdimHUchar} (i).
Concludiamo che $L(\chi)$ è semplice.
\end{proof}

\begin{theorem}\label{MsmpLchi}{\ }
\begin{enumerate}[(i)]
	\item Ogni $G$-modulo semplice è isomorfo a esattamente un $L(\chi)$ con
		$\chi \in X(T)$ tale che $H^0(\chi) \neq 0$;
	\item Sia $\chi \in X(T)$ con $H^0(\chi) \neq 0$.
		Allora $L(\chi)^{U^+} = L(\chi)_{\chi}$ e $\dim(L(\chi)^{U^+}) = 1$.
		Inoltre ogni peso $\tau$ di $L(\chi)$ soddisfa $w_0.\chi \le \tau \le \chi$.
		Infine la molteplicità di $L(\chi)$ come fattore di composizione di $H^0(\chi)$ è uguale a $1$.
\end{enumerate}
\end{theorem}
\begin{proof}

Iniziamo dimostrando il punto (ii).
Dato che $L(\chi) \neq 0$, per~\ref{VUneq0} (i) anche $L(\chi)^{U^+} \neq 0$.
Utilizzando~\ref{HdimHUchar} (i) troviamo che $1 \le \dim(L(\chi)^{U^+}) \le \dim(H^0(\chi)^{U^+}) = 1$,
da cui $L(\chi)^{U^+} = H^0(\chi)^{U^+}$, in particolare $\dim(L(\chi)^{U^+}) = 1$.
Inoltre $L(\chi)_{\chi} = L(\chi) \cap H^0(\chi)_{\chi} = L(\chi) \cap H^0(\chi)^{U^+} = L(\chi)^{U^+}$.
Un peso $\tau$ di $L(\chi)$ è anche un peso di $H^0(\chi)$, quindi per~\ref{HdimHUchar} (ii) soddisfa
$w_0.\chi \le \tau \le \chi$.

$L(\chi)$, essendo un sottomodulo semplice di $H^0(\chi)$, è un suo fattore di composizione.
Visto che $\dim(L(\chi)_{\chi}) = 1$, che $\dim(H^0(\chi)_{\chi}) = 1$ e che $V \mapsto V_{\chi}$
preserva le immersioni, in quanto funtore esatto a sinistra, e le somme dirette, si evince che
la molteplicità di $L(\chi)$ nella serie di zoccoli di $H^0(\chi)$ non può essere maggiore di $1$.
Questo conclude la dimostrazione del punto (ii).

Per quanto riguarda il punto (i), detto $V$ il $G$-modulo semplice in questione,
per~\ref{VUneq0} (ii) esiste $\chi \in X(T)$ con $\Hom_G(V,H^0(\chi)) = \Hom_G(V,\Ind_{B^-}^G k_{\chi}) \neq 0$.
Sia $\phi \in \Hom_G(V,H^0(\chi))$ con $\phi \neq 0$, allora $\Ker(\phi)$ è un $G$-sottomodulo di $V$
diverso da $V$, quindi $\Ker(\phi) = 0$ per semplicità.
Segue che $\Img(\phi) \simeq V$ è semplice, quindi $\Img(\phi) \subset \soc_G H^0(\chi) = L(\chi)$.
Infine, dato che per~\ref{Hchismp} $L(\chi)$ è semplice e che $\Img(\phi) \neq 0$,
si ha $\Img(\phi) \simeq L(\chi)$, perciò $V \simeq L(\chi)$.

Se per assurdo l'unicità non fosse verificata, esisterebbero $\chi, \chi' \in X(T)$ con
$L(\chi) \simeq L(\chi')$, ma allora
\[
	L(\chi)_{\chi} = L(\chi)^{U^+} = L(\chi')^{U^+} = L(\chi')_{\chi'} = L(\chi)_{\chi'}
\]
e $L(\chi)_{\chi} \neq 0$, assurdo in quanto due spazi-peso distinti hanno intersezione banale.

\end{proof}

\medskip
Lo scopo della restante parte della sezione è dimostrare che $H^0(\chi) \neq 0$ se e solo se $\chi$ è dominante,
proprietà che definiamo subito:

% II.2.6
\subsection
Sia
\begin{align*}
X(T)_+
&= \{ \chi \in X(T) \mid \langle \chi, \alpha^{\vee} \rangle \ge 0 \quad \forall \alpha \in R^+ \}\\
&= \{ \chi \in X(T) \mid \langle \chi, \alpha^{\vee} \rangle \ge 0 \quad \forall \alpha \in D \}
\end{align*}
Gli elementi di $X(T)_+$ sono chiamati \textit{pesi dominanti} di $T$ rispetto $R^+$.

\medskip
Enunciamo, senza dimostrazione, il teorema di estensione di Hartogs, un risultato puramente
di geometria algebrica che utilizzeremo nella dimostrazione finale nella forma data dal successivo corollario.
Si ricordi la definizione di varietà normale, data in~\ref{DomVarNorm}.

\begin{lemma}[Teorema di estensione di Hartogs]\label{HartogExt}
Sia $H$ una varietà algebrica normale e $K$ un sottoinsieme chiuso di $H$ di codimensione $\ge 2$.
Se $f$ è una funzione regolare definita su $H \setminus K$, allora $f$ si estende ad una funzione regolare su $H$.
\end{lemma}

Data una radice $\alpha \in D$ poniamo $U_{\sim \alpha}^+ = \prod_{\beta \in R^+ \setminus \{\alpha\}} U_{\beta}$.
Si noti che $U_{\sim \alpha}^+$ è un sottogruppo di $U^+$ e $U^+ = U_{\sim \alpha}^+ U_{\alpha} \simeq U_{\sim \alpha}^+ \times U_{\alpha}$
come varietà per~\ref{UwGroup} e~\ref{saDPermRp}.

\begin{corollary}\label{GrpExt}
Sia $f$ una funzione regolare definita su $U^+ B^- \cup \bigcup_{\alpha \in D} \dot{s_{\alpha}} U^+ B^-$,
allora $f$ si estende a una funzione regolare su tutto $G$.
\end{corollary}
\begin{proof}
Per~\ref{GSpOmoNorm} $G$ è una varietà normale.
Ricordiamo che $U^+ B^-$ è un aperto denso di $G$ per~\ref{Cw0OpDen},
quindi $\dot{s_{\alpha}} U^+ B^-$ ($\alpha \in D$) sono aperti,
e di conseguenza $U^+ B^- \cup \bigcup_{\alpha \in D} \dot{s_{\alpha}} U^+ B^-$ è un aperto di $G$.
Da~\ref{saDPermRp} e~\ref{Wactua} si evince che $\dot{s_{\alpha}}$ normalizza $U_{\sim \alpha}^+$, quindi
\begin{align*}
	U^+ \dot{s_{\alpha}} B^-
	&= U_{\sim \alpha}^+ U_{\alpha} \dot{s_{\alpha}} B^-\\
	&= \dot{s_{\alpha}} (\dot{s_{\alpha}}^{-1} U_{\sim \alpha}^+ \dot{s_{\alpha}}) (\dot{s_{\alpha}}^{-1} U_{\alpha} \dot{s_{\alpha}}) B^-\\
	&= \dot{s_{\alpha}} U_{\sim \alpha}^+ U_{-\alpha} B^-\\
	&= \dot{s_{\alpha}} U_{\sim \alpha}^+ B^-\\
	&\subset \dot{s_{\alpha}} U^+ B^-
\end{align*}
Detto $H = G \setminus \left( U^+ B^- \cup \bigcup_{\alpha \in D} \dot{s_{\alpha}} U^+ B^-\right)$,
sappiamo che $H$ è un chiuso di $G$, e ricordando~\ref{BrhLmm} si trova
\[
	H
	\subset G \setminus \left( U^+ B^- \cup \bigcup_{\alpha \in D} U^+ \dot{s_{\alpha}} B^-\right)
	= \bigcup_{w \in W \setminus (\{e\} \cup \{s_{\alpha} \mid \alpha \in D\})} U^+ \dot{w} B^-
\]
Per~\ref{dimCw} ognuna delle classi laterali al membro di destra ha codimensione $\ge 2$,
quindi anche la loro unione e di conseguenza $H$ ha codimensione $\ge 2$.
A questo punto possiamo applicare il teorema di estensione di Hartogs alla funzione $f$
ottenendo un'estensione regolare su tutto $G$.
\end{proof}

\medskip
Siamo giunti alla dimostrazione più importante della tesi, ma anche alla più difficile.
Nella seguente proposizione il punto particolarmente interessante è quello che afferma che
se $\chi$ è un carattere dominante allora $H^0(\chi) \neq 0$.
L'idea della dimostrazione è costruire in modo semi-esplicito un elemento non nullo di $H^0(\chi)$:
si parte definendo una funzione non nulla sull'aperto denso $U^+B^-$, la si estende su $\dot{s_{\alpha}} U^+ B^-$
per ogni $\alpha \in D$ ed infine, attraverso il teorema di Hartogs, si ottiene una funzione definita su tutto $G$,
che si verificherà appartenere a $H^0(\chi)$.

\begin{proposition}\label{chidomHneq0}
Sia $\chi \in X(T)$.
Le seguenti affermazioni sono equivalenti:
\begin{enumerate}[(a)]
	\item $\chi$ è dominante;
	\item $H^0(\chi) \neq 0$;
	\item Esiste un $G$-modulo $V$ con $(V^{U^+})_{\chi} \neq 0$.
\end{enumerate}
\end{proposition}
\begin{proof}{\ }
\begin{enumerate}
	\item [(c) $\Rightarrow$ (b)]
		Sia $v \in (V^{U^+})_{\chi}$ con $v \neq 0$, allora per~\ref{GlocfinM} $v$ è contenuto in un $G$-sottomodulo $V'$ di $V$
		di dimensione finita, da cui $(V'^{U^+})_{\chi} \neq 0$.
		Consideriamo una serie di zoccoli $\soc_G^i V'$ di $V'$.
		Dato che $V'$ ha dimensione finita esiste $n \in \Nb$ con $\soc_G^n V' = V'$, dunque $v \notin \soc_G^0 V' = 0$ mentre $v \in \soc_G^n V'$.
		Sia $j \in \Nb$ il più piccolo numero per cui $v \notin \soc_G^{j-1} V'$ e $v \in \soc_G^j V'$,
		allora $[v] \in \soc_G^j V' / \soc_G^{j-1} V' = \soc_G (V' / \soc_G^{j-1} V')$ e $[v] \neq 0$.
		Inoltre $[v]$ è fissato da $U^+$ e appartiene allo spazio-peso associato a $\chi$ esattamente come $v$,
		quindi $(\soc_G (V' / \soc_G^{j-1} V')^{U^+})_{\chi} = ((\soc_G^j V' / \soc_G^{j-1} V')^{U^+})_{\chi} \neq 0$.
		Per definizione $\soc_G (V' / \soc_G^{j-1} V')$ è somma diretta di $G$-moduli semplici, diciamo
		$\soc_G (V' / \soc_G^{j-1} V') = V_1 \oplus \dots \oplus V_k$, allora
		\begin{align*}
			0
			&\neq (\soc_G (V' / \soc_G^{j-1} V')^{U^+})_{\chi}\\
			&= ((V_1 \oplus \dots \oplus V_k)^{U^+})_{\chi}\\
			&= (V_1^{U^+} \oplus \dots \oplus V_k^{U^+})_{\chi}\\
			&= (V_1^{U^+})_{\chi} \oplus \dots \oplus (V_k^{U^+})_{\chi}
		\end{align*}
		Ne deduciamo che esiste un $G$-modulo semplice $V_i$ con $(V_i^{U^+})_{\chi} \neq 0$,
		quindi possiamo ricondurci al caso $V$ semplice.

		Per~\ref{MsmpLchi} (i) $V$ è isomorfo a $L(\tau)$ per un qualche $\tau \in X(T)$
		tale che $H^0(\tau) \neq 0$.
		Utilizzando~\ref{MsmpLchi} (ii) si trova che
		\[
			0 \neq (V^{U^+})_{\chi} = (L(\tau)^{U^+})_{\chi} = (L(\tau)_{\tau})_{\chi}
		\]
		Dato che due spazi-peso distinti hanno intersezione banale, si deve avere $\tau = \chi$,
		da cui $H^0(\chi) \neq 0$.

	\item [(b) $\Rightarrow$ (c)]
		Prendiamo $V = H^0(\chi)$, allora per~\ref{HdimHUchar} (i)
		\[
			(V^{U^+})_{\chi} = (H^0(\chi)^{U^+})_{\chi} = (H^0(\chi)_{\chi})_{\chi} = H^0(\chi)_{\chi} = H^0(\chi)^{U^+}
		\]
		che, sempre per~\ref{HdimHUchar} (i), è non nullo in quanto ha dimensione positiva.

	\item [(b) $\Rightarrow$ (a)]
		Dato che $H^0(\chi) \neq 0$, per~\ref{HdimHUchar} (i) si ha che $(H^0(\chi))_{\chi} \neq 0$.
		Da~\ref{SpPesoConj} segue che per ogni $\alpha \in R^+$, $s_{\alpha}.\chi$ è un peso di $H^0(\chi)$, dunque
		$s_{\alpha}.\chi \le \chi$ per~\ref{HdimHUchar} (ii).
		Ma $s_{\alpha}.\chi = \chi - \langle \chi, \alpha^{\vee} \rangle \alpha$,
		segue che $\langle \chi, \alpha^{\vee} \rangle \alpha \ge 0$ e quindi
		$\langle \chi, \alpha^{\vee} \rangle \ge 0$, cioè $\chi$ è dominante.

	\item [(a) $\Rightarrow$ (b)]
		Ricordando la caratterizzazione di $H^0(\chi)$ data in~\ref{h0chi}, ci basta trovare una funzione regolare
		$f : G \to k$ non nulla tale che $f(gtu) = \chi(t)^{-1} f(g)$ ($g \in G, t \in T, u \in U^-$).

		Definiamo una funzione regolare $f_{\chi}$ su $U^+ T U^-$, che ricordiamo essere un aperto denso per~\ref{Cw0OpDen},
		ponendo $f_{\chi}(u_1 t u_2) = \chi(t)^{-1}$ ($u_1 \in U^+, t \in T, u_2 \in U^-$).
		Si vede immediatamente che la definizione è ben posta e che $f_{\chi} \neq 0$.
		Supponiamo che per ogni radice semplice $\alpha \in D$ la restrizione di $f_{\chi}$ all'insieme
		$\dot{s_{\alpha}} U^+ B^- \cap U^+ B^-$ si estenda a una funzione regolare su $\dot{s_{\alpha}} U^+ B^-$.
		Prese $\alpha, \beta \in D$, le estensioni di $f_{\chi}$ agli insiemi $\dot{s_{\alpha}} U^+ B^-$ e $\dot{s_{\beta}} U^+ B^-$
		coincidono su $\dot{s_{\alpha}} U^+ B^- \cap \dot{s_{\beta}} U^+ B^-$ in quanto coincidono su
		$\dot{s_{\alpha}} U^+ B^- \cap \dot{s_{\beta}} U^+ B^- \cap U^+ B^-$ e quest'ultimo è un aperto denso,
		visto che la proprietà di essere un aperto denso viene conservata dalle riflessioni semplici e
		dalle intersezioni finite.
		Dunque è possibile estendere $f_{\chi}$ a una funzione regolare su
		$U^+ B^- \cup \bigcup_{\alpha \in D} \dot{s_{\alpha}} U^+ B^-$.
		Utilizzando~\ref{GrpExt} possiamo estendere ulteriormente $f_{\chi}$ su tutto $G$.
		Fissati $t \in T$ e $u \in U^-$, per ogni $g = u_1 t' u_2 \in U^+ T U^-$ vale l'uguaglianza
		\[
			f_{\chi}(gtu)
			= f_{\chi}(u_1 (t t') ((t'^{-1} u_1 t') u_2))
			= \chi(t t')^{-1} = \chi(t)^{-1} f_{\chi}(u_1 t' u_2)
			= \chi(t)^{-1} f_{\chi}(g)
		\]
		segue che tale relazione è soddisfatta anche sulla chiusura $\overline{U^+ T U^-} = G$.
		Dall'arbitrarietà di $t$ e $u$ si conclude che $f_{\chi}$ è un elemento non nullo di $H^0(\chi)$.

		Data una radice semplice $\alpha \in D$, è sufficiente mostrare che $f_{\chi}$ ristretta all'insieme
		$\dot{s_{\alpha}} U^+ B^- \cap U^+ B^-$ si estende a una funzione regolare su $\dot{s_{\alpha}} U^+ B^-$.
		Visto che $\dot{s_{\alpha}}$ normalizza $U_{\sim \alpha}^+$ per~\ref{saDPermRp} e~\ref{Wactua}, si ha
		\[
			\dot{s_{\alpha}} U^+ B^- = U_{\sim \alpha}^+ \dot{s_{\alpha}} U_{\alpha} B^-
		\]
		Sia $u_{\alpha}$ un isomorfismo tra $k$ e $U_{\alpha}$ come descritto in~\ref{uaUaTgenG}
		e similmente definiamo $u_{-\alpha}$.
		La mappa $\phi : U_{\sim \alpha}^+ \times k \times T \times U^- \to \dot{s_{\alpha}} U^+ B^-$ definita da
		$\phi(u_1, x, t, u) = u_1 \dot{s_{\alpha}} u_{\alpha}(x) t u$ è un isomorfismo di varietà
		per il risultato precedente, per~\ref{TGuisoG} e dato che $U^+ \cap B^- = \{0\}$.

		Utilizzando~\ref{SL2toG} è possibile scegliere $u_{\alpha}$, $u_{-\alpha}$ in modo che esista un omomorfismo
		$\psi_{\alpha} : \SLb_2 \to G$ con
		\[
			u_{\alpha}(x) = \psi_{\alpha} \begin{pmatrix} 1 & x \\ 0 & 1 \end{pmatrix} \qquad
			u_{-\alpha}(x) = \psi_{\alpha} \begin{pmatrix} 1 & 0 \\ x & 1 \end{pmatrix} \qquad
			\alpha^{\vee}(x) = \psi_{\alpha} \begin{pmatrix} x & 0 \\ 0 & x^{-1} \end{pmatrix}
		\]
		inoltre, detto $n_{\alpha} = u_{\alpha}(1) u_{-\alpha}(-1) u_{\alpha}(1)$, si ha
		$n_{\alpha} \in N_G(T)$ e l'immagine di $n_{\alpha}$ in $W$ è $s_{\alpha}$.

		Allora
		\begin{align*}
			n_{\alpha} u_{\alpha}(x)
			&= u_{\alpha}(1) u_{-\alpha}(-1) u_{\alpha}(1) u_{\alpha}(x)
			= \psi_{\alpha} \begin{pmatrix} 0 & 1 \\ -1 & -x \end{pmatrix}\\
			&= u_{\alpha}(-x^{-1}) \alpha^{\vee}(-x^{-1}) u_{-\alpha}(x^{-1})
		\end{align*}
		per ogni $x \in k^*$.

		Scegliendo $\dot{s_{\alpha}} = n_{\alpha}$ si vede che
		\begin{align*}
			\phi(u_1, x, t, u)
			&= u_1 \dot{s_{\alpha}} u_{\alpha}(x) t u\\
			&= u_1 u_{\alpha}(-x^{-1}) \alpha^{\vee}(-x^{-1}) u_{-\alpha}(x^{-1}) t u\\
			&= u_1 u_{\alpha}(-x^{-1}) \alpha^{\vee}(-x^{-1}) t u_{-\alpha}((-\alpha)(t^{-1}) x^{-1}) u\\
			&= (u_1 u_{\alpha}(-x^{-1})) ((\alpha^{\vee}(-x^{-1}) t) u_{-\alpha}(\alpha(t) x^{-1}) u)\\
			&\in U^+ B^-
		\end{align*}
		per ogni $u_1 \in U_{\sim \alpha}^+, x \in k^*, t \in T, u \in U^-$, da cui
		\[
			f_{\chi}(u_1 \dot{s_{\alpha}} u_{\alpha}(x) t u) =
			\chi(\alpha^{\vee}(-x^{-1}) t)^{-1} =
			\chi(t)^{-1} \chi(\alpha^{\vee}(-x)) =
			\chi(t)^{-1} (-x)^{\langle \chi, \alpha^{\vee} \rangle}
		\]
		Dato che $\langle \chi, \alpha^{\vee} \rangle \ge 0$ per ipotesi,
		possiamo estendere il dominio di $f_{\chi}$ da
		$U_{\sim \alpha}^+ \times k^* \times T \times U^-$ a $U_{\sim \alpha}^+ \times k \times T \times U^-$,
		cioè a $\dot{s_{\alpha}} U^+ B^-$, concludendo la dimostrazione.
\end{enumerate}
\end{proof}

\medskip
Unendo i vari risultati ottenuti si ottiene il teorema di Borel-Weil:

\begin{corollary}[Borel-Weil]\label{borelweil}
L'insieme $\{ L(\chi) \mid \chi \in X(T)_+ \}$ fornisce un sistema di rappresentanti
per le classi di isomorfismo dei $G$-moduli semplici.
\end{corollary}
\begin{proof}
Il corollario discende direttamente da~\ref{Hchismp}, da~\ref{MsmpLchi} (i) e da~\ref{chidomHneq0}.
\end{proof}

\begin{remark}
Nessuna delle dimostrazioni di questa sezione, o anche di questo capitolo, utilizza direttamente il fatto che $k$ abbia caratteristica $0$.
In effetti tale ipotesi viene utilizzata esclusivamente in alcuni lemmi riguardanti la struttura di $G$ nella prima parte della tesi,
con lo scopo di semplificare le dimostrazioni ma senza essere strettamente necessaria.
Sviluppata una teoria sui gruppi algebrici lineari in caratteristica $p$ analoga a quella in caratteristica $0$,
le dimostrazioni e i teoremi presentati, in particolare quello di Borel-Weil, rimangono validi.
\end{remark}

\begin{remark}
In caratteristica $0$ si può ottenere un risultato aggiuntivo, cioè che tutti i $G$-moduli sono
semi-semplici, da cui $L(\chi) = H^0(\chi)$ per $\chi \in X(T)$.
Questo fatto, oltre a semplificare l'enunciato del teorema di Borel-Weil, ci permetterà di trovare
dei rappresentanti alternativi in modo puramente geometrico, come vedremo nella prossima sezione.
\end{remark}
