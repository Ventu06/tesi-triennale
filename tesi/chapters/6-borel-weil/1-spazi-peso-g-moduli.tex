Questa breve sezione costituisce un'introduzione alla successiva, dove si dimostrerà il teorema di Borel-Weil.
Qui fisseremo alcune notazioni, definiremo gli spazi-peso per moduli e proveremo qualche fatto a loro riguardo
di interesse generale.

\medskip
Sia $G$ un gruppo algebrico lineare connesso riduttivo e sia $T$ un suo toro massimale.
Sia $R$ il sistema di radici di $(G,T)$ e consideriamo un sistema di radici positive $R^+ \subset R$.
Sia $D$ il sottoinsieme di $R^+$ costituito dalle radici semplici.

Per~\ref{BuGenUa} e~\ref{TUaGenB} possiamo considerare i sottogruppi $U^+$ e $U^-$, dove $U^+$ è generato da $U_{\alpha}$ con $\alpha \in R^+$
e $U^-$ è generato da $U_{\alpha}$ con $\alpha \in -R^+$.
Siano inoltre $B^+ = U^+T$ e $B^- = U^-T$ due sottogruppi di Borel, con $U^+$ e $U^-$ parti unipotenti rispettivamente di $B^+$ e $B^-$ sempre
per i teoremi appena citati.

\subsection
Ricordiamo che ogni $T$-modulo $M$ ammette una decomposizione $M = \bigoplus_{\chi \in X(T)} M_{\chi}$ per~\ref{GdiagiffXfiniffRonedim}.
I caratteri per cui $M_{\chi}$ è non nullo si chiamano pesi di $M$, sottintendendo la rappresentazione di $T$ su $M$.

Diciamo che un peso $\chi$ è massimale se lo è nell'ordinamento indotto dalla scelta del sistema di radici positive,
cioè se $\chi + \sum_{\alpha \in R^+} n_{\alpha} \alpha$ non è un peso per $M$ per nessun $n_\alpha \in \Nb$
al variare di $\alpha \in R^+$.

\begin{lemma}\label{SpPesoConj}
Sia $M$ un $G$-modulo, allora $\dot{w}.M_{\chi} = M_{w.\chi}$ per $w \in W$, $\chi \in X(T)$,
in particolare se $\chi$ è un peso di $M$, allora anche $w.\chi$ è un peso di $M$ per $w \in W$.
\end{lemma}
\begin{proof}

Per $t \in T$ e $m \in M_{\chi}$ si ha
\[
	t.(\dot{w}.m) =
	\dot{w}.((\dot{w}^{-1} t \dot{w}).m) =
	\dot{w}.(\chi(\dot{w}^{-1} t \dot{w}) m) =
	((w.\chi)(t))(\dot{w}.m)
\]
cioè $\dot{w}.m \in M_{w.\chi}$, da cui $\dot{w}.M_{\chi} \subset M_{w.\chi}$.
In modo simile si ha $\dot{w}^{-1}.M_{w.\chi} \subset M_{\chi}$, da cui l'uguaglianza
$\dot{w}.M_{\chi} = M_{w.\chi}$.
\end{proof}


\begin{lemma}
Sia $M$ un $G$-modulo, $\chi$ un suo peso e $m \in M_{\chi}$.
Per ogni $u \in U^+$ possiamo scrivere $u.m = m + m_1 + \dots + m_k$ con $m_j \in M_{\chi_j}$ e $\chi_j > \chi$.
\end{lemma}
\begin{proof}
Per~\ref{BuGenUa} possiamo scrivere $u = u_{\alpha_1}(x_1) \dots u_{\alpha_k}(x_k)$ dove $R^+ = \{ \alpha_1, \dots, \alpha_k \}$,
quindi $u.m = u_{\alpha_1}(x_1).(u_{\alpha_2}(x_2).(\dots (u_{\alpha_k}(x_k).m) \dots))$.
Assumendo la tesi vera per $u \in U_{\alpha}$ per ogni $\alpha \in R^+$ e applicandola più volte nel modo suggerito
dalla precedente espressione si verifica il caso generale, quindi è sufficiente restringersi al caso $u = u_{\alpha}(x)$.

$M$ è naturalmente un $U_{\alpha}$ modulo e la relativa azione è descritta dalla mappa di comodulo
$\Delta_M : M \to M \otimes k[U_{\alpha}]$.
Dato che $k \simeq U_{\alpha}$ tramite la mappa $u_{\alpha}$, possiamo identificare $k[U_{\alpha}]$ con l'anello di polinomi $k[T]$.
In questo modo, se $\Delta_M(m) = \sum_{i \ge 0} m_i \otimes T^i$ (con $m_i \neq 0$ solo per un numero finito di indici), allora
\[
	u_{\alpha}(x).m = \Delta_M(m)(x) = \sum_{i \ge 0} x^i m_i
\]
Ponendo $x=0$ si ottiene $m_0 = m$.

Prendiamo $t \in T$ e notiamo che
\begin{align*}
	\sum_{i \ge 0} x^i (t.m_i)
	&= t.(u_{\alpha}(x).m)\\
	&= (t u_{\alpha}(x) t^{-1}).(t.m)\\
	&= \chi(t) (u_{\alpha}(\alpha(t)x).m)\\
	&= \sum_{i \ge 0} \chi(t) (\alpha(t)x)^i m_i\\
	&= \sum_{i \ge 0} (\chi + i\alpha)(t) x^i m_i
\end{align*}
da cui
\[
	\sum_{i \ge 0} x^i (t.m_i - (\chi + i\alpha)(t)m_i) = 0
\]
Dovendo quest'uguaglianza valere per ogni $x \in k$, si deduce che $t.m_i = (\chi + i\alpha)(t)m_i$ per ogni $i$.
Dall'arbitrarietà di $t \in T$ segue che $m_i \in M_{\chi + i\alpha}$ e naturalmente $\chi + i\alpha > \chi$ per $i > 0$,
concludendo la dimostrazione.

\end{proof}

% II.1.19.7
\begin{corollary}\label{MlsubMU}
Sia $M$ un $G$-modulo e sia $\chi$ un suo peso massimale, allora
\[
	M_{\chi} \subset M^{U^+}
\]
\end{corollary}
\begin{proof}
Siano $m \in M_{\chi}$ e $u \in U^+$, allora applicando il precedente lemma e sfruttando la massimalità di $\chi$
otteniamo $u.m = m$, segue $M_{\chi} \subset M^{U^+}$.
\end{proof}

\subsection
Per ogni carattere $\chi \in X(T)$ possiamo costruire un'azione di $T$ su $k$ data da
$t.x = \chi(t)x$.
Denotiamo il $T$-modulo così ottenuto con $k_{\chi}$.
Nel caso in cui $\chi$ è il carattere banale, cioè $\chi(t) = 1$ per ogni $t$,
$T$ agisce banalmente su $k$ e indichiamo tale $T$-modulo semplicemente con $k$.

\subsection
Ricordiamo che $U^-$ è normalizzato da $T$, di conseguenza è un sottogruppo normale di $U^-T = B^-$.
Possiamo quindi considerare la mappa quoziente $\pi : B^- \to B^-/U^- \simeq T$.
Componendo un carattere $\chi \in X(T)$ con $\pi$ otteniamo il carattere $\chi \circ \pi$ di $B$,
che indicheremo sempre con $\chi$ senza specificare ogni volta l'estensione del dominio.
Si può procedere allo stesso modo con $B^+$ al posto di $B^-$.

In maniera simile si può rendere ogni $T$-modulo un $B^-$-modulo o un $B^+$-modulo, semplicemente
componendo la rappresentazione con la mappa $\pi$.
