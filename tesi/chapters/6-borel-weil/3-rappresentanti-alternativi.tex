In questa sezione enunceremo una formulazione alternativa del teorema di Borel-Weil, più in particolare
forniremo dei rappresentanti alternativi per le classi di isomorfismo dei $G$-moduli semplici,
ottenuti mediante una costruzione del tutto diversa da quella mostrata in precedenza.
Tale costruzione è, in un certo senso, più geometrica, in quanto riguarda le sezioni di un fibrato lineare,
cioè un fibrato vettoriale di dimensione uno, sullo spazio quoziente $G/B^-$.
Inoltre è la formulazione più nota di questo teorema, in parte perché esiste un teorema analogo
per le rappresentazioni di gruppi di Lie formulato in questo modo.

\medskip
Il primo obbiettivo della sezione è definire $E_{\chi}$ per ogni carattere $\chi$, fibrato lineare di cui andremo a considerare le sezioni.

\medskip
Il sottogruppo di Borel $B^-$ è chiuso per definizione, quindi per~\ref{GquozHexun} esiste il quoziente $G/B^-$,
il quale ha la struttura di varietà quasi proiettiva.
Sia $\pi_{G/B^-} : G \to G/B^-$ la relativa proiezione.
Si ricordi inoltre la definizione di sezione locale data in~\ref{locsec}.

\begin{lemma}\label{GBmLocSec}
$\pi_{G/B^-}$ ammette sezioni locali.
\end{lemma}
\begin{proof}
In modo simile a come fatto nella dimostrazione di~\ref{GrpExt}, notiamo che $U^+ \dot{w} B^- \subset \dot{w} U^+ B^-$ per $w \in W$.
Dato che $U^+ \dot{w} B^-$ ricoprono $G$ per~\ref{BrhLmm} e che $U^+ B^-$ è aperto per~\ref{Cw0OpDen},
$\dot{w} U^+ B^-$ costituisce un ricoprimento finito di aperti di $G$, saturi rispetto la proiezione $\pi_{G/B^-}$.
Segue che $\pi_{G/B^-}(\dot{w} U^+ B^-)$ è un ricoprimento aperto finito di $G/B^-$

Supponiamo esista una sezione locale $\sigma_e : \pi_{G/B^-}(U^+ B^-) \to G$, allora per ogni $w \in W$, possiamo costruire una sezione locale
$\sigma_w : \pi_{G/B^-}(\dot{w} U^+ B^-) \to G$ con $\sigma_w([u]) = \dot{w} \sigma_e([u])$.

Procediamo a costruire $\sigma_e$.
Per~\ref{AhxBisoCw} (ii) si ha $U^+ \times B^- \simeq U^+ B^-$ tramite la mappa $\phi(u,b) = ub$,
allora $\pi_{G/B^-}(U^+ B^-) \simeq (U^+ B^-)/B^- \simeq (U^+ \times B^-)/B^- \simeq U^+$ tramite $[u] \mapsto u$.
Utilizzando l'isomorfismo appena trovato possiamo porre $\sigma_e([u]) = \phi(u,e)$.
\end{proof}

Sia $\chi \in X(T)$ un carattere di $T$.
Rendiamo $G$ un $B^-$-spazio facendo agire $B^-$ come traslazione destra.

Ricordando~\ref{GxHXexist}, possiamo considerare il fibrato di $G/B^-$ associato a $k_{\chi}$:
\[
	E_{\chi} = G \times_{B^-} k_{\chi} = (G \times k_{\chi}) / B^-
\]
cioè lo spazio prodotto quozientato per l'azione di $B^-$, la quale agisce singolarmente sui due spazi.
In altre parole, se $[(g_1,z_1)], [(g_2,z_2)] \in E_{\chi}$ e $[(g_1,z_1)] = [(g_2,z_2)]$,
allora esiste $b \in B^-$ con $(g_1,z_1) = (g_2 b^{-1}, \chi(b) z_2)$.
Indichiamo con $\pi_{E_{\chi}}$ la proiezione $G \times k_{\chi} \to E_{\chi}$.

\medskip
Definiamo una struttura di $G$-spazio su $G/B^-$ e su $E_{\chi}$, nel primo caso mediante l'azione $h.[g] = [hg]$
e nel secondo caso mediante l'azione $h.[(g,z)] = [(hg,z)]$.
Entrambe le azioni sono razionali per definizione di quoziente, vedasi~\ref{quoz} e~\ref{GxHXexist}.

\medskip
Consideriamo la mappa $\pi_{\chi} : E_{\chi} \to G/B^-$ definita da $\pi_{\chi}([(g,z)]) = [g]$.
Si verifica facilmente che è ben definita, suriettiva ed equivariante per l'azione di $G$.
Per quanto riguarda la regolarità, detta $\pi_1 : G \times k_{\chi} \to G$ la proiezione sulla prima componente,
si vede che $\pi_{G/B^-} \circ \pi_1$ è un morfismo invariante per l'azione di $B^-$, quindi passa al quoziente
definendo il morfismo $\pi_{\chi} : E_{\chi} \to G/B^-$.

\medskip
Introduciamo ora l'insieme delle sezioni $\Gamma(G/B^-, E_{\chi})$, spazio che si rivelerà essere isomorfo a $H^0(\chi)$,
fornendo il collegamento cercato con il teorema di Borel-Weil.

\begin{definition}
Sia $\Gamma(G/B^-, E_{\chi})$ l'insieme delle sezioni del fibrato lineare $\pi_{\chi}$:
\[
	\Gamma(G/B^-, E_{\chi}) = \{ \sigma : G/B^- \to E_{\chi} \mid \sigma \text{ regolare}, \ \pi_{\chi} \circ \sigma = \Id_{G/B^-} \}
\]
\end{definition}

$\Gamma(G/B^-, E_{\chi})$ ha una naturale struttura di spazio vettoriale, con le operazioni di somma e moltiplicazione per scalare
definite puntualmente sulle fibre: date $\sigma_1, \sigma_2 \in \Gamma(G/B^-, E_{\chi})$, $[g] \in G/B^-$ e $r \in k$ con
$\sigma_1([g]) = [(g,z_1)]$ e $\sigma_2([g]) = [(g,z_2)]$, poniamo $(\sigma_1 + \sigma_2)([g]) = [(g,z_1+z_2)]$ e $(r \sigma_1)([g]) = [(g,rz_1)]$.
Si verifica facilmente che $\sigma_1 + \sigma_2$ e $r\sigma_1$ sono ben definite e sono elementi di $\Gamma(G/B^-, E_{\chi})$.

\medskip
Il seguente è il teorema centrale di questa sezione e motiva le costruzioni effettuate finora.

\begin{theorem}
$\Gamma(G/B^-, E_{\chi})$ e $H^0(\chi)$ sono isomorfi come moduli.
\end{theorem}
\begin{proof}

Ricordiamo che per~\ref{h0chi}
\[
	H^0(\chi) = \{ f \in k[G] \mid f(gb) = \chi(b)^{-1}f(g) \quad \forall g \in G, b \in B^- \}
\]

Definiamo la mappa lineare $\xi : H^0(\chi) \to \Gamma(G/B^-, E_{\chi})$ ponendo $\xi(f)([g]) = [(g, f(g))]$.
Affinché sia un omomorfismo di moduli, è necessario che $\xi$ sia ben definita,
lineare e che effettivamente abbia immagine in $\Gamma(G/B^-, E_{\chi})$.

La linearità è immediata, notiamo poi che $\xi$ è ben definita, in quanto
\begin{align*}
	\xi(f)([gb])
	&= [(gb, f(gb))]\\
	&= [(g(b^{-1})^{-1}, \chi(b^{-1})f(g))]\\
	&= [(g, f(g))]\\
	&= \xi(f)([g])
\end{align*}

Mostriamo che $\xi(f) \in \Gamma(G/B^-, E_{\chi})$ per ogni $f \in H^0(\chi)$.
Si vede subito che $(\pi_{\chi} \circ \xi(f))([g]) = \pi_{\chi}([g, f(g)]) = [g]$, cioè che $\pi_{\chi} \circ \xi(f) = \Id_{G/B^-}$,
resta da dimostrare che $\xi(f)$ è una funzione regolare.
Consideriamo il morfismo $\pi_{E_{\chi}} \circ (\Id,f) : G \to E_{\chi}$ e notiamo che è invariante per l'azione di $B^-$,
quindi definisce al quoziente il morfismo $\xi(f) : G/B^- \to E_{\chi}$.

Mostriamo l'iniettività di $\xi$: presa $f \in \Ker(\xi)$, abbiamo che
$\xi(f)([g])=0$ per ogni $g \in G$, cioè $f(g)=0$ per ogni $g \in G$,
dunque $f=0$, da cui $\Ker(\xi)=0$.

Mostriamo la suriettività di $\xi$: data $\sigma \in \Gamma(G/B^-, E_{\chi})$,
procediamo a costruire $f \in H^0(\chi)$ tale che $\xi(f) = \sigma$.
Per $g \in G$, $\sigma([g])$ è una classe di equivalenza di elementi di $G \times k_{\chi}$, di cui uno e uno solo
della forma $(g, x_g)$ per qualche $x_g \in k_{\chi}$.
Poniamo $f(g) = x_g$ e procediamo a mostrare che $f \in k[G]$, che $f(gb) = \chi(b)^{-1}f(g)$
per ogni $g \in G$, $b \in B^-$ e che $\xi(f) = \sigma$.
Per la seconda verifica, si noti che $[gb, x_{gb}] = [g, x_g] = [gb, \chi(b)^{-1}x_g]$, da cui $f(gb) = \chi(b)^{-1}f(g)$,
mentre per l'ultima basta vedere che $\xi(f)([g]) = [(g,f(g))] = [(g,x_g)] = \sigma([g])$.
Concentriamoci ora sulla regolarità di $f$.

Ricordiamo che $\dot{w}U^+B^-$ al variare di $w \in W$ costituisce un ricoprimento aperto finito di $G$,
così come $\dot{w}U^+B^- \times k_{\chi}$ è un ricoprimento aperto finito di $G \times k_{\chi}$.
Da~\ref{AhxBisoCw} (ii) sappiamo che $U^+ \times B^- \simeq U^+B^-$ attraverso la moltiplicazione,
Vediamo poi che $\pi_{G/B^-}(\dot{w}U^+B^-) \simeq (\dot{w}U^+ \times B^-)/B^- \simeq \dot{w}U^+$ e
$\pi_{E_{\chi}}(\dot{w}U^+B^- \times k_{\chi}) \simeq (\dot{w}U^+ \times B^- \times k_{\chi})/B^- \simeq \dot{w}U^+ \times k_{\chi}$
al variare di $w \in W$ sono ricoprimenti aperti rispettivamente di $G/B^-$ e $E_{\chi}$:
per quest'ultimo, si ricordi la costruzione di $E_{\chi}$ descritta in~\ref{GxHXexist} mediante le sezioni locali costruite in~\ref{GBmLocSec}.

Restringendo la mappa $\sigma$ a $\pi_{G/B^-}(\dot{w}U^+B^-)$, tramite gli isomorfismi mostrati si ottiene una mappa da
$\dot{w}U^+$ in $\dot{w}U^+ \times k_{\chi}$, che indicheremo sempre con $\sigma$.
Consideriamo il seguente diagramma, di cui si verifica facilmente la commutatività:
\[
\begin{tikzcd}
	{\dot{w}U^+B^-} & {\dot{w}U^+B^- \times k_{\chi}} \\
	{\dot{w}U^+ \times B^-} & {\dot{w}U^+ \times k_{\chi} \times B^-}
	\arrow["{(\sigma,\Id)}", from=2-1, to=2-2]
	\arrow["{(\Id,f)}", from=1-1, to=1-2]
	\arrow["\simeq", from=1-1, to=2-1]
	\arrow["\simeq", from=1-2, to=2-2]
\end{tikzcd}
\]
Ne deduciamo che $(\Id,f)|_{\dot{w}U^+B^-}$ è un morfismo su $\dot{w}U^+B^- \times k_{\chi}$ per ogni $w \in W$,
quindi si conclude che anche $f$ è un morfismo.

Unendo i pezzi, si conclude che $\xi$ è un isomorfismo di moduli fra $H^0(\chi)$ e $\Gamma(G/B^-, E_{\chi})$.

\end{proof}

\medskip
Definiamo una struttura di $G$-modulo su $\Gamma(G/B^-, E_{\chi})$:
consideriamo l'azione definita da
\[
	(h.\sigma)([g]) = h.(\sigma(h^{-1}.[g]))
\]

Che quest'azione manda sezioni in sezioni è facile da vedere:
$h \circ \sigma \circ h^{-1} : G/B^- \to E_{\chi}$ è un morfismo in quanto composizione di morfismi e
\[
	\pi_{\chi} \circ h \circ \sigma \circ h^{-1} = h \circ \pi_{\chi} \circ \sigma \circ h^{-1} = h \circ h^{-1} = \Id_{G/B^-}
\]

Vediamo poi che $\xi$ è $G$-equivariante, in quanto
\begin{align*}
	\xi(h.f)([g])
	&= [(g, (h.f)(g))]\\
	&= [(g, f(h^{-1}g))]\\
	&= h.[(h^{-1}g, f(h^{-1}g))]\\
	&= h.(\xi(f)([h^{-1}g]))\\
	&= (h.\xi(f))([g])
\end{align*}

L'isomorfismo fra $H^0(\chi)$ e $\Gamma(G/B^-, E_{\chi})$ ci permette di definire una struttura di $G$-modulo su $\Gamma(G/B^-, E_{\chi})$
a partire da quella di $H^0(\chi)$, la cui azione è proprio quella appena mostrata vista l'equivarianza di $\xi$.

\medskip
Possiamo ora rafforzare il precedente teorema enunciando un risultato che ci permetterà di presentare la formulazione alternativa del teorema di Borel-Weil.

\begin{corollary}
$\Gamma(G/B^-, E_{\chi})$ e $H^0(\chi)$ sono isomorfi come $G$-moduli.
\end{corollary}
\begin{proof}
Segue immediatamente dal precedente teorema e dalle successive osservazioni.
\end{proof}

\medskip
Introduciamo i rappresentanti alternativi:

\begin{definition}
Per ogni $\chi \in X(T)$ sia
\[
	V_{\chi} = \soc_G \Gamma(G/B^-, E_{\chi})
\]
\end{definition}

\medskip
Dal precedente corollario segue immediatamente che $V_{\chi} \simeq L_{\chi}$,
quindi otteniamo il seguente:

\begin{theorem}[Borel-Weil]{\ \\}
L'insieme $\{ V_{\chi} \mid \chi \in X(T)_+ \}$ fornisce un sistema di rappresentanti
per le classi di isomorfismo dei $G$-moduli semplici.
\end{theorem}
\begin{proof}
Il teorema è conseguenza diretta di~\ref{borelweil} e del precedente risultato.
\end{proof}

\begin{remark}
Come osservato nella precedente sezione, in caratteristica $0$ tutti i $G$-moduli sono semi-semplici,
quindi $V_{\chi} = \Gamma(G/B^-, E_{\chi})$ per ogni $\chi \in X(T)$.
\end{remark}
