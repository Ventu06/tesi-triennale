In questa sezione assoceremo ad ogni elemento del gruppo di Weyl una successione di riflessioni semplici
più corta possibile necessaria per scrivere tale elemento, detta decomposizione ridotta.
Vedremo le relazioni di tale successione e della sua lunghezza con alcuni sottoinsiemi di radici
e studieremo come l'azione delle riflessioni semplici influenza la decomposizione.
Alla fine della sezione mostreremo due proposizioni che forniscono una presentazione del gruppo di Weyl:
pur non essendo necessaria ai successivi sviluppi della teoria, rimane un risultato interessante su un oggetto
che abbiamo utilizzato e continueremo ad utilizzare per il resto della tesi, nonché costituisce una
degna conclusione di questo capitolo.

\medskip
Introduciamo gli insiemi $R(w)$, che vedremo essere strettamente legati alla decomposizione ridotta di $w$.

% 8.3.1
\subsection\label{Rw}
Dato $w \in W$, poniamo
\[
	R(w) = \{ \alpha \in R^+ \mid w.\alpha \in -R^+ \}
\]

\begin{proposition}\label{RSymOnWeyl}
Per $\alpha \in D$
\begin{align*}
	R(w s_{\alpha}) &= s_{\alpha}.R(w) \cup \{\alpha\} & &\text{se } w.\alpha \in R^+\\
	R(w s_{\alpha}) &= s_{\alpha}.(R(w) \setminus \{\alpha\}) & &\text{se } w.\alpha \in -R^+
\end{align*}
\end{proposition}
\begin{proof}
Supponiamo $w.\alpha \in R^+$, allora $w s_{\alpha}.\alpha = w.(-\alpha) = - w.\alpha \in - R^+$;
dato che $\alpha \notin R(w)$, $s_{\alpha}.R(w) \subset R^+$, inoltre $w s_{\alpha}.(s_{\alpha}.R(w)) = w.R(w) \subset -R^+$,
da cui $s_{\alpha}.R(w) \cup \{\alpha\} \subset R(w s_{\alpha})$.

Se invece $w.\alpha \in -R^+$, $s_{\alpha}.(R(w) \setminus \{\alpha\}) \subset R^+ \setminus \{\alpha\} \subset R^+$,
inoltre $w s_{\alpha}.(s_{\alpha}.(R(w) \setminus \{\alpha\})) = w.(R(w) \setminus \{\alpha\}) \subset -R^+$,
da cui $s_{\alpha}.(R(w) \setminus \{\alpha\}) \subset R(w s_{\alpha})$.

Notiamo che $w.\alpha \in R^+$ se e solo se $w s_{\alpha}.\alpha \in -R^+$, e a meno di sostituire $\alpha$ con $s_{\alpha}.\alpha = -\alpha$ possiamo assumere che $w.\alpha \in R^+$.
Allora
\[
	R(w s_{\alpha}) =
	s_{\alpha}.s_{\alpha}.(R(w s_{\alpha}) \setminus \{\alpha\}) \cup \{\alpha\} \subset
	s_{\alpha}.R(w s_{\alpha}^2) \cup \{\alpha\} =
	s_{\alpha}.R(w) \cup \{\alpha\} \subset
	R(w s_{\alpha})
\]
mostrando che entrambi i contenimenti sono uguaglianze vista la bigettività di $s_{\alpha}$ e il fatto che $\alpha \notin s_{\alpha}.R(w)$.
\end{proof}

\medskip
Ricordiamo che $W$ è generato da $S$ per~\ref{WTransGenS} (iii).

\begin{definition}[Lunghezza e Decomposizione Ridotta]{\ \\}
Dato $w \in W$, la \textit{lunghezza} $l(w)$ di $w$ (relativamente ad $S$) è il più piccolo intero $h \ge 0$ tale che $w$ si possa esprimere come prodotto di $h$ elementi di $S$.
Una \textit{decomposizione ridotta} di $w$ è una sequenza $\textbf{s} = (s_1, \dots, s_h)$ in $S$ con $w = s_1 \dots s_h$ e $h = l(w)$.
\end{definition}

\medskip
Il seguente è il risultato centrale di questa sezione.

% 8.3.2
\begin{lemma}\label{PropDec}
Sia $\textbf{s} = (s_1, \dots, s_h)$ una decomposizione ridotta di $w \in W$ dove $s_i = s_{\alpha_i}$ ($\alpha_i \in D$).
\begin{enumerate}[(i)]
	\item $R(w) = \{ \alpha_h, s_h.\alpha_{h-1}, \dots, s_h \dots s_2.\alpha_1 \}$;
	\item Il numero di elementi di $R(w)$ è $l(w)$;
	\item Dato $\alpha \in D$, $l(w s_{\alpha}) = l(w)+1$ se $w.\alpha \in R^+$ e $l(w s_{\alpha}) = l(w)-1$ se $w.\alpha \in -R^+$;
		dato $\alpha \in D$, $l(s_{\alpha} w) = l(w)+1$ se $w^{-1}.\alpha \in R^+$ e $l(s_{\alpha} w) = l(w)-1$ se $w^{-1}.\alpha \in -R^+$.
	\item Dato $\alpha \in D$ tale che $w.\alpha \in -R^+$, esiste una decomposizione ridotta di $w$ il cui elemento finale è $s_{\alpha}$;
	\item Data una decomposizione ridotta $\textbf{s}' = (s_1', \dots, s_h')$ di $w$, esiste $i$ con $1 \le i \le h$ tale che $s_1 \dots s_{i-1} s_{i+1} \dots s_h = s_1' \dots s_{h-1}'$.
\end{enumerate}
\end{lemma}
\begin{proof}{\ }
\begin{enumerate}[(i)]
	\item la tesi è banalmente vera per $h=0$, mentre per $h=1$ discende da~\ref{saDPermRp}.
		Sia $h>1$ e sia $w' = s_1 \dots s_{h-1}$.
		Per ipotesi induttiva si ha che $R(w') = \{ \alpha_{h-1}, s_{h-1}.\alpha_{h-2}, \dots, s_{h-1} \dots s_{w}.\alpha_1 \}$.
		Se $w' \alpha_h \in R^+$ allora (i) vale per~\ref{RSymOnWeyl}.
		Se invece $w' \alpha_h \in -R^+$ avremmo che $\alpha_h \in R(w')$, quindi $\alpha_h = s_{h-1} \dots s_{i+1}.\alpha_i$ per un qualche $i$.
		Segue che
		\begin{align*}
			s_h
			&= s_{\alpha_h}
			= s_{s_{h-1} \dots s_{i+1}.\alpha_i}
			= (s_{h-1} \dots s_{i+1}) s_{i} (s_{h-1} \dots s_{i+1})^{-1}\\
			&= s_{h-1} \dots s_{i+1} s_{i} s_{i+1} \dots s_{h-1}
		\end{align*}
		da cui
		\[ w = s_1 \dots s_h = s_1 \dots s_{h-1} (s_{h-1} \dots s_{i+1} s_{i} s_{i+1} \dots s_{h-1}) = s_1 \dots s_{i-1} s_{i+1} \dots s_{h-1} \]
		assurdo per minimalità di $h$;
	\item per (i) il numero di elementi di $R(w)$ è uguale alla cardinalità della decomposizione ridotta di $w$, cioè a $l(w)$;
	\item la parte su $w s_{\alpha}$ discende direttamente da (ii) e~\ref{RSymOnWeyl}. Per la seconda parte abbiamo
		$l(s_{\alpha} w) = l((s_{\alpha} w)^{-1}) = l(w^{-1} s_{\alpha})$ e $l(w) = l(w^{-1})$, riconducendoci dunque al primo caso;
	\item per definizione $\alpha \in R(w)$, quindi per (i) esiste $i$ tale che $\alpha = s_h \dots s_{i+1}.\alpha_i$.
		Segue che
		\begin{align*}
			w s_{\alpha}
			&= s_1 \dots s_h s_{s_h \dots s_{i+1}.\alpha_i}
			= s_1 \dots s_h (s_h \dots s_{i+1}) s_i (s_h \dots s_{i+1})^{-1}\\
			&= s_1 \dots s_{i-1} s_{i+1} \dots s_h
		\end{align*}
		da cui $w = s_1 \dots s_{i-1} s_{i+1} \dots s_h s_{\alpha}$ è la decomposizione ridotta cercata;
	\item dato che $l(w s_h') = l(w)-1$, $w.\alpha_h' \in -R^+$, quindi procedendo come nella dimostrazione di (iv)
		troviamo $i$ tale che $s_1' \dots s_h' = w = s_1 \dots s_{i-1} s_{i+1} \dots s_h s_h'$.
		Eliminando $s_h'$ in entrambi i membri si ottiene la tesi.
\end{enumerate}
\end{proof}

\medskip
La seguente proposizione è l'ingrediente fondamentale del successivo teorema, dove verrà fornita una presentazione di $W$.

\medskip
Dati $s,t \in S$ con $s \neq t$, indichiamo con $m(s,t)$ l'ordine di $st$ in $W$.
Notiamo che $m(s,t) = m(t,s)$ per~\ref{alphabetavalues} (ii).

% 8.3.3
\begin{proposition}\label{ExtMon}
Sia $\mu$ una mappa da $S$ in un monoide (scriveremo l'operazione moltiplicativamente) con le seguenti proprietà:
\begin{enumerate}[(a)]
	\item $\mu(s)^2 = 1$ ($s \in S$);
	\item se $s,t \in S$ con $s \neq t$, allora $(\mu(s)\mu(t))^{m(s,t)} = 1$.
\end{enumerate}
Allora esiste un'unica estensione di $\mu$ a tutto $W$ che sia un omomorfismo.
\end{proposition}
\begin{proof}

Per ogni $w \in W$, presa una decomposizione ridotta $\textbf{s} = (s_1, \dots, s_h)$, poniamo
\[
	\mu(w) = \mu(s_1) \dots \mu(s_h)
\]
È sufficiente mostrare che $\mu(s_1) \dots \mu(s_h)$ è indipendente dalla scelta della decomposizione ridotta di $w$ per ottenere la tesi.
Infatti siano $r,s,t \in W$ tali che $rs=t$ e consideriamo delle decomposizioni ridotte $\textbf{r} = (r_1, \dots, r_h)$ e $\textbf{s} = (s_1, \dots, s_k)$ rispettivamente di $r$ e $s$.
Se $l(r)+l(s)=l(t)$, concatenando $\textbf{r}$ e $\textbf{s}$ otteniamo una decomposizione ridotta di $t$, così da verificare $\mu(r) \mu(s) = \mu(t)$.
Se invece $l(r)+l(s)>l(t)$, sia $j$, con $1 \le j \le k$ il primo indice per cui $l(r s_1 \dots s_j) < l(r)+j$.
Siano $r' = r s_1 \dots s_{j-1}$ e $s' = s_j \dots s_k$ e consideriamo le loro decomposizioni ridotte $\textbf{r}' = (r_1, \dots, r_h, s_1, \dots, s_{j-1})$ e $\textbf{s}' = (s_j, \dots, s_k)$.
Allora $\mu(r')\mu(s') = \mu(r)\mu(s)$ e $r's'=t$, quindi a meno di cambiare i fattori possiamo assumere $l(r s_1) < l(r)+1$.

Utilizzando~\ref{PropDec} (iii), (iv) troviamo una decomposizione ridotta di $r$ il cui ultimo elemento è $s_1$, decomposizione che possiamo assumere aver scelto come $\textbf{r}$.
Siano $r' = r_1 \dots r_{h-1}$ e $s' = s_2 \dots s_k$ con decomposizioni ridotte $\textbf{r}' = (r_1, \dots, r_{h-1})$ e $\textbf{s}' = (s_2, \dots, s_k)$, allora $rs = (r_1 \dots r_{h-1} s_1)(s_1 s_2 \dots s_k) = (r_1 \dots r_{h-1})(s_2 \dots s_k) = r's'$ e $\mu(r)\mu(s) = \mu(r_1) \dots \mu(r_{h-1}) \mu(s_1)^2 \mu(s_2) \dots \mu(s_k) = \mu(r') \mu(s_1)^2 \mu(s') = \mu(r') \mu(s')$.
Possiamo quindi sostituire $r$ e $s$ con $r'$ e $s'$, e notando che $l(r')+l(s')<l(r)+l(s)$ riusciamo ad iterare il procedimento arrivando in un numero finito di passaggi al caso $l(r)+l(s)=l(t)$.

Procediamo ora a dimostrare l'indipendenza di $\mu(w)$ dalla decomposizione ridotta scelta.
Siano $\textbf{s} = (s_1, \dots, s_h)$, $\textbf{s}' = (s_1', \dots, s_h')$ due decomposizioni ridotte di $w$.
Si procede per induzione su $h = l(w)$.
I casi $h=0,1$ sono banali in quanto la decomposizione ridotta è unica, possiamo quindi assumere $h>1$.

Per~\ref{PropDec} (v) esiste $i$ tale che
$s_1 \dots s_{i-1} s_{i+1} \dots s_h = s_1' \dots s_{h-1}'$, allora
$s_1 \dots s_{i-1} s_{i+1} \dots s_h s_h' = s_1' \dots s_{h-1}' s_h' = s_1 \dots s_h$ da cui
$s_{i+1} \dots s_h s_h' = s_i \dots s_h$.
Allo stesso modo esiste $i'$ tale che
$s_1' \dots s_{i'-1}' s_{i'+1}' \dots s_h' = s_1 \dots s_{h-1}$, da cui
$s_{i'+1} \dots s_h' s_h = s_i' \dots s_h'$.

Se $i>1$, $s_i \dots s_h$ ha lunghezza $h-i+1<h$, perciò per ipotesi induttiva
\[
	\mu(s_{i+1}) \dots \mu(s_h) \mu(s_h') = \mu(s_i) \dots \mu(s_h)
\]
Dato che $s_1' \dots s_{h-1}'$ ha lunghezza $h-1<h$, utilizzando la precedente uguaglianza e l'ipotesi induttiva si trova
\begin{align*}
	\mu(\textbf{s}')
	&= \mu(s_1') \dots \mu(s_{h-1}') \mu(s_h')
	= \mu(s_1) \dots \mu(s_{i-1}) \mu(s_{i+1}) \dots \mu(s_h) \mu(s_h')
	= \mu(s_1) \dots \mu(s_h)\\
	&= \mu(\textbf{s})
\end{align*}
Allo stesso modo se $i'>1$ si trova che $\mu(\textbf{s}') =	\mu(\textbf{s})$.

Resta da trattare il caso $i=i'=1$.
Siano $\widehat{\textbf{s}} = (s_2, \dots, s_h, s_h')$ e $\widehat{\textbf{s}}' = (s_2', \dots, s_h', s_h)$, allora
\[
	\widehat{w} =
	s_2 \dots s_h s_h' =
	s_1' \dots s_{h-1}' s_h' =
	s_1 \dots s_{h-1} s_h =
	s_2' \dots s_h' s_h =
\]
e, per ipotesi induttiva, come già fatto in precedenza,
\begin{align*}
	\mu(\textbf{s}') &=	\mu(s_1') \dots \mu(s_{h-1}') \mu(s_h') = \mu(s_2) \dots \mu(s_h) \mu(s_h') = \mu(\widehat{\textbf{s}})\\
	\mu(\textbf{s}) &= \mu(s_1) \dots \mu(s_{h-1}) \mu(s_h) = \mu(s_2') \dots \mu(s_h') \mu(s_h) = \mu(\widehat{\textbf{s}}')
\end{align*}
Segue che $\mu(\textbf{s}) = \mu(\textbf{s}')$ se e solo se $\mu(\widehat{\textbf{s}}) = \mu(\widehat{\textbf{s}}')$.

Se $l(\widehat{w}) < l(w)$ allora $l(\widehat{w}) = l(w)-2$ e utilizzando~\ref{PropDec} (iii), (iv) troviamo una decomposizione ridotta
$\widetilde{\textbf{s}} = (\widetilde{s_1}, \dots, \widetilde{s}_{h-2}, s_h')$ di $s_2 \dots s_h$ il cui ultimo elemento è $s_h'$ e una decomposizione ridotta
$\widetilde{\textbf{s}}' = (\widetilde{s_1}', \dots, \widetilde{s}'_{h-2}, s_h)$ di $s_2' \dots s_h'$ il cui ultimo elemento è $s_h$.
Allora
\begin{align*}
	\mu(\widehat{\textbf{s}}) &= \mu(\widetilde{\textbf{s}}) \mu(s_h') = \mu(\widetilde{s}_1, \dots, \widetilde{s}_{h-2}) \mu(s_h')^2 = \mu(\widetilde{s}_1, \dots, \widetilde{s}_{h-2})\\
	\mu(\widehat{\textbf{s}}') &= \mu(\widetilde{\textbf{s}}') \mu(s_h) = \mu(\widetilde{s}'_1, \dots, \widetilde{s}'_{h-2}) \mu(s_h)^2= \mu(\widetilde{s}'_1, \dots, \widetilde{s}'_{h-2})
\end{align*}
quindi $\mu(\widehat{\textbf{s}}) = \mu(\widehat{\textbf{s}}')$ se e solo se
$\mu(\widetilde{s}_1, \dots, \widetilde{s}_{h-2}) = \mu(\widetilde{s}'_1, \dots, \widetilde{s}'_{h-2})$, vero per ipotesi induttiva.

Se invece $l(\widehat{w}) = l(w)$, possiamo ricondurci al caso $\textbf{s} = (\dots, s, t)$ e $\textbf{s}' = (\dots, t, s)$ con $s=s_h$ e $t=s_h'$.
Iterando il ragionamento ci riconduciamo a dimostrare il caso $\textbf{s} = (\dots, t, s, t)$ e $\textbf{s}' = (\dots, s, t, s)$
fino poi ad arrivare a decomposizioni ridotte composte alternatamente solo da $s$ e $t$ di lunghezza $d$.
Possiamo riscrivere $\underbrace{sts \dots}_{d \text{\ fattori}} = \underbrace{tst \dots}_{d \text{\ fattori}}$ come $(st)^d=1$ e la tesi
$\mu(s)\mu(t) \dots = \mu(t)\mu(s) \dots$ equivale a $(\mu(s)\mu(t))^d = 1$.
Ma $m(s,t) \mid d$ per definizione, quindi per ipotesi $(\mu(s)\mu(t))^d = ((\mu(s)\mu(t))^{m(s,t)})^\frac{d}{m(s,t)} = 1$.

\end{proof}

\medskip
Concludiamo il capitolo caratterizzando completamente $W$.

% 8.3.4
\begin{theorem}
L'insieme dei generatori $S$ e le relazioni $s^2 = 1$, $(st)^{m(s,t)} = 1$ ($s,t \in S$, $s \neq t$) costituiscono una presentazione di $W$.
\end{theorem}
\begin{proof}
Sia $\widetilde{W}$ il gruppo definito dai generatori $\widetilde{s}$ ($s \in S$) e dalle relazioni $\widetilde{s}^2=1$, $(\widetilde{s}\widetilde{t})^{m(s,t)} = 1$ ($s,t \in S$, $s \neq t$).
Dato che $W$ rispetta le relazioni che definiscono $\widetilde{W}$, esiste un omomorfismo $\pi : \widetilde{W} \to W$ con $\pi(\widetilde{s}) = s$,
inoltre grazie a~\ref{ExtMon} abbiamo un omomorfismo $\mu : W \to \widetilde{W}$ con $\mu(s) = \widetilde{s}$.
Notiamo che $\pi \circ \mu$ e $\mu \circ \pi$ sono l'identità sui generatori $S$ e $\{\widetilde{s} \mid s \in S\}$, quindi sono le mappe identità sugli interi gruppi,
da cui concludiamo che $\pi$ è un isomorfismo.
\end{proof}
