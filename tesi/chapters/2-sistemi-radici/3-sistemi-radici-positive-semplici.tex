In questa sezione svilupperemo ulteriormente al teoria sui sistemi di radici.
Preso un sistema di radici positive gli assoceremo un suo sottoinsieme costituito dalle cosiddette
radici semplici, il quale costituirà una base del nostro sistema di radici.
Questo ci permetterà di verificare svariate proprietà riguardanti il gruppo di Weyl,
dal fatto che è generato dalle riflessioni rispetto le radici semplici alla transitività
dell'azione sui sistemi di radici positive.

\medskip
Sia $R^+$ un sistema di radici positive.

\begin{definition}[Radici Semplici]{\ \\}
Poniamo
\[
	D = D(R^+) = \{ \alpha \in R^+ \mid \alpha \neq \beta + \gamma \quad \forall \beta,\gamma \in R^+ \}
\]
Gli elementi di $D$ sono detti \textit{radici semplici} di $R^+$ e le riflessioni $s_{\alpha}$ per $\alpha \in D$
sono dette \textit{riflessioni semplici} di $R^+$ e costituiscono l'insieme $S$.
\end{definition}

\medskip
Proviamo due lemmi che utilizzeremo nelle prossime dimostrazioni.

% 8.2.7 (ii) - Dim. diversa
\begin{lemma}\label{abDProdle0}
Siano $\alpha, \beta \in D$ con $\alpha \neq \beta$, allora $\langle \alpha, \beta^{\vee} \rangle \le 0$.
\end{lemma}
\begin{proof}
Se $\langle \alpha, \beta^{\vee} \rangle > 0$, per~\ref{sumRinR} $\alpha - \beta$ è una radice,
quindi una fra $\alpha - \beta$ e $\beta - \alpha$ è una radice positiva.
Nei due casi potremmo scrivere, rispettivamente, $\alpha$ o $\beta$ come somma di due radici positive,
assurdo visto che sono radici semplici.
\end{proof}

% 8.2.8 (iii)
\begin{lemma}\label{vixIndp}
Siano $v_1, \dots, v_n \in V$ tali che $(v_i,v_j) \le 0$ per ogni $i \neq j$ e che esista $x \in V$ con $(v_i,x) > 0$ per ogni $i$.
Allora $v_1, \dots, v_n$ sono linearmente indipendenti.
\end{lemma}
\begin{proof}
Per assurdo, consideriamo una relazione di dipendenza $\sum_{i=1}^n m_i v_i = 0$ a coefficienti reali.
Sia $I_1 = \{ 1 \le i \le n \mid m_i > 0 \}$.
A meno di cambiare segno a tutti i coefficienti, possiamo assumere $I_1 \neq \emptyset$.
Riscriviamo la relazione come
\[
	t = \sum_{i \in I_1} m_i v_i = - \sum_{j \in I \setminus I_1} m_j v_j
\]
Notiamo che $(t,t) = - \sum_{i \in I_1, j \in I \setminus J_1} m_i m_j (v_1, v_2) \le 0$.
Segue $t=0$, da cui $0 = (t,x) = \sum_{i \in I_1} m_i (v_i,x) > 0$, assurdo.
\end{proof}

\medskip
Il seguente teorema è un risultato fondamentale di questa sezione.
Da esso discende subito che $D$ è una ``base'' delle radici, e dato che queste ultime generano $V$,
il teorema afferma anche che $D$ è una base di $V$.

% HMP.10.1 + 8.2.8 (iii)
\begin{theorem}\label{Dbase}
Le radici di $D$ sono linearmente indipendenti su $V$ e ogni radice di $R^+$
è combinazione lineare di elementi di $D$ con coefficienti interi non-negativi.
\end{theorem}
\begin{proof}
Sia $x \in V$ con $(\alpha,x) \neq 0$ per ogni $\alpha \in R$ tale che $R^+ = \{ \alpha \in R \mid (\alpha,x) > 0 \}$.

Iniziamo mostrando che ogni radice di $R^+$ è combinazione lineare di elementi di $D$ con coefficienti interi non-negativi.
Per assurdo, consideriamo una radice positiva $\alpha \in R^+$ non esprimibile in tale forma che minimizza $(\alpha,x)$
(la scelta è possibile in quanto le radici sono in numero finito).
$\alpha \notin D$, dunque $\alpha = \alpha_1 + \alpha_2$ per qualche $\alpha_1, \alpha_2 \in R^+$.
Ma $(\alpha,x) = (\alpha_1,x) + (\alpha_2,x)$ e per minimalità di $\alpha$ possiamo scrivere $\alpha_1$ e $\alpha_2$
come combinazione lineare di elementi di $D$ con coefficienti interi non-negativi, dunque possiamo esprimere anche $\alpha$
in questa forma, assurdo.

Mostriamo ora che le radici di $D$ sono linearmente indipendenti.
Per~\ref{abDProdle0} si ha $(\alpha,\beta) \le 0$ per ogni $\alpha, \beta \in D$, $\alpha \neq \beta$,
quindi è sufficiente applicare~\ref{vixIndp}.
\end{proof}

\medskip
Proviamo un altro lemma che troverà utilità nelle prossime dimostrazioni.

% HMP.10.2.A
\begin{lemma}\label{aMinbInR}
Se $\alpha \in R^+ \setminus D$, allora esiste $\beta \in D$ tale che $\alpha - \beta \in R^+$.
\end{lemma}
\begin{proof}
Sia $x \in V$ con $(\alpha,x) \neq 0$ per ogni $\alpha \in R$ tale che $R^+ = \{ \alpha \in R \mid (\alpha,x) > 0 \}$.
Se $(\alpha, \beta) \le 0$ per ogni $\beta \in D$, ricordando~\ref{abDProdle0} potremmo applicare~\ref{vixIndp}
a $\{ \alpha \} \cup D$ ottenendo un insieme di vettori linearmente indipendenti, assurdo in quanto $D$ è già una base di $V$
per il precedente teorema.
Sia dunque $\beta \in D$ con $(\alpha, \beta) > 0$ e applicando~\ref{sumRinR} troviamo che $\alpha - \beta \in R$.
Se per assurdo $\alpha - \beta \in -R^+$, allora $\beta = \alpha + (\beta - \alpha)$, cioè $\beta$ è somma di due radici positive,
assurdo in quanto radice semplice.
Possiamo quindi concludere che $\alpha - \beta \in R^+$.
\end{proof}

\medskip
Il seguente lemma ha una particolare importanza in quanto verrà ampiamente utilizzato in varie dimostrazioni,
anche al di fuori di questa sezione.

% 8.2.7 (i)
\begin{lemma}\label{saDPermRp}
Sia $\alpha \in D$, allora $s_{\alpha}$ permuta gli elementi di $R^+ \setminus \{\alpha\}$;
\end{lemma}
\begin{proof}
Sia $\beta \in R^+ \setminus \{ \alpha \}$ e scriviamo $\beta = \sum_{\gamma \in D} m_{\gamma} \gamma$ con $m_{\gamma} \in \Nb$.
$\beta$ non è multiplo di $\alpha$, quindi esiste $\delta \neq \alpha$ con $m_{\delta} > 0$.
Dato che $s_{\alpha}.\beta \in R$, possiamo scrivere $s_{\alpha}.\beta = \sum_{\gamma \in D} n_{\gamma} \gamma$
con $n_{\gamma}$ tutti non-negativi o non-positivi a seconda che $s_{\alpha}.\beta$ sia o meno una radice positiva.
Ma $s_{\alpha}.\beta = \beta - \langle \alpha, \beta^{\vee} \rangle \alpha$, quindi $n_{\delta} = m_{\delta} > 0$,
il che conclude $s_{\alpha}.\beta \in R^+$.
\end{proof}

\medskip
Concludiamo la sezione con altri due risultati fondamentali:
le seguenti proposizioni ci descrivono importanti proprietà dell'azione del gruppo di Weyl sulle radici
e su insiemi di queste ultime, stabiliscono inoltre che tale gruppo è generato dalle riflessioni semplici.

% 8.2.4 (ii)
\begin{proposition}\label{WTransGenS}{\ }
\begin{enumerate}[(i)]
	\item Siano $R^+$, $R'^+$ sistemi di radici positive in $R$, allora esiste $w \in W$
		tale che $R'^+ = w.R^+$ e $D(R'^+) = w.D(R^+)$;
	\item Sia $\alpha \in R$, allora esiste $w \in W$ tale che $w.\alpha \in D$;
	\item $W$ è generato da $S$, cioè da $\{ s_{\alpha} \mid \alpha \in D \}$;
\end{enumerate}
\end{proposition}
\begin{proof}
Sia $W'$ il sottogruppo di $W$ generato da $\{ s_{\alpha} \mid \alpha \in D \}$.
Dimostriamo i primi due punti per $W'$, poi proviamo che $W' = W$.
\begin{enumerate}[(i)]
	\item Siano $x,y \in V$ con $(\beta,x) \neq 0 \neq (\beta,y)$ per ogni $\beta \in R$ tali che
		$R^+ = \{ \beta \in R \mid (\beta,x) > 0 \}$ e $R'^+ = \{ \beta \in R \mid (\beta,y) > 0 \}$.
		Sia $\delta = \frac{1}{2} \sum_{\alpha \in R^+} \alpha$ e scegliamo $w' \in W'$ che massimizzi $(\delta, w'^{-1}.y)$,
		possibile in quanto $W' < W$ è finito per~\ref{Wfin}.
		Sia $\alpha \in D = D(R^+)$, allora
		\[
			(\delta, w'^{-1}.y)
			\ge (\delta, s_{\alpha} w'^{-1}.y)
			= (s_{\alpha}.\delta, w'^{-1}.y)
			= (\delta - \alpha, w'^{-1}.y)
		\]
		dove l'ultimo passaggio è vero per~\ref{saDPermRp}.
		Troviamo dunque che $(w'.\alpha, y) = (\alpha, w'^{-1}.y) \ge 0$, e non potendo valere l'uguaglianza per ipotesi,
		si ha $w'.\alpha \in R'^+$.
		Dovendo valere per ogni $\alpha \in D$, vale anche $w'.D \subset R'^+$ e quindi $w'.R^+ \subset R'^+$.
		Visto che $\#|w'.R^+| = \#|R^+| = \frac{1}{2} \#|R| = \#|R'^+|$, troviamo che $w'.R^+ = R'^+$.
		Sia $\alpha \in R^+$, allora
		\begin{align*}
			\alpha \in D(R^+)
			&\iff \alpha \neq \beta + \gamma \quad \forall \beta,\gamma \in R^+\\
			&\iff w'.\alpha \neq w'.\beta + w'.\gamma \quad \forall \beta,\gamma \in R^+\\
			&\iff w'.\alpha \neq \beta + \gamma \quad \forall \beta,\gamma \in w'.R^+ = R'^+\\
			&\iff w'.\alpha \in D(R'^+)
		\end{align*}
		da cui concludiamo $D(R'^+) = w'.D(R^+)$.
	\item Per il punto (i) è sufficiente mostrare che $\alpha \in D(R'^+)$ per un qualche sistema di radici positive $R'^+$.
		Sia $U = V \setminus \bigcup_{\beta \in R^+ \setminus \{ \alpha \}} \Span(\beta)^{\perp}$,
		che notiamo essere un aperto di $V$ (con la topologia euclidea).
		Dato che $\alpha \notin \Span(\beta)$ per $\beta \in R^+ \setminus \{ \alpha \}$,
		esiste $y \in \Span(\alpha)^{\perp} \cap U$.
		In questo modo $(\alpha, y) = 0$ e $(\beta, y) \neq 0$ per ogni $\beta \in R \setminus \{ \pm \alpha \}$.
		Possiamo considerare un intorno $U'$ di $y$ contenuto in $U$ sufficientemente piccolo
		e scegliere $y'$ in $U' \setminus \Span(\alpha)^{\perp}$ in modo che
		$0 < |(\alpha, y')| < |(\beta, y')|$ per ogni $\beta \in R \setminus \{ \pm \alpha \}$.
		Inoltre, a meno di scambiare $y'$ con $-y'$, assumiamo $(\alpha, y') > 0$.
		Detto $R'^+ = \{ \gamma \in R \mid (\gamma,y') > 0 \}$,	si vede facilmente che $\alpha \in D(R'^+)$,
		concludendo anche il secondo punto.
	\item Dato che $W$ è generato da $\{ s_{\alpha} \mid \alpha \in R \}$, è sufficiente mostrare che
		$s_{\alpha} \in W'$ per ogni $\alpha \in R$.
		Per il punto (ii) esiste $w' \in W'$ con $w'.\alpha \in D$, ma allora
		\begin{align*}
			s_{w'.\alpha}(z)
			&= z - 2 (z, z)^{-1} (z, w'.\alpha) w'.\alpha\\
			&= w'.(w'^{-1}.z - 2 (w'^{-1}.z, w'^{-1}.z)^{-1} (w'^{-1}.z, \alpha) \alpha)\\
			&= w'.s_{\alpha}(w'^{-1}z)\\
			&= (w' s_{\alpha} w'^{-1})(z)
		\end{align*}
		per ogni $z \in V$, cioè $w' s_{\alpha} w'^{-1} = s_{w'.\alpha} \in W'$, da cui anche $s_{\alpha} \in W'$.
\end{enumerate}
\end{proof}

\begin{proposition}\label{wUniq}
Sia $w \in W$ tale che $w.R^+ = R^+$, allora $w = e$.
\end{proposition}
\begin{proof}
Scriviamo $w$ come prodotto del minimo numero possibile di riflessioni semplici,
cioè $w = s_{\alpha_1} \dots s_{\alpha_t}$ con $\alpha_i \in D$,
e supponiamo per assurdo $t>0$, dunque $t \ge 2$ in quanto $s_{\alpha_1}.R^+ \neq R^+$.
Sia $r_i = s_{\alpha_{i+1}} \dots s_{\alpha_{t-1}}.\alpha_t$ e notiamo che
$r_{t-1} = \alpha_t \in R^+$, mentre $r_0 = w s_{\alpha_t}.\alpha_t = - w.\alpha_t \in -R^+$.
Sia $j$ il più piccolo indice per cui $r_j \in R^+$, allora
$s_{\alpha_j}.r_j = s_{\alpha_j} s_{\alpha_{j+1}} \dots s_{\alpha_{t-1}}.\alpha_t = r_{j-1} \in -R^+$.
Per~\ref{saDPermRp} si deve avere $r_j = \alpha_j$, quindi, ricordando che $w s_{\alpha} w^{-1} = s_{w.\alpha}$
come visto nell'ultimo punto della dimostrazione della precedente proposizione, abbiamo
\begin{align*}
	s_{\alpha_j}
	&= s_{r_j}\\
	&= s_{s_{\alpha_{j+1}} \dots s_{\alpha_{t-1}}.\alpha_t}\\
	&= (s_{\alpha_{j+1}} \dots s_{\alpha_{t-1}}) s_{\alpha_t} (s_{\alpha_{j+1}} \dots s_{\alpha_{t-1}})^{-1}\\
	&= (s_{\alpha_{j+1}} \dots s_{\alpha_{t-1}}) s_{\alpha_t} (s_{\alpha_{t-1}} \dots s_{\alpha_{j+1}})
\end{align*}
da cui
\begin{align*}
	w
	&= s_{\alpha_1} \dots s_{\alpha_j} \dots s_{\alpha_t}\\
	&= s_{\alpha_1} \dots s_{\alpha_{j-1}} (s_{\alpha_{j+1}} \dots s_{\alpha_{t-1}}) s_{\alpha_t} (s_{\alpha_{t-1}} \dots s_{\alpha_{j+1}}) s_{\alpha_{j+1}} \dots s_{\alpha_t}\\
	&= s_{\alpha_1} \dots s_{\alpha_{j-1}} s_{\alpha_{j+1}} \dots s_{\alpha_{t-1}}
\end{align*}
Ma ciò è assurdo per la minimalità di $t$, dunque $w = e$.
\end{proof}
