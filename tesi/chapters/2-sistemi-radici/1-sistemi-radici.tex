Gli oggetti di studio di questo capitolo sono i sistemi di radici e i sistemi di radici positive:
si tratta di concetti combinatorici che possono essere definiti e analizzati indipendentemente dai gruppi algebrici,
ma che poi si riveleranno molto utili nello studio di questi ultimi.
Utilizzeremo notazioni già viste, come ``gruppo di Weyl'', ``radici'', \dots, per enfatizzare legami
che espliciteremo successivamente, ma ciò non deve trarre in inganno:
gli oggetti che andremo a studiare sono tutti definiti all'interno di questa sezione
e non hanno (ancora) relazioni con le precedenti.

\medskip
Sia $V$ uno spazio vettoriale reale dotato di un prodotto scalare definito positivo $(\cdot, \cdot)$.
Per ogni $x,y \in V$, $x \neq 0$ poniamo
\[
	\langle y, x^{\vee} \rangle = 2 \frac{(y,x)}{(x,x)} \qquad \text{e} \qquad s_{x}(y) = y - \langle y, x^{\vee} \rangle x
\]
Come la notazione suggerisce, possiamo vedere $x^{\vee}$ come un elemento del duale $V^{\vee}$ con
$x^{\vee}(y) = \langle y, x^{\vee} \rangle$.

\begin{definition}[Sistema di Radici]{\ \\}
Un \textit{Sistema di Radici} è una coppia $(V,R)$ con $V$ spazio vettoriale reale dotato di una forma
bilineare simmetrica definita positiva $(\cdot, \cdot)$ e $R$ sottoinsieme di $V$ per la quale:
\begin{enumerate}[(RS 1)]
	\item $R$ è finito e genera $V$, inoltre $0 \notin R$;
	\item Per ogni $\alpha \in R$ si ha $s_{\alpha}(R) = R$;
	\item Per ogni $\alpha, \beta \in R$ si ha $\langle \alpha, \beta^{\vee} \rangle \in \Zb$.
	\item Gli unici multipli di $\alpha$ in $R$ sono $\pm \alpha$, cioè se $c \in \Rb$ con $c\alpha \in R$, allora $c = \pm 1$.
\end{enumerate}
\end{definition}

Chiameremo gli elementi di $R$ \textit{radici}.

\begin{definition}[Gruppo di Weyl]{\ \\}
Se $(V,R)$ è un sistema di radici, definiamo il suo gruppo di Weyl
$W = W(V,R)$ come il gruppo di automorfismi di $V$ generato da $s_{\alpha}$ per $\alpha \in R$.
\end{definition}

Notiamo che il prodotto scalare $(\cdot, \cdot)$ è invariante per l'azione di $W$, infatti per $\alpha \in R$
\[
	(s_{\alpha}(x), s_{\alpha}(y))
	= (x - 2 (\alpha,\alpha)^{-1} (x,\alpha) \alpha, y - 2 (\alpha,\alpha)^{-1} (y,\alpha) \alpha)\\
	= (x,y)
\]

% 7.4.2
\begin{lemma}\label{Wfin}
$W$ è un gruppo finito.
\end{lemma}
\begin{proof}
Gli elementi di $W$ agiscono sulle radici permutandole, in quanto prodotto delle riflessioni $s_{\alpha}$ che, per ipotesi, permutano le radici.
Possiamo definire un omomorfismo da $W$ nel gruppo simmetrico $S_{\#|R|}$, che si verifica essere iniettivo in quanto $R$ genera $V$.
Concludiamo che $\#|W| \le \#|R|!$, in particolare $W$ è un gruppo finito.
\end{proof}

% 7.4.5
\begin{definition}[Sistema di Radici Positive]\label{RadPos}{\ \\}
Sia $(V,R)$ un sistema di radici con gruppo di Weyl $W$.
Un sottoinsieme $R^+$ di $R$ è un sistema di radici positive se esiste $x \in V$ con $(\alpha, x) \neq 0$ per ogni $\alpha \in R$ tale che
\[
	R^+ = \{ \alpha \in R \mid (\alpha, x) > 0 \}
\]
\end{definition}

\subsection\label{Rplusprops}
Notiamo che $R^+$ ha le seguenti proprietà:
\begin{enumerate}[(a)]
	\item $R$ è unione disgiunta di $R^+$ e $-R^+$;
	\item Date $\alpha, \beta \in R^+$ e $i,j \in \Nb$ tali che $i\alpha + j\beta \in R$, allora $i\alpha + j\beta \in R^+$.
\end{enumerate}

Per la prima basta notare che, se $\alpha \in R$, allora $-\alpha = s_{\alpha}(\alpha) \in R$, mentre la seconda è immediata.
