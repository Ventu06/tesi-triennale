In questa sezione proseguiremo con lo studio dei sistemi di radici,
in particolare dimostreremo alcuni teoremi riguardanti l'interazione di due radici.

Sia $(V,R)$ un sistema di radici e sia $W$ il relativo gruppo di Weyl.
Per~\ref{Wfin} $W$ è un gruppo finito.
Siano $\alpha, \beta$ radici linearmente indipendenti.

% 7.5.1
\begin{lemma}\label{alphabetavalues}{\ }
\begin{enumerate}[(i)]
	\item I possibili valori che la coppia
		$(\langle \alpha, \beta^{\vee} \rangle, \langle \beta, \alpha^{\vee} \rangle)$ può assumere sono
		$(0,0)$, $(1,1)$, $(1,2)$, $(1,3)$, $(2,1)$, $(3,1)$, $(-1,-1)$, $(-1,-2)$, $(-1,-3)$, $(-2,-1)$, $(-3,-1)$;
	\item Per i quattro possibili valori che
		$\max(|\langle \alpha, \beta^{\vee} \rangle|, |\langle \beta, \alpha^{\vee} \rangle|)$
		può assumere, cioè $0,1,2,3$, l'ordine di $s_{\alpha}s_{\beta}$ è rispettivamente $2,3,4,6$;
\end{enumerate}
\end{lemma}
\begin{proof}

Consideriamo il sottospazio $V'$ di dimensione $2$ generato da $\alpha$ e $\beta$.
Notiamo $s_{\alpha}$ e $s_{\beta}$ stabilizzano $V'$, in particolare l'azione di $s_{\alpha} s_{\beta}$ su $V'$
è descritta dalla matrice
\[
	M_{\alpha \beta} =
	\begin{pmatrix}
		\langle \alpha, \beta^{\vee} \rangle \langle \beta, \alpha^{\vee} \rangle -1 & \langle \beta, \alpha^{\vee} \rangle \\
		- \langle \alpha, \beta^{\vee} \rangle & -1
	\end{pmatrix}
\]

Dato che $W$ è finito, $s_{\alpha}s_{\beta}$ ha ordine finito, quindi gli autovalori di $M_{\alpha \beta}$ sono radici dell'unità,
coniugate in quanto la matrice ha determinante $1$.
Da ciò deduciamo anche che la somma degli autovalori, quindi la traccia, è al più $2$ in valore assoluto, da cui
$| \langle \alpha, \beta^{\vee} \rangle \langle \beta, \alpha^{\vee} \rangle -2 | \le 2$.
Gli unici casi in cui si ha l'uguaglianza sono quelli in cui i due autovalori sono uguali,
quindi entrambi $1$ o $-1$, cioè quelli in cui $M_{\alpha \beta}$ coincide con $\Id$ o con $-\Id$.
Notiamo che non è possibile ottenere il primo caso.

Se $\langle \alpha, \beta^{\vee} \rangle = 0$, dato che la matrice deve avere ordine finito, si trova
$\langle \beta, \alpha^{\vee} \rangle = 0$.
In modo simile se $\langle \beta, \alpha^{\vee} \rangle = 0$ si ha che $\langle \alpha, \beta^{\vee} \rangle = 0$.
Inoltre in questo caso $M_{\alpha \beta} = - \Id$, quindi ha ordine $2$.

Possiamo supporre nei restanti casi che
$| \langle \alpha, \beta^{\vee} \rangle \langle \beta, \alpha^{\vee} \rangle -2 | < 2$
A meno di cambiare base sostituendo $\alpha$ con $-\alpha$, mantenendo il sottospazio $V'$ e l'applicazione lineare $M_{\alpha \beta}$ invariati,
possiamo assumere che $\langle \alpha, \beta^{\vee} \rangle > 0$.

Cerchiamo i possibili valori di
$\langle \alpha, \beta^{\vee} \rangle$ e $\langle \beta, \alpha^{\vee} \rangle$
che soddisfano la precedente disuguaglianza:
\begin{enumerate}[(i)]
	\item se $\langle \alpha, \beta^{\vee} \rangle = 1$ allora $\langle \beta, \alpha^{\vee} \rangle \in \{1,2,3\}$;
	\item se $\langle \alpha, \beta^{\vee} \rangle = 2$ allora $\langle \beta, \alpha^{\vee} \rangle = 1$;
	\item se $\langle \alpha, \beta^{\vee} \rangle = 3$ allora $\langle \beta, \alpha^{\vee} \rangle = 1$;
	\item se $\langle \alpha, \beta^{\vee} \rangle \ge 4$ non ci sono soluzioni.
\end{enumerate}
Questo dimostra il punto (i).

Per ognuno dei cinque casi elencati si può verificare esplicitamente che la matrice abbia ordine finito.
Riassumiamo i vari casi e l'ordine per ognuno di essi:
\[
\begin{array}{l|cccccc}
(\langle \alpha, \beta^{\vee} \rangle, \langle \beta, \alpha^{\vee} \rangle) & (0,0) & (1,1) & (1,2) & (1,3) & (2,1) & (3,1)\\
\hline
\ord(M_{\alpha \beta}) & 2 & 3 & 4 & 6 & 4 & 6
\end{array}
\]
Questo conclude il punto (ii).

\end{proof}

% HMP.9.4
\begin{lemma}\label{sumRinR}
Siano $\alpha, \beta$ radici linearmente indipendenti.
Se $\langle \alpha, \beta^{\vee} \rangle > 0$, allora $\alpha - \beta$ è una radice.
Se $\langle \alpha, \beta^{\vee} \rangle < 0$, allora $\alpha + \beta$ è una radice.
\end{lemma}
\begin{proof}
Notiamo che la seconda affermazione discende dalla prima sostituendo $\beta$ con la radice $-\beta$,
quindi è sufficiente dimostrare la prima.
Per~\ref{alphabetavalues} (i), almeno uno fra $\langle \alpha, \beta^{\vee} \rangle$ e $\langle \beta, \alpha^{\vee} \rangle$
deve essere uguale ad $1$.
Se $\langle \alpha, \beta^{\vee} \rangle = 1$, abbiamo $\alpha - \beta = s_{\beta}.\alpha \in R$.
Se invece $\langle \beta, \alpha^{\vee} \rangle = 1$, abbiamo $\beta - \alpha = s_{\alpha}.\beta \in R$,
da cui anche $\alpha - \beta \in R$.
\end{proof}

\medskip
Consideriamo un sistema di radici positive $R^+$ in $R$.
Data $\alpha \in R$, scriviamo $\alpha > 0$ se $\alpha \in R^+$ e $\alpha < 0$ se $-\alpha \in R^+$.

% 7.5.2
\begin{lemma}\label{wawbpos}
Siano $\alpha, \beta \in R$, allora esiste $w \in W$ tale che $w.\alpha > 0$ e $w.\beta > 0$.
\end{lemma}
\begin{proof}

In questa dimostrazione verranno utilizzati più volte i risultati del lemma~\ref{alphabetavalues}.

Poniamo $a = \langle \alpha, \beta^{\vee} \rangle \langle \beta, \alpha^{\vee} \rangle$, allora
$a$ assume uno fra i valori $0,1,2,3$.

Se $a=0$ allora $\langle \alpha, \beta^{\vee} \rangle = \langle \beta, \alpha^{\vee} \rangle = 0$.
In questo caso $s_{\alpha}(\alpha) = -\alpha$, $s_{\alpha}(\beta) = \beta$,
$s_{\beta}(\alpha) = \alpha$, $s_{\beta}(\beta) = -\beta$, quindi possiamo cambiare adeguatamente
i segni di $\alpha$ e $\beta$ scegliendo $w$ fra $\Id$, $s_{\alpha}$, $s_{\beta}$ e $s_{\alpha}s_{\beta}$.

Consideriamo ora il caso $a>0$ e, senza perdita di generalità, possiamo supporre $\alpha < 0$,
dato che se $\alpha, \beta > 0$ basterebbe porre $w = \Id$.

Assumiamo che $\beta < 0$.

Se $(\langle \alpha, \beta^{\vee} \rangle, \langle \beta, \alpha^{\vee} \rangle) = (-1,-1)$
poniamo $w = s_{\alpha} s_{\beta} s_{\alpha}$ in modo che $w.\alpha = -\beta > 0$, $w.\beta = -\alpha > 0$.

Se $(\langle \alpha, \beta^{\vee} \rangle, \langle \beta, \alpha^{\vee} \rangle) = (1,1)$,
ponendo $w = s_{\alpha} s_{\beta}$ si ha che $w.\alpha = -\beta > 0$, $w.\beta = \alpha -\beta$,
mentre ponendo $w = s_{\beta} s_{\alpha}$ si ha che $w.\alpha = -\alpha + \beta$, $w.\beta = -\beta > 0$,
quindi possiamo scegliere $w$ in base al segno di $\alpha - \beta$ (che naturalmente è non nullo).

Se $a>1$ allora $s_{\alpha} s_{\beta}$ ha ordine pari.
Poniamo $w = (s_{\alpha} s_{\beta})^{\frac{1}{2} \ord(s_{\alpha} s_{\beta})}$,
in modo che $w$ abbia ordine $2$ e la matrice associata abbia determinante $1$,
quindi coincide con $-\Id$, da cui $w.\alpha = -\alpha > 0$, $w.\beta = -\beta > 0$.

Consideriamo ora il caso $\beta > 0$.

Se $\langle \beta, \alpha^{\vee} \rangle > 0$, ponendo $w = s_{\alpha}$ troviamo
$w.\alpha = -\alpha > 0$, $w.\beta = \beta - \langle \beta, \alpha^{\vee} \rangle \alpha > 0$.

Supponendo $\langle \alpha, \beta^{\vee} \rangle = \langle \beta, \alpha^{\vee} \rangle = -1$,
se $\alpha + \beta > 0$, possiamo nuovamente considerare $w = s_{\alpha}$
in modo che  $w.\alpha = -\alpha > 0$, $w.\beta = \beta + \alpha > 0$,
mentre se $\alpha + \beta < 0$, ponendo $w = s_{\alpha} s_{\beta}$ troviamo
$w.\alpha = \beta > 0$, $w.\beta = - (\alpha + \beta) > 0$.

Nei rimanenti casi si ha $a>1$.
Esattamente uno fra $\langle \alpha, \beta^{\vee} \rangle$ e $\langle \beta, \alpha^{\vee} \rangle$ deve essere uguale a $-1$
e, a meno di sostituire $\alpha$ e $\beta$ con $-\beta$ e $-\alpha$, lasciando invariate le assunzioni su $\alpha < 0$ e $\beta > 0$,
possiamo assumere $\langle \alpha, \beta^{\vee} \rangle = -1$.

Supponiamo $a=2$, allora
\begin{align*}
& s_{\alpha}.\alpha = -\alpha &
& s_{\beta}.\alpha = \alpha + \beta &
& s_{\alpha}s_{\beta}.\alpha = \alpha + \beta \\
& s_{\alpha}.\beta = 2\alpha + \beta &
& s_{\beta}.\beta = -\beta &
& s_{\alpha}s_{\beta}.\beta = -2\alpha - \beta
\end{align*}
Se $2\alpha + \beta > 0$ scegliamo $w = s_{\alpha}$, altrimenti se $\alpha + \beta < 0$ poniamo $w = s_{\beta}$
riconducendoci al caso già trattato in cui entrambe le radici sono negative,
infine se $2\alpha + \beta < 0$ e $\alpha + \beta > 0$ è sufficiente scegliere $w = s_{\alpha} s_{\beta}$.

Supponiamo infine $a=3$, allora
\begin{align*}
& s_{\alpha}.\alpha = -\alpha &
& s_{\beta}.\alpha = \alpha + \beta &
& s_{\alpha}s_{\beta}.\alpha = 2\alpha + \beta \\
& s_{\alpha}.\beta = 3\alpha + \beta &
& s_{\beta}.\beta = -\beta &
& s_{\alpha}s_{\beta}.\beta = -3\alpha - \beta
\end{align*}
\begin{align*}
& s_{\beta}s_{\alpha}.\alpha = -\alpha - \beta &
& s_{\alpha}s_{\beta}s_{\alpha}.\alpha = -2\alpha - \beta \\
& s_{\beta}s_{\alpha}.\beta = 3\alpha + 2\beta &
& s_{\alpha}s_{\beta}s_{\alpha}.\beta = 3\alpha + 2\beta
\end{align*}
Se $3\alpha + \beta > 0$ scegliamo $w = s_{\alpha}$, altrimenti se $\alpha + \beta < 0$ poniamo $w = s_{\beta}$
riconducendoci al caso già trattato in cui entrambe le radici sono negative.
Se $2\alpha + \beta > 0$ e $3\alpha + \beta < 0$ scegliamo $w = s_{\alpha}s_{\beta}$,
altrimenti se $\alpha + \beta > 0$ e $3\alpha + 2\beta < 0$ poniamo $w = s_{\beta}s_{\alpha}$
riconducendoci nuovamente al caso già trattato in cui entrambe le radici sono negative.
L'ultimo possibile caso è quello in cui $2\alpha - \beta < 0$ e $3\alpha + 2\beta > 0$,
nel quale è sufficiente scegliere $w = s_{\alpha}s_{\beta}s_{\alpha}$.
\end{proof}
