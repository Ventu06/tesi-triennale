In questa sezione assoceremo ad ogni radice di un gruppo algebrico lineare un sottogruppo isomorfo
a $\Gb_a$ sul quale $T$ agisce moltiplicativamente attraverso la radice.
Studieremo poi come questi sottogruppi interagiscono fra loro e in che modo sono utili per
descrivere la struttura dei sottogruppi di Borel e dell'intero gruppo.
Utilizzeremo frequentemente risultati provenienti dai sistemi di radici nelle dimostrazioni.

\medskip
Sia $G$ un gruppo algebrico lineare connesso riduttivo e sia $T$ un suo toro massimale.
Sia inoltre $(X, R, X^{\vee}, R^{\vee})$ il dato di radici di $(G,T)$.

% 8.1.1
\begin{proposition}\label{uaUaTgenG}{\ }
\begin{enumerate}[(i)]
	\item Per ogni $\alpha \in R$ esiste un isomorfismo $u_{\alpha}$ di $\Gb_a$ su un sottogruppo chiuso
		$U_{\alpha}$ di $G$ tale che $t u_{\alpha}(x) t^{-1} = u_{\alpha}(\alpha(t)x)$ ($t \in T, x \in k$),
		unico a meno di riscalamento del dominio.
		Inoltre $\Img(du_{\alpha}) = \gf_{\alpha}$ dove quest'ultimo è lo spazio-peso per il peso $\alpha$ di $T$;
	\item $T$ e gli $U_{\alpha}$ per $\alpha \in R$ generano $G$.
\end{enumerate}
\end{proposition}
\begin{proof}
Il gruppo $G_{\alpha}$ è riduttivo e ha rango semi-semplice uno per~\ref{GaProp}.
Per~\ref{UaTBperGSS1} (i) esiste $u_{\alpha}$ con le proprietà richieste.
L'unicità di $U_{\alpha}$ segue dal fatto che ogni possibile gruppo con le proprietà richieste
è contenuto in $G_{\alpha}$ per definizione di quest'ultimo
e all'interno di $G_{\alpha}$ si ha l'unicità di $U_{\alpha}$ sempre per~\ref{UaTBperGSS1} (i).

Da~\ref{GagenG} sappiamo che $G$ è generato dai gruppi $G_{\alpha}$ per $\alpha \in R$
e da~\ref{UaTBperGSS1} (iii) che $G_{\alpha}$ è generato da $T$, $U_{\alpha}$ e $U_{-\alpha}$,
concludendo la dimostrazione del secondo punto.
\end{proof}

% 8.1.2
\begin{corollary}\label{gaonedim}
Per ogni radice $\alpha \in R$ lo spazio-peso $\gf_{\alpha}$ ha dimensione uno.
\end{corollary}
\begin{proof}
Segue immediatamente dalla precedente proposizione notando che $\gf_{\alpha}$ è isomorfo a $k$ tramite $du_{\alpha}$.
\end{proof}

\medskip
Analizziamo ora le relazioni fra gli $U_{\alpha}$ e i sottogruppi di Borel.

% 8.1.3
\begin{corollary}\label{aRUaBgabdim}
Sia $B$ un sottogruppo di Borel di $G$ contenente $T$ e sia $\alpha \in R$.
\begin{enumerate}[(i)]
	\item Le seguenti proprietà sono equivalenti:
		\begin{enumerate*}[(a)]
			\item $\alpha \in R^+(B)$;
			\item $U_{\alpha} \subset B$;
			\item $\gf_{\alpha} \subset \bf$;
		\end{enumerate*}
	\item $\dim(B) = \dim(T) + \frac{1}{2}\#|R|$ e $\dim(G) = \dim(T) + \#|R|$;
\end{enumerate}
\end{corollary}
\begin{proof}
Fissata $\alpha \in R$, per definizione di $R^+(B)$ si ha che $\alpha \in R^+(B)$ se e solo se
$\alpha \in R^+(B \cap G_{\alpha})$, inoltre $U_{\alpha} \subset G_{\alpha}$ e $g_{\alpha} \subset L(G_{\alpha})$
per la precedente dimostrazione.
Ci siamo ricondotti al caso di un gruppo connesso riduttivo di rango semi-semplice uno,
caso nel quale sappiamo verificare il primo punto grazie a~\ref{UaTBperGSS1} (i).

Per quanto riguarda il secondo punto, sempre tramite $G_{\alpha}$ possiamo ricondurci a~\ref{GaRoots} e a~\ref{UaTBperGSS1} (ii)
e trovare $\bf = \tf \oplus \bigoplus_{\alpha \in R^+(B)} \gf_{\alpha}$ e $\bf = \tf \oplus \bigoplus_{\alpha \in R} \gf_{\alpha}$.
Ricordando che $\dim(B) = \dim(\bf)$, che $\dim(G) = \dim(\gf)$ e utilizzando il precedente corollario si ottiene la tesi.
\end{proof}

\medskip
Il seguente lemma tornerà utile molto spesso.

% 8.1.12
\begin{lemma}\label{Wactua}
Sia $w$ un elemento del gruppo di Weyl di $(G,T)$, rappresentato da $\dot{w} \in N_G(T)$.
Data $\alpha \in R$, esiste $c_{w,\alpha} \in k^*$ tale che $\dot{w} u_{\alpha}(x) \dot{w}^{-1} = u_{w.\alpha}(c_{w,\alpha}x)$ ($x \in k$).
\end{lemma}
\begin{proof}
Per $t \in T, x \in k$ abbiamo che
\begin{align*}
	t (\dot{w} u_{\alpha}(x) \dot{w}^{-1}) t^{-1}
	&= \dot{w} ((\dot{w}^{-1} t \dot{w}) u_{\alpha}(x) (\dot{w}^{-1} t \dot{w})^{-1}) \dot{w}^{-1}\\
	&= \dot{w} u_{\alpha}(\alpha(\dot{w}^{-1} t \dot{w}) x) \dot{w}^{-1}\\
	&= \dot{w} u_{\alpha}((w.\alpha)(t) x) \dot{w}^{-1}
\end{align*}
Grazie a~\ref{uaUaTgenG} (i) concludiamo che $\dot{w} u_{\alpha}(x) \dot{w}^{-1} = u_{w.\alpha}(c_{w,\alpha}x)$
per qualche $c_{w,\alpha} \in k^*$.
\end{proof}

\medskip
Il lemma successivo verrà subito utilizzato per dimostrare un importante teorema sulla struttura dei sottogruppi di Borel.

% 8.2.2
\begin{lemma}\label{HIsoGa}
Sia $H$ un gruppo algebrico lineare connesso e risolubile, siano $H_u$ il suo radicale unipotente e $S$ un suo toro massimale.
Si assuma che esistano isomorfismi $v_i$ ($1 \le i \le n$) da $\Gb_a$ su sottogruppi chiusi di $H$ tali che
\begin{enumerate}[(a)]
	\item esistono caratteri $\beta_i$ di $S$ non banali, a coppie linearmente indipendenti, con $s v_i(x) s^{-1} = v_i(\beta_i(s) x)$ ($s \in S$, $x \in k$, $1 \le i \le n$);
	\item gli spazi-peso $\hf_{\beta_i}$ ($1 \le i \le n$) hanno dimensione $1$ e il loro $\Span$ è $L(H_u)$.
\end{enumerate}
Allora il morfismo $\psi : \Gb_a^n \to H_u$ dato da $\psi(x_1, \dots, x_n) = v_1(x_1) \dots v_n(x_n)$ è un isomorfismo di varietà.
\end{lemma}
\begin{proof}

Procediamo per induzione su $n$.

Se $n=1$ si ha
\[
	\dim(H_u) = \dim(L(H_u)) = \dim(\hf_{\beta_1}) = 1 = \dim(\Gb_a) = \dim(\Img(v_1))
\]
da cui $\Img(v_1) = H_u$, quindi $\psi$ è un isomorfismo.

Assumiamo $n>1$ e consideriamo, come in~\ref{GainGu}, un sottogruppo normale chiuso $N$ di $H$ isomorfo a $\Gb_a$ contenuto nel centro di $H_u$.
Allora $L(N)$ è un sottospazio di $L(H_u)$ stabile per l'azione di $S$ e di dimensione $\dim(N) = \dim(\Gb_a) = 1$.
Considerando l'azione di $S$ su $L(N)$ e il fatto che i caratteri sono tutti distinti deduciamo che
$L(N)$ deve coincidere con un qualche spazio-peso $\hf_{\beta_j}$.
Ma allora $L(N) = L(\Img(v_j))$, da cui $N = \Img(v_j)$ per~\ref{SubalgThenSubgrp}.

Consideriamo il gruppo quoziente $H/N$, come definito in~\ref{GquozHNormGAL}, e per $i \neq j$ sia $w_i : \Gb_a \to H/N$ l'omomorfismo indotto da $v_i$.
Mostriamo che le ipotesi del teorema valgono anche per $H/N$ con i morfismi $w_i$, in modo da poter applicare l'ipotesi induttiva.

Per~\ref{TGuisoG} $H \simeq S \times H_u$, quindi $H/N \simeq S \times (H_u/N)$ e di conseguenza $S$ è un toro massimale di $H/N$ e $(H/N)_u = H_u/N$.
Si vede immediatamente che $s w_i(x) s^{-1} = w_i(\beta_i(s) x)$ per $s \in S$, $x \in k$, $i \neq j$, mentre l'indipendenza lineare
e la non-banalità dei caratteri $\beta_i$ ($i \neq j$) è vera per ipotesi.
Inoltre per~\ref{GquozHLie} $L((H/N)_u) = L(H_u/N) = L(H_u) / L(N)$, di conseguenza gli spazi-peso $\hf_{\beta_i}$ ($i \neq j$) hanno dimensione $1$
e il loro $\Span$ è $L((H/N)_u)$.

La preimmagine di $\Img(w_i)$ in $H$ è $\Img(v_i) N$, sottogruppo chiuso in quanto prodotto di sottogruppi chiusi
per~\ref{GroupProd}, quindi $\Img(w_i)$ è chiuso in $H/N$.
Non resta che mostrare che i $w_i$ sono isomorfismi.

Dato che $\Img(v_i)$ e $\Img(v_j)$ sono sottogruppi distinti, chiusi e connessi di dimensione $1$, in quanto isomorfi a $\Gb_a$,
la loro intersezione è un sottogruppo di dimensione minore di $1$, quindi è finito.
Ma l'unico sottogruppo finito di $\Gb_a$ è $\{e\}$, quindi l'intersezione di $\Img(v_i)$ e $\Img(v_j)$ è banale.
Da ciò segue che $w_i$ è una bigezione su $\Img(w_i)$, quindi per~\ref{IsoiffBigG} $w_i$ è un isomorfismo.

Possiamo ora applicare l'ipotesi induttiva e assumere il lemma per $H/N$.

Sapendo che la mappa $\widetilde{\psi}(x_1, \dots, x_{j-1}, x_{j+i}, \dots x_n) = w_1(x_1) \dots w_n(x_n)$ è una bigezione
$\Gb_a^{n-1} \to (H/N)_u \simeq H_u/N$ e dalla centralità di $N$ in $H_u$, si evince che
$\psi(x_1, \dots, x_n) = v(x_1) \dots v(x_n) = (v(x_1) \dots v(x_{j-1}) v(x_{j+1}) v(x_n)) v(x_j)$
è una bigezione $\Gb_a^n \to H_u$.

$\Gb_a^n$ è irriducibile in quanto prodotto di irriducibili mentre $H_u$ è connesso per definizione di radicale unipotente,
quindi irriducibile per~\ref{CmpConnIrr} (ii).
Inoltre $H_u$ è una varietà normale per~\ref{GSpOmoNorm}, quindi è sufficiente applicare~\ref{IsoiffBigNorm}
per concludere che $\psi$ è un isomorfismo.

\end{proof}

\medskip
Sia $B$ un sottogruppo di Borel di $G$ contenente il toro massimale $T$.
Indichiamo con $U = B_u$ il radicale unipotente di $B$ e con $R^+$ il sistema di radici positive definito da $B$.

% 8.2.1
\begin{corollary}\label{BuGenUa}
Siano $\{\alpha_1, \alpha_2, \dots, \alpha_m\}$ le radici di $R^+$.
Il morfismo $\phi : G_a^m \to U$ dato da $\phi(x_1, \dots, x_m) = u_{\alpha_1}(x_1) \dots u_{\alpha_m}(x_m)$ è un isomorfismo di varietà.
In particolare, $U$ è generato dai gruppi $U_{\alpha}$ con $\alpha \in R^+$.
\end{corollary}
\begin{proof}
Verifichiamo le ipotesi del precedente lemma per $B \supset T$:
la proposizione~\ref{uaUaTgenG} mostra l'esistenza degli isomorfismi e dei caratteri e~\ref{Rrid} la loro indipendenza a due a due,
mentre~\ref{gaonedim} ci garantisce la monodimensionalità degli spazi-peso e~\ref{UaTBperGSS1} (ii) che il loro span sia $L(U)$.
\end{proof}

\medskip
Consideriamo un insieme di rappresentanti $(\dot{w})_{w \in W}$ degli elementi di $W$ in $N_G(T)$.

% 8.2.4 (i)
\begin{proposition}\label{TUaGenB}
Sia $R'^+$ un sistema di radici positive in $R$, allora $T$ e gli $U_{\alpha}$ con $\alpha \in R'^+$ generano un sottogruppo di Borel $B'$ di $G$.
Il prodotto $T \times \prod_{\alpha \in R'^+} U_{\alpha} \to B'$ è un isomorfismo di varietà.
\end{proposition}
\begin{proof}
Per~\ref{WTransGenS} (i) esiste $w \in W$ con $R'^+ = w.R^+$.
Per~\ref{BuGenUa} e~\ref{TGuisoG} sappiamo che $T$ e $U_{\alpha}$ per $\alpha \in R^+$ generano $B$
e che il prodotto $T \times \prod_{\alpha \in R^+} U_{\alpha} \to B$ è un isomorfismo di varietà,
quindi $\dot{w} T \dot{w}^{-1}$ e $\dot{w} U_{\alpha} \dot{w}^{-1}$ per $\alpha \in R^+$
generano $\dot{w} B \dot{w}^{-1}$, a sua volta un sottogruppo di Borel, e il loro prodotto è un isomorfismo di varietà.
Ma $\dot{w} T \dot{w}^{-1} = T$ e $\dot{w} U_{\alpha} \dot{w}^{-1} = U_{w.\alpha}$ per~\ref{Wactua}, segue la tesi.
\end{proof}

\medskip
Il risultato che segue mette in relazione i vari $U_{\alpha}$ con l'intero gruppo $G$.

% 8.2.10
\begin{proposition}\label{TUapmGenG}
$G$ è generato da $T$ e dai gruppi $U_{\pm \alpha}$ con $\alpha \in D$, l'insieme delle radici semplici.
\end{proposition}
\begin{proof}
Sia $G_1$ il sottogruppo di $G$ generato da $T$ e $U_{\pm \alpha}$ ($\alpha \in D$).
Per~\ref{GroupProd} $G_1$  è chiuso e connesso, inoltre per~\ref{UaTBperGSS1} (iii) contiene tutti i sottogruppi $G_{\alpha}$ con $\alpha \in D$.
Di conseguenza il gruppo di Weyl $W_1$ di $(G_1,T)$, sottogruppo di $W$ in quanto dato da
\begin{align*}
	W_1
	&= N_{G_1}(T)/Z_{G_1}(T)\\
	&= (N_{G}(T) \cap G_1)/(Z_{G}(T) \cap G_1)\\
	&= (N_{G}(T) \cap G_1)/((N_{G}(T) \cap G_1) \cap Z_{G}(T))\\
	&= ((N_{G}(T) \cap G_1) Z_{G}(T))/Z_{G}(T)
\end{align*}
contiene le riflessioni semplici.
Ma per~\ref{WTransGenS} (iii) queste ultime generano $W$, quindi $W_1 = W$.
Da~\ref{WTransGenS} (ii) e~\ref{Wactua} vediamo che $G_1$ contiene tutti gli $U_{\alpha}$ ($\alpha \in R$), quindi $G_1 = G$ per~\ref{uaUaTgenG} (ii).
\end{proof}

\medskip
La prossima proposizione ci dà informazioni sul prodotto fra elementi di diversi $U_{\alpha}$ al variare di $\alpha$.
In particolare, ci consentirà in futuro di definire una struttura di gruppo sul prodotto di vari $U_{\alpha}$ con
$\alpha$ appartenente a determinati sottoinsiemi di $R$.

\medskip
Poniamo $(g,h) = ghg^{-1}h^{-1}$ ($g,h \in G$).

% 8.2.3
\begin{proposition}\label{CommUaUb}
Siano $\alpha, \beta \in R$, $\alpha \neq \pm \beta$.
Fissato un ordinamento totale su $R$, esistono costanti $c_{\alpha, \beta; i, j} \in k$ tali che
\[
	(u_{\alpha}(x), u_{\beta}(y)) = \prod_{i \alpha + j \beta \in R; i,j>0} u_{i \alpha + j \beta}(c_{\alpha, \beta; i, j} x^i y^j) \qquad (x,y \in k)
\]
dove i fattori nel membro di destra sono ordinati come stabilito.
\end{proposition}
\begin{proof}

Usando~\ref{wawbpos} e~\ref{Wactua} possiamo ricondurci al caso $\alpha, \beta \in R^+$.
Allora $U_{\alpha}, U_{\beta} \in U$ e, per~\ref{BuGenUa}
\[ (u_{\alpha}(x), u_{\beta}(y)) = \prod_{\gamma \in R^+} u_{\gamma}(P_{\gamma}(x,y)) \]
con $P_{\gamma} \in k[X,Y]$ e i fattori ordinati come precedentemente stabilito.
Coniugando per $t \in T$ otteniamo
\begin{align*}
	& \prod_{\gamma \in R^+} u_{\gamma}(\gamma(t)P_{\gamma}(x,y))\\
	&= \prod_{\gamma \in R^+} t u_{\gamma}(P_{\gamma}(x,y)) t^{-1}
	= t \left( \prod_{\gamma \in R^+} u_{\gamma}(P_{\gamma}(x,y)) \right) t^{-1}\\
	&= t (u_{\alpha}(x), u_{\beta}(y)) t^{-1}
	= (t u_{\alpha}(x) t^{-1}, t u_{\beta}(y) t^{-1})
	= (u_{\alpha}(\alpha(t)x), u_{\beta}(\beta(t)y))\\
	&= \prod_{\gamma \in R^+} u_{\gamma}(P_{\gamma}(\alpha(t)x,\beta(t)y))
\end{align*}
da cui, sempre per~\ref{BuGenUa},
\[ \gamma(t)P_{\gamma}(x,y) = P_{\gamma}(\alpha(t)x,\beta(t)y) \]
Dato che l'uguaglianza deve essere vera ``termine a termine'', se $P_{\gamma} \neq 0$ si deve avere
$\gamma(t) = \alpha(t)^i \beta(t)^j$, cioè $\gamma = i\alpha + j\beta$, con $i,j \ge 0$.
Dato che per~\ref{Rrid} $\alpha$ e $\beta$ sono linearmente indipendenti, tali $i$ e $j$ sono unici,
da cui $P_{\gamma}(x,y) = c_{\alpha,\beta;i,j} x^iy^j$.

Resta da mostrare che $i,j > 0$; supponiamo per assurdo $j=0$.
Allora, sempre per~\ref{Rrid} si deve avere $i=1$, da cui
\[
	(u_{\alpha}(x), u_{\beta}(y)) =
	\left( \prod_{\gamma \in R^+; \gamma \prec \alpha} u_{\gamma}(P_{\gamma}(x,y)) \right)
	u_{\alpha}(c_{\alpha,\beta;1,0} x)
	\left( \prod_{\gamma \in R^+; \gamma \succ \alpha} u_{\gamma}(P_{\gamma}(x,y)) \right)
\]
Ponendo $y=0$ otteniamo
\[
	e = \left( \prod_{\gamma \in R^+; \gamma < \alpha} u_{\gamma}(P_{\gamma}(x,0)) \right)
	u_{\alpha}(c_{\alpha,\beta;i,j} x)
	\left( \prod_{\gamma \in R^+; \gamma > \alpha} u_{\gamma}(P_{\gamma}(x,0)) \right)
\]
assurdo per~\ref{BuGenUa}.
\end{proof}
