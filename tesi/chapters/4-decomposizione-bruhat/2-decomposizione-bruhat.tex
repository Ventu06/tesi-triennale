Questa è la sezione conclusiva della prima parte della tesi.
Qui andremo a dimostrare il lemma di Bruhat, il quale ci permette di partizionare $G$ in
classi laterali doppie $C(w) = B \dot{w} B$ al variare di $w \in W$, cioè secondo la cosiddetta
decomposizione di Bruhat.
Per far ciò introdurremo dei sottogruppi $U_w$ di $B$, uno per ogni elemento di $w \in W$,
che ci aiuteranno a caratterizzare le classi $C(w)$, il tutto utilizzando ampiamente la teoria
sulla decomposizione ridotta sviluppata alla fine del secondo capitolo.

\subsection
Sapendo che $R^+$ è un sistema di radici positive, dalla definizione si evince che anche
$-R^+$ è un sistema di radici positive, quindi per~\ref{WTransGenS} (i) esiste $w_0 \in W$
con $w_0.R^+ = -R^+$, unico per~\ref{wUniq}.
$w_0$ è anche l'unico elemento di $W$ per cui $l(w) = \#|R^+|$.
Notiamo che $w_0^2.R^+ = R^+$, quindi sempre per~\ref{wUniq} $w_0^2 = e$.

\subsection\label{w0ord}
Ricordando l'ordinamento definito in~\ref{ordpos}, se $x,y \in X(T)$ e $x \ge y$, allora $w_0.x \le w_0.y$:
infatti, se $x = y + \sum_{\alpha \in R^+} n_{\alpha} \alpha$,
allora $w_0.y = w_0.x + \sum_{\alpha \in R^+} n_{\alpha} (-w_0.\alpha) = w_0.x + \sum_{\alpha \in R^+} n_{-w_0.\alpha} \alpha$.

\medskip
Ricordando la definizione di $R(w)$ per $w \in W$ data in~\ref{Rw}, introduciamo subito i sottogruppi $U_w$.

% 8.3.5
\begin{lemma}\label{UwGroup}
Sia $w \in W$.
\begin{enumerate}[(i)]
	\item I gruppi $U_{\alpha}$ con $\alpha \in R(w)$ generano un sottogruppo $U_w$ di $U$ chiuso, connesso e normalizzato da $T$.
		Infatti si ha $U_w = \prod_{\alpha \in R(w)} U_{\alpha}$, dove il prodotto può essere considerato in qualsiasi ordine;
	\item il morfismo $U_w \times U_{w_0 w} \to U$ indotto dal prodotto è un isomorfismo di varietà.
\end{enumerate}
\end{lemma}
\begin{proof}{\ }
\begin{enumerate}[(i)]
	\item Dato che i sottogruppi $U_{\alpha}$ ($\alpha \in R(w)$) sono chiusi, connessi e normalizzati da $T$, anche $U_w$ è chiuso e connesso per~\ref{GroupProd} e normalizzato da $T$.

		Affinché $\widehat{U}_w = \prod_{\alpha \in R(w)} U_{\alpha}$ coincida con $U_w$ ci basta mostrare che tale prodotto è effettivamente un gruppo.

		Per ogni $\alpha, \beta \in R(w)$ con $\alpha \neq \beta$, diciamo che $\alpha \prec \beta$ se $U_{\alpha}$ precede (strettamente) $U_{\beta}$ nell'ordine in cui si svolge il prodotto di $\widehat{U}_w$.
		Estendiamo inoltre, solo per questa dimostrazione, l'ordinamento~\ref{ordpos} a un ordinamento totale su $R$.

		Data una stringa $u = u_{\alpha_{j_1}} u_{\alpha_{j_2}} \dots u_{\alpha_{j_n}}$ con $\alpha_{j_i} \in R(w)$ e $u_{\alpha_{j_i}} \in U_{\alpha_{j_i}}$, sia
		\[
			f(u) = \min \{ \infty \} \cup \{ \alpha \in R(w) \mid \exists \ i_1, i_2 : \alpha_{j_{i_1}}, \alpha_{j_{i_2}} \le \alpha \ \wedge \ i_1 < i_2 \ \wedge \ \alpha_{j_{i_1}} \succeq \alpha_{j_{i_2}} \}
		\]
		dove si intende $\infty > \alpha$ per ogni $\alpha \in R$.
		Notiamo che, se $f(u) = \infty$, allora l'elemento che $u$ rappresenta appartiene a $\widehat{U}_w$.

		Mostriamo per induzione decrescente sul valore di $f$ che ogni stringa $u$ del tipo descritto in precedenza è equivalente (come elemento) a una stringa $u'$ con $f(u') = \infty$.
		Il caso base $f(u) = \infty$ è valido per ipotesi, supponiamo $f(u) = \alpha$ e che la tesi sia vera per ogni radice più grande di $\alpha$.

		Preso $i$ con $\alpha_{j_i} = \alpha$, abbiamo
		\begin{align*}
			u_{\alpha_{j_i}} u_{\alpha_{j_{i+1}}}
			&= u_{\alpha_{j_i}} u_{\alpha_{j_{i+1}}} u_{\alpha_{j_i}}^{-1} u_{\alpha_{j_{i+1}}}^{-1} u_{\alpha_{j_{i+1}}} u_{\alpha_{j_i}}\\
			&= (u_{\alpha_{j_i}}, u_{\alpha_{j_{i+1}}}) u_{\alpha_{j_{i+1}}} u_{\alpha_{j_i}}\\
			&= \left( \prod_{h \alpha_{j_i} + k \alpha_{j_{i+1}} \in R; h,k>0} u_{h \alpha_{j_i} + k \alpha_{j_{i+1}}} \right) u_{\alpha_{j_{i+1}}} u_{\alpha_{j_i}}
		\end{align*}
		per alcuni elementi $u_{h \alpha_{j_i} + k \alpha_{j_{i+1}}}$, dove l'ultimo passaggio segue da~\ref{CommUaUb}.

		Un risultato simile si ottiene con $u_{\alpha_{j_{i-1}}} u_{\alpha_{j_i}}$ al posto di $u_{\alpha_{j_i}} u_{\alpha_{j_{i+1}}}$.
		Detta $u'$ la stringa ottenuta con una delle due precedenti sostituzioni, si verifica facilmente che $f(u') \ge f(u)$ e il numero di indici $i$ tali che $\alpha_{j_i} = \alpha$
		non è aumentato. Effettuando una successione di sostituzioni, possiamo accorpare i vari $u_{\alpha_{j_i}}$ con $\alpha_{j_i} = \alpha$ fin quando ne rimane al più uno
		e poi eventualmente spostare quest'ultimo nella stringa in modo che alla fine, per ogni $i_1,i_2 \le \alpha$ si abbia $i_1 < i_2$ se e solo se $\alpha_{j_{i_1}} \prec \alpha_{j_{i_2}}$.
		In questo maniera troviamo una stringa $u'$ con $f(u') > f(u)$, concludendo per ipotesi induttiva.

		Arrivati a questo punto, ci basta notare che il prodotto di due elementi $u_1 \dots u_n$ e $v_1 \dots v_n$ di $\widehat{U}_w$
		è la stringa $u_1 \dots u_n v_1 \dots v_n$ e l'inverso del primo elemento è la stringa $u_n^{-1} \dots u_1^{-1}$, entrambe
		stringhe del tipo considerato, quindi elementi di $\widehat{U}_w$.
		In questo modo concludiamo che $\widehat{U}_w$ è chiuso per moltiplicazione e inverso, quindi è un sottogruppo;

	\item notiamo che $R^+ = R(w) \sqcup R(w_0 w)$, quindi ricordando~\ref{BuGenUa} abbiamo un isomorfismo di varietà
		\[
			U_w \times U_{w_0 w} = \prod_{\alpha \in R(w)} U_{\alpha} \times \prod_{\alpha \in R(w_0 w)} U_{\alpha} \simeq \prod_{\alpha \in R+} U_{\alpha} \simeq U
		\]
\end{enumerate}
\end{proof}

\medskip
Il seguente lemma sarà utile per dimostrare il risultato successivo.

\begin{lemma}\label{UasubH}
Sia $H$ un sottogruppo chiuso non nullo di $B_u$ normalizzato da $T$, allora esiste $\alpha \in R^+$ tale che $U_{\alpha} \subset H$.
\end{lemma}
\begin{proof}

Iniziamo notando che esiste $t \in T$ tale che $\alpha(t) \neq \beta(t)$ per ogni $\alpha,\beta \in R^+$ con $\alpha \neq \beta$.
Infatti sia $f : T \to k$ la funzione regolare data da
\[
	f(t) = \prod_{\alpha, \beta \in R, \ \alpha \neq \beta} (\alpha(t) - \beta(t))
\]
Se per assurdo non esistesse un tale $t$, si avrebbe $f(t)=0$ per ogni $t$.
Detti $T_{\alpha-\beta}^0 = \{ t \in T \mid \alpha(t) = \beta(t) \}$, che notiamo essere chiusi, si ha
$T = \bigcup_{\alpha, \beta \in R, \ \alpha \neq \beta} T_{\alpha-\beta}^0$,
quindi esistono $\alpha, \beta \in R$, $\alpha \neq \beta$ tali che $T_{\alpha-\beta}^0 = T$
per irriducibilità di $T$, che ricordiamo essere prodotto di copie dell'irriducibile $k^*$.
Ma allora $\alpha = \beta$, assurdo, sia quindi $t_0$ un elemento di $T$ che soddisfa la proprietà enunciata.

Ricordiamo che l'algebra di Lie di $B_u$ è $\bigoplus_{\alpha \in R^+} \gf_{\alpha}$,
quindi, preso un elemento $X$ dell'algebra di Lie $\hf$ di $H$, possiamo scrivere
$X = \sum_{\alpha \in R^+} X_{\alpha}$ con $X_{\alpha} \in \gf_{\alpha}$.
L'azione di $T$ per coniugio su $H$ induce un'azione su $\hf$, esplicitamente data da
\[
	t.X
	= t.\sum_{\alpha \in R^+} X_{\alpha}
	= \sum_{\alpha \in R^+} t.X_{\alpha}
	= \sum_{\alpha \in R^+} \alpha(t) X_{\alpha}
\]
Numerando le radici da $1$ a $r = \#|R^+|$ e facendo agire $e, t_0, t_0^2, \dots, t_0^{r-1}$ su $X$
otteniamo il sistema lineare $(X_{\alpha_1}, \dots, X_{\alpha_r}) M = (X, t_0.X, t_0^2.X, \dots, t_0^{r-1}.X)$,
dove $M$ è la matrice $r \times r$ di Vandermonde nei valori $(\alpha_1(t_0), \dots, \alpha_r(t_0))$.
Essendo gli $\alpha_j(t_0)$ tutti distinti, $M$ è invertibile, dunque possiamo ottenere i vari $X_{\alpha_j}$
come combinazione lineare dei $t^j.X$, in particolare, $X_{\alpha_j} \in \hf$ per ogni $j$.

Prendiamo ora $X \in \hf$ con $X \neq 0$ e sia $\alpha \in R^+$ tale che $X_{\alpha} \neq 0$.
Per quanto appena detto $X_{\alpha} \in \hf$, quindi $\gf_{\alpha} \subset \hf$ e per~\ref{SubalgThenSubgrp}
questo equivale a $U_{\alpha} \subset H$.

\end{proof}

\medskip
Indichiamo con $C(w)$ la classe laterale doppia $B \dot{w} B$.

Il lemma che segue fornisce una descrizione della struttura dei $C(w)$ attraverso degli isomorfismi di varietà con alcuni sottogruppi noti.

% 8.3.6
\begin{lemma}\label{AhxBisoCw}
Sia $\textbf{s} = (s_1, \dots, s_h)$ una decomposizione ridotta di $w \in W$, con $s_i = s_{\alpha_i}$ ($\alpha_i \in D$).
\begin{enumerate}[(i)]
	\item Il morfismo $\phi : \Gb_a^h \times B \to G$ dato da
		\[
			\phi(x_1, \dots, x_h; b) = u_{\alpha_1}(x_1) \dot{s}_1 u_{\alpha_2}(x_2) \dot{s}_2 \dots u_{\alpha_h}(x_h) \dot{s}_h b
		\]
		induce un isomorfismo $\Gb_a^h \times B \simeq C(w)$;
	\item La mappa $(u,b) \mapsto u \dot{w} b$ è un isomorfismo di varietà $U_{w^{-1}} \times B \simeq C(w)$.
\end{enumerate}
\end{lemma}
\begin{proof}{\ }
\begin{enumerate}[(i)]
	\item Notiamo che $C(w) = B \dot{w} B = UT \dot{w} B = U \dot{w} TB = U_{w^{-1}}U_{w_0 w^{-1}} \dot{w} B$ per~\ref{UwGroup} (ii).
		Da~\ref{Wactua} si ha
		\[
			\dot{w}^{-1}U_{w_0 w^{-1}}\dot{w} =
			\prod_{\alpha \in R(w_0 w^{-1})} \dot{w}^{-1} U_{\alpha} \dot{w} =
			\prod_{\alpha \in R(w_0 w^{-1})} U_{w^{-1}.\alpha} =
			\prod_{\alpha \in w^{-1}.R(w_0 w^{-1})} U_{\alpha}
		\]
		Se $\alpha \in w^{-1}.R(w_0 w^{-1})$, allora $w_0 w^{-1}.w.\alpha \in -R^+$, cioè $\alpha \in R^+$ e quindi $U_{\alpha} \subset B$.
		Segue che $\dot{w}^{-1}U_{w_0 w^{-1}}\dot{w} \subset B$ da cui $C(w) = U_{w^{-1}} \dot{w} (\dot{w}^{-1} U_{w_0 w^{-1}} \dot{w}) B = U_{w^{-1}} \dot{w} B$.
		Visto che $\textbf{s}^{-1} = (s_h^{-1}, \dots, s_1^{-1}) = (s_h, \dots, s_1)$ è una decomposizione ridotta di $w^{-1}$,	da~\ref{PropDec} (i),
		$R(w^{-1}) = \{\alpha_1, s_1.\alpha_2, \dots, s_1 \dots s_{h-1}.\alpha_h\}$.
		Usando~\ref{Wactua} otteniamo
		\begin{align*}
			U_{w^{-1}}
				&= U_{\alpha_1} U_{s_1.\alpha_2} \dots U_{s_1 \dots s_{h-1}.\alpha_h}\\
				&= U_{\alpha_1} \dot{s_1} U_{\alpha_2} \dot{s_1}^{-1} \dots \dot{s_1} U_{s_2 \dots s_{h-1}.\alpha_h} \dot{s_1}^{-1}\\
				&= U_{\alpha_1} \dot{s_1} (U_{\alpha_2}  \dots U_{s_2 \dots s_{h-1}.\alpha_h}) \dot{s_1}^{-1}\\
				&= U_{\alpha_1} (\dot{s_1} U_{w^{-1} s_1} \dot{s_1}^{-1})
		\end{align*}
		da cui
		\begin{align*}
			C(w)
				&= U_{w^{-1}} \dot{w} B\\
				&= U_{\alpha_1} (\dot{s_1} U_{w^{-1} s_1} \dot{s_1}^{-1}) \dot{w} B\\
				&= U_{\alpha_1} \dot{s_1} U_{(s_1^{-1} w)^{-1}} (\dot{s_1}^{-1} \dot{w}) B\\
				&= U_{\alpha_1} \dot{s_1} U_{(s_1 w)^{-1}} \dot{(s_1 w)} B\\
				&= U_{\alpha_1} \dot{s_1} C(s_1 w)
		\end{align*}
		Per induzione su $h = l(w)$ possiamo assumere la tesi vera per $s_1 w = s_2 \dots s_h$.
		Questo ci garantisce la suriettività di $\phi$ su $C(w)$.
		Per~\ref{UwGroup} (i) e~\ref{uaUaTgenG} (i) sappiamo che la mappa $\Gb_a^h \times B \to U_{w^{-1}} \times B$ data da
		$(x_1, \dots, x_h; b) \mapsto (u_{\alpha_1}(x_1) \dots u_{s_1 \dots s_{h-1}.\alpha_h}(x_h), b)$
		è un isomorfismo.
		Dato che $w u_{\alpha}(x) w^{-1} = u_{w.\alpha}(c_{w, \alpha} x)$ per~\ref{Wactua}, come si è mostrato nei precedenti passaggi
		\[
			\phi : \Gb_a^h \times B
			\xrightarrow{(\cdot d_1, \dots, \cdot d_h; \Id)} \Gb_a^h \times B
			\xrightarrow{(u_{\alpha_1}(\cdot) \dots u_{s_1 \dots s_{h-1}.\alpha_h}(\cdot), \Id)} U_{w^{-1}} \times B
			\xrightarrow{\text{morfismo di (ii)}} C(w)
		\]
		cioè $\phi$ è composizione dell'isomorfismo $\Gb_a^h \times B \to U_{w^{-1}} \times B$ appena costruito e del morfismo del punto (ii),
		a meno di riscalare i singoli termini di $\Gb_a^h$, a sua volta un isomorfismo.
		Ci siamo perciò ricondotti a dimostrare il punto (ii);

	\item la tesi è equivalente a mostrare che $\psi : U_{w^{-1}} \times B \to \dot{w}^{-1}.C(w)$ dato da $\psi(u,b) = \dot{w}^{-1} u \dot{w} b^{-1}$ è un isomorfismo.

		Facciamo agire $U_{w^{-1}} \times B$ su se stesso come traslazione sinistra e su $\dot{w}^{-1}.C(w)$ mediante
		l'azione data da $(u,b).c = \dot{w}^{-1} u \dot{w} c b^{-1}$.
		Notiamo che entrambi gli spazi sono omogenei rispetto quest'azione, inoltre
		\begin{align*}
			\psi((u,b).(u',b'))
			&= \psi(u u',b b')\\
			&= \dot{w}^{-1} u u' \dot{w} (b b')^{-1}\\
			&= \dot{w}^{-1} u \dot{w} (\dot{w}^{-1} u' \dot{w} b'^{-1}) b\\
			&= (u,b).(\dot{w}^{-1} u' \dot{w} b'^{-1})\\
			&= (u,b).\psi(u',b')
		\end{align*}
		cioè $\psi$ è un morfismo equivariante.
		Più in generale, $\psi(u,b) = (u,b).\psi(e,e) = (u,b).e$, cioè $\psi$ corrisponde
		all'azione di $U_{w^{-1}} \times B$ sull'identità.
		Per~\ref{IsoiffBigG} è sufficiente mostrare che $\psi$ è bigettiva affinché sia un isomorfismo.

		La suriettività è immediata da verificare, mentre per l'iniettività basta mostrare che
		$H = \Stab_{U_{w^{-1}} \times B}(e) = (e,e)$.
		$H$ è un sottogruppo chiuso di $U_{w^{-1}} \times B$, in quanto controimmagine del chiuso $\{e\}$ rispetto l'azione di $U_{w^{-1}} \times B$ su $e$.

		Preso $(u,b) \in H$ e $t \in T$ si ha
		\[
			e
			= t e t^{-1}
			= t (\dot{w}^{-1} u \dot{w} b^{-1}) t^{-1}
			= \dot{w}^{-1} ((\dot{w} t \dot{w}^{-1}) u (\dot{w} t \dot{w}^{-1})^{-1}) \dot{w} (t b t^{-1})^{-1}
		\]
		Dato che $\dot{w}$ normalizza $T$ e $T$ normalizza sia $U_{w^{-1}}$ che $B$ si ha
		$((\dot{w} t \dot{w}^{-1}) u (\dot{w} t \dot{w}^{-1})^{-1}, t b t^{-1}) \in H$.
		Sia $H_1$ il gruppo ottenuto mediante la proiezione sulla prima componente di $H$,
		chiuso per~\ref{ImgSubGrpCls} dato che $H$, essendo un sottogruppo chiuso di un gruppo algebrico, è un gruppo algebrico.
		Per quanto appena visto $H_1$ è un sottogruppo di $U_{w^{-1}}$ normalizzato da $\dot{w} T \dot{w}^{-1} = T$.
		Se $H_1 = \{e\}$ è immediato vedere che $H = (e,e)$, altrimenti per~\ref{UasubH} esiste
		$\alpha \in R^+$ con $U_{\alpha} \subset H_1$.
		Ricordando la definizione di $U_{w^{-1}}$ e notando che $U_{\alpha} \subset U_{w^{-1}}$
		si deve avere $\alpha \in R(w^{-1})$, cioè $w^{-1}.\alpha \in -R^+$,
		altrimenti quello di~\ref{UwGroup} (ii) non sarebbe un isomorfismo.
		Per ogni $u \in U_{\alpha}$ esiste $b \in B$ con $(u,b) \in H$, cioè con
		$\dot{w}^{-1} u \dot{w} = b$, ma allora $U_{w^{-1}.\alpha} = \dot{w}^{-1} U_{\alpha} \dot{w} \subset B$,
		assurdo per~\ref{aRUaBgabdim} (i).
\end{enumerate}
\end{proof}

\medskip
Notiamo come la dimensione delle classi laterali doppie sia strettamente legata alla lunghezza
degli elementi di $W$:

\begin{corollary}\label{dimCw}
Dato $w \in W$ si ha
\[
	\dim(C(w)) = l(w) + \dim(B) = \dim(G) - (\#|R^+| - l(w))
\]
In particolare $\dim(C(w_0)) = \dim(G)$, $\dim(C(s_{\alpha}.w_0)) = \dim(G)-1$ per ogni $\alpha \in D$,
mentre $\dim(C(w)) \le \dim(G)-2$ per ogni altro $w \in W$.
\end{corollary}
\begin{proof}
La tesi discende direttamente dal precedente teorema.
\end{proof}

\medskip
Presentiamo un risultato che sarà fondamentale nella dimostrazione del teorema di Borel-Weil.

% 8.3.11
\begin{corollary}\label{Cw0OpDen}
$C(w_0)$ è un aperto denso di $G$
\end{corollary}
\begin{proof}
Per~\ref{CmpConnIrr} (ii) le nozioni di componente connessa e irriducibile coincidono,
quindi $G$ è irriducibile, e dato che $\dim(C(w_0)) = \dim(G)$ per~\ref{dimCw} si ha
$\overline{C(w_0)} = G$, cioè $C(w_0)$ è denso in $G$.
Considerando $C(w_0) = B \dot{w_0} B$ come un $B \times B$ spazio tramite le traslazioni
sinistra e destra, per~\ref{OrbOpen} $C(w_0)$ è aperto nella suo chiusura, quindi è un aperto di $G$.
\end{proof}

\medskip
Prima di dimostrare il lemma di Bruhat è necessario un ultimo risultato sul prodotto fra alcune classi laterali doppie.

% 8.3.7
\begin{lemma}\label{CsCw}
Siano $w \in W$, $s \in S$, allora
\begin{align*}
	C(s).C(w) &= C(sw) & &\text{se} \quad l(sw) = l(w)+1\\
	C(s).C(w) &= C(w) \cup C(sw) & &\text{se} \quad l(sw) = l(w)-1
\end{align*}
\end{lemma}
\begin{proof}

Sia $s = s_{\alpha} = s_{-\alpha}$ ($\alpha \in D$).
Per~\ref{AhxBisoCw} (ii), $C(s) = U_{\alpha} \dot{s} B$, quindi
$C(s).C(w) = (U_{\alpha} \dot{s} B)(B \dot{w} B) = U_{\alpha} \dot{s} C(w)$.

Se $l(sw) = l(w)+1$, la tesi segue ripercorrendo parte della dimostrazione di~\ref{AhxBisoCw} (i):
presa una decomposizione ridotta di $w$, si può ottenere una decomposizione ridotta di $sw$ aggiungendo $s$ in testa alla precedente,
da cui $C(sw) = U_{\alpha} \dot{s} C(s(sw)) = U_{\alpha} \dot{s} C(w) = C(s).C(w)$.

Se invece $l(sw) = l(w)-1$, in modo simile al caso precedente si trova
\[ C(s).C(w) = C(s).C(s(sw)) = C(s).C(s).C(sw) \]
Supponiamo che $C(s).C(s) = C(e) \cup C(s)$; allora
\begin{align*}
	C(s).C(w)
		&= C(s).C(s).C(sw)
		= (C(e) \cup C(s)).C(sw)\\
		&= B (B \dot{(sw)} B) \cup C(s).C(sw)
		= C(sw) \cup C(w)
\end{align*}

Non ci resta che mostrare quest'ultima assunzione.
Sia $G_{\alpha}$ il gruppo definito in~\ref{Galpha}, allora $U_{\alpha} \subset G_{\alpha}$ e $B_{\alpha} = B \cap G_{\alpha}$ è un sottogruppo di Borel di $G_{\alpha}$ per~\ref{ZGScapB}.
Dato che $C(s).C(s) = U_{\alpha} \dot{s} U_{\alpha} \dot{s} B = U_{\alpha} \dot{s} U_{\alpha} \dot{s} B_{\alpha} B$ e che
$C(e) \cup C(s) = B \cup U_{\alpha} \dot{s} B = (B_{\alpha} \cup U_{\alpha} \dot{s} B_{\alpha}) B$,
ci basta mostrare che $U_{\alpha} \dot{s} U_{\alpha} \dot{s} B_{\alpha} = B_{\alpha} \cup U_{\alpha} \dot{s} B_{\alpha}$,
cioè è sufficiente restringersi a gruppi di rango semi-semplice 1.

Si assuma $G = G_{\alpha}$, e ricordando che il radicale $R(G)$ di $G$ è un toro centrale normale, abbiamo $B = UT = U(T/R(G))R(G)$ e $B/R(G) = U(T/R(G))$,
quindi in maniera simile alla precedente possiamo ulteriormente restringerci a mostrare che
$C(s)/R(G).C(s)/R(G) = U \dot{s} U \dot{s} U (T/R(G)) = U(T/R(G)) \cup U \dot{s} U(T/R(G)) = C(e)/R(G) \cup C(s)/R(G)$,
cioè possiamo assumere $G$ semi-semplice.

Per~\ref{GSL2PSL2} $G$ è isomorfo a $\SLb_2$ o $\PSLb_2$.
Se $G \simeq \PSLb_2$, possiamo considerare l'omomorfismo di gruppi algebrici $\pi : \SLb_2 \to \PSLb_2$ definito in~\ref{SL2toPSL2}
e scegliere un sottogruppo di Borel e un rappresentante del gruppo di Weyl in $\SLb_2$ che vengano mappati da $\pi$
nei corrispettivi di $\PSLb_2$.

Rimane dunque come unico caso da trattare $G=\SLb_2$ e a meno di coniugio $B$, $U$ e $\dot{s}$ corrispondono alle matrici del tipo
\begin{align*}
	B = \left\{
		\begin{pmatrix}
			\lambda & a\\
			0 & \lambda^{-1}\\
		\end{pmatrix}
	\right\}
	\qquad
	U = \left\{
		\begin{pmatrix}
			1 & a\\
			0 & 1\\
		\end{pmatrix}
	\right\}
	\qquad
	\dot{s} =
	\begin{pmatrix}
		0 & 1\\
		-1 & 0\\
	\end{pmatrix}
	\qquad
	(a \in k, \ \lambda \in k^*)
\end{align*}

Siano
\begin{align*}
	p(a,\lambda) &=
	\begin{pmatrix}
		\lambda & a\\
		0 & \lambda^{-1}\\
	\end{pmatrix}\\[10pt]
	q(a,b,\lambda) &=
	\begin{pmatrix}
		1 & a\\
		0 & 1\\
	\end{pmatrix}
	\cdot
	\begin{pmatrix}
		0 & 1\\
		-1 & 0\\
	\end{pmatrix}
	\cdot
	\begin{pmatrix}
		\lambda & b\\
		0 & \lambda^{-1}\\
	\end{pmatrix}\\
	&=
	\begin{pmatrix}
		-a\lambda & -ab+\lambda^{-1}\\
		-\lambda & -b\\
	\end{pmatrix}\\[10pt]
	r(a,b,c,\lambda) &=
	\begin{pmatrix}
		1 & a\\
		0 & 1\\
	\end{pmatrix}
	\cdot
	\begin{pmatrix}
		0 & 1\\
		-1 & 0\\
	\end{pmatrix}
	\cdot
	\begin{pmatrix}
		1 & b\\
		0 & 1\\
	\end{pmatrix}
	\cdot
	\begin{pmatrix}
		0 & 1\\
		-1 & 0\\
	\end{pmatrix}
	\cdot
	\begin{pmatrix}
		\lambda & c\\
		0 & \lambda^{-1}\\
	\end{pmatrix}\\
	&=
	\begin{pmatrix}
		\lambda(ab-1) & c(ab-1)-a\lambda^{-1}\\
		b\lambda & bc-\lambda^{-1}\\
	\end{pmatrix}\\
\end{align*}
generici elementi di rispettivamente $C(e)$, $C(s)$, $C(s).C(s)$, con $a,b,c \in k$, $\lambda \in k^*$.

Consideriamo un elemento $r(a,b,c,\lambda)$ di $C(s).C(s)$.
Se $b = 0$ abbiamo $p(-c-a\lambda^{-1}, -\lambda) = r(a,0,c,\lambda)$;
se invece $b \neq 0$ abbiamo $q(a-b^{-1}, \lambda^{-1}-bc, -b\lambda) = r(a,b,c,\lambda)$.
Questo mostra che $C(s).C(s) \subset C(e) \cup C(s)$.

Consideriamo un elemento $p(a,\lambda)$ di $C(e)$, allora $r(0,0,-a,-\lambda) = p(a,\lambda)$.
Consideriamo un elemento $q(a,b,\lambda)$ di $C(s)$, allora $r(a+1,1,-b-\lambda^{-1},-\lambda) = q(a,b,\lambda)$.
Questo mostra che $C(e) \cup C(s) \subset C(s).C(s)$, ottenendo l'uguaglianza dei membri e concludendo la dimostrazione.

\end{proof}

\medskip
Arrivati a questo punto, possediamo tutti gli strumenti necessari a dimostrare il lemma di Bruhat,
obbiettivo principale della prima parte della tesi.

% 8.3.8
\begin{theorem}[Lemma di Bruhat]\label{BrhLmm}{\ \\}
$G$ è unione disgiunta della classi laterali doppie $C(w)$ ($w \in W$).
\end{theorem}
\begin{proof}

Sia $G_1 = \bigcup_{w \in W} C(w)$ e sia $s = s_{\alpha} \in S$.

Dal precedente lemma abbiamo che
\[
	\bigcup_{w \in W} C(w) =
	\bigcup_{w \in W} C(sw) \subset
	\bigcup_{w \in W} C(s).C(w) \subset
	\bigcup_{w \in W} C(w) \cup C(sw) =
	\bigcup_{w \in W} C(w)
\]
dunque $C(s).G_1 = \bigcup_{w \in W} C(s).C(w) = \bigcup_{w \in W} C(w) = G_1$.

Dato che $T, U_{\alpha} \subset B$ e $C(w) = B \dot{w} B$, abbiamo che $C(w)$ è stabile per moltiplicazione di $T$ e $U_{\alpha}$ per ogni $w \in W$, quindi anche $G_1$.

Inoltre
\[
	U_{-\alpha}.G_1 =
	\dot{s_{\alpha}} U_{\alpha} \dot{s_{\alpha}}.G_1 =
	\dot{s_{\alpha}} (U_{s_{\alpha}^{-1}} \dot{s_{\alpha}} B).G_1 =
	\dot{s_{\alpha}} C(s_{\alpha}).G_1 =
	\dot{s_{\alpha}}.G_1 =
	\dot{s_{\alpha}} C(s_{\alpha} w_0).G_1
\]
Ma, notando che $(s_{\alpha} w_0)^{-1} = w_0 s_{\alpha}$, si trova
\begin{align*}
	\dot{s_{\alpha}} C(s_{\alpha} w_0)
	&= \dot{s_{\alpha}} (U_{w_0 s_{\alpha}} \dot{s_{\alpha}} \dot{w_0} B)
	= \dot{s_{\alpha}} \left( \prod_{\beta \in R(w_0 s_{\alpha})} U_{\beta} \right) \dot{s_{\alpha}} \dot{w_0} B\\
	&= \left( \prod_{\beta \in R(w_0 s_{\alpha})} \dot{s_{\alpha}} U_{\beta} \dot{s_{\alpha}} \right) \dot{w_0} B
	= \left( \prod_{\beta \in R(w_0 s_{\alpha})} U_{s_{\alpha}.\beta} \right) \dot{w_0} B\\
	&= \left( \prod_{\beta \in s_{\alpha}.R(w_0 s_{\alpha})} U_{\beta} \right) \dot{w_0} B
\end{align*}
Per~\ref{RSymOnWeyl}, dato che $w_0 s_{\alpha}.\alpha = \alpha \in R^+$, si ha che $s_{\alpha}.R(w_0 s_{\alpha}) \subset R(w_0)$, perciò
\[
	\dot{s_{\alpha}} C(s_{\alpha} w_0) \subset
	\left( \prod_{\beta \in R(w_0)} U_{\beta} \right) \dot{w_0} B =
	U_{w_0} \dot{w_0} B =
	C(w_0)
\]
da cui $U_{-\alpha}.G_1 = \dot{s_{\alpha}} C(s_{\alpha} w_0). G_1 \subset C(w_0).G_1 = G_1$.
L'inclusione $G_1 \subset U_{-\alpha}.G_1$ è scontata, quindi $G_1$ è stabile anche per moltiplicazione per $U_{-\alpha}$.
Dato che $T$, $U_{\alpha}$ e $U_{-\alpha}$ ($\alpha \in D$) generano $G$ per~\ref{TUapmGenG}, $G_1 = G$.

Questo mostra che $G$ è unione delle classi laterali doppie $C(w)$, procediamo a mostrare che tale unione è disgiunta.
Siano $w, w' \in W$ e supponiamo $C(w) \cap C(w') \neq \emptyset$.

Essendo entrambi orbite dell'azione
\[ (B \times B) \times G \to G \qquad (b,b';g) \mapsto b g b'^{-1} \]
si deve avere $C(w) = C(w')$.

È sufficiente mostrare che $C(w) = C(w')$ implica $w = w'$.
Per~\ref{dimCw}
\[ l(w) = \dim(C(w)) - \dim(B) = \dim(C(w')) - \dim(B) = l(w') \]
Se $l(w) = l(w') = 0$ si ha necessariamente $w = e = w'$, quindi possiamo supporre $l(w) > 0$ e procedere induttivamente su $l(w)$.
Sia $s$ il primo elemento di una decomposizione ridotta di $w$, in modo che $l(sw) = l(w)-1$.
Per~\ref{CsCw}
\[
	C(sw) \subset C(s).C(w) = C(s).C(w') \subset C(w') \cup C(sw')
\]
quindi $C(sw) = C(w')$ o $C(sw) = C(sw')$ per quanto appena mostrato.
Il primo caso implica $l(sw) = l(w') = l(w)$ per quanto visto poche righe fa, assurdo.
Il secondo caso implica, per ipotesi induttiva, che $sw = sw'$, quindi $w = w'$.
\end{proof}

\medskip
Come conseguenza otteniamo una scrittura univoca di ogni elemento di $G$ nel seguente modo:

% 8.3.9
\begin{corollary}[Decomposizione di Bruhat]\label{BrhDec}{\ \\}
Ogni elemento di $G$ può scrivere univocamente nella forma $u \dot{w} b$ con $w \in W$, $u \in U_{w^{-1}}$, $b \in B$.
\end{corollary}
\begin{proof}
La tesi discende direttamente dal teorema precedente e da~\ref{AhxBisoCw} (ii).
\end{proof}
