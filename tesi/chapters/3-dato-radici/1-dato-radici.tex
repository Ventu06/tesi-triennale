L'obiettivo di questo capitolo è associare un sistema di radici ad un dato gruppo algebrico lineare,
così da poter sfruttare la teoria sviluppata sui sistemi di radici nello studio del gruppo.
Per raggiungere questo scopo introdurremo il dato di radici, un oggetto che si collega naturalmente ad entrambi.
In questa sezione vedremo il dato di radici come concetto combinatorico, definito e studiato
senza riferimenti ai gruppi algebrici, e costruiremo un sistema di radici ad esso associato.
Anche qui utilizzeremo notazioni note, come ``radici'', ``coradici'', ``accoppiamento'', \dots,
sempre per enfatizzare legami che espliciteremo successivamente, in parte in questa sezione e
in parte nella prossima.

\medskip
Introduciamo subito la nozione di dato di radici:

% 7.4.1
\begin{definition}[Dato di Radici]\label{rootdatum}{\ \\}
	Un \textit{dato di radici} è una quadrupla $\Psi = (X, R, X^{\vee}, R^{\vee})$ in cui:
\begin{enumerate}[(a)]
	\item $X$ e $X^{\vee}$ sono gruppi abeliani liberi di rango finito ed è dato un accoppiamento $\langle \cdot , \cdot \rangle : X \times X^{\vee} \to \Zb$;
	\item $R$ e $R^{\vee}$ sono sottoinsiemi finiti di $X$ e $X^{\vee}$ ed è data una bigezione $\alpha \mapsto \alpha^{\vee}$ da $R$ in $R^{\vee}$.
\end{enumerate}
Dato $\alpha \in R$, definiamo gli endomorfismi $s_{\alpha}$ e $s_{\alpha}^{\vee}$ di $X$ e $X^{\vee}$ ponendo
\[
	s_{\alpha}(x) = x - \langle x, \alpha^{\vee} \rangle \alpha
	\qquad
	s_{\alpha}^{\vee}(y) = y - \langle \alpha, y \rangle \alpha^{\vee}
\]
Imponiamo altre due condizioni al dato di radici:
\begin{enumerate}[(DR 1)]
	\item $\langle \alpha, \alpha^{\vee} \rangle = 2$ per ogni $\alpha \in R$
	\item $s_{\alpha}(R) = R$ e $s_{\alpha}^{\vee} (R^{\vee}) = R^{\vee}$ per ogni $\alpha \in R$
\end{enumerate}
\end{definition}

\medskip
Sotto alcune semplici condizioni, che vedremo soddisfatte dai dati di radici associati ai gruppi oggetto di studio,
possiamo definire un sistema di radici associato a un dato di radici.

\subsection
Se $\Psi = (X, R, X^{\vee}, R^{\vee})$ è un dato di radici, poniamo $V = \Rb \otimes_{\Zb} \langle R \rangle$,
dove $\langle R \rangle$ è il sottogruppo di $X$ generato da $R$.
Notiamo che $\langle R \rangle$ è un gruppo abeliano libero finitamente generato in quanto sottogruppo di $X$,
quindi la mappa $x \mapsto 1 \otimes_{\Zb} x$ è iniettiva e possiamo identificare $\alpha \in R$ con $1 \otimes_{\Zb} \alpha$.
Supponiamo che per ogni $\alpha, \beta \in R$ con $\alpha \neq \pm \beta$ non esista $p \in \Qb$ con $\alpha = p \beta$,
allora $\alpha$ e $\beta$ sono linearmente indipendenti su $V$ per le proprietà del prodotto tensore.
Supponiamo inoltre esista un prodotto scalare $(\cdot, \cdot)$ definito positivo su $V$ tale che per ogni
$\alpha \in R$, $x \in \langle R \rangle$ si abbia $\langle x, \alpha^{\vee} \rangle = 2 (\alpha, \alpha)^{-1} (x, \alpha)$.
Se queste due condizioni sono soddisfatte è immediato verificare che $(V,R)$ è un sistema di radici,
che chiameremo sistema di radici associato a $\Psi$.

\medskip
Si noti come le notazioni sono coerenti, cioè l'accoppiamento $\langle \cdot, \cdot \rangle$ e gli elementi $\alpha^{\vee}$ di $X^{\vee}$
corrispondono a quelli definiti nel capitolo precedente, a meno di considerare il funtore $\Rb \otimes_{\Zb} \cdot$.

\medskip
D'ora in poi sia $(V,R)$ un sistema di radici associato a $\Psi$.
Diamo una definizione alternativa di sistema di radici positive:

% 7.4.5
\subsection
$R^+$ è un sistema di radici positive se esiste $\lambda \in X^{\vee}$ con
$\langle \alpha, \lambda \rangle \neq 0$ per ogni $\alpha \in R$ tale che
\[
	R^+ = \{ \alpha \in R \mid \langle \alpha, \lambda \rangle > 0 \}
\]

\begin{lemma}
La definizione originale di sistema di radici positive, che ricordiamo essere un sottoinsieme $R^+$ di $R$ per il quale esiste
$x \in V$ con $(\alpha, x) \neq 0$ per ogni $\alpha \in R$ tale che $R^+ = \{ \alpha \in R \mid (\alpha, x) > 0 \}$,
e quella alternativa appena fornita sono effettivamente equivalenti.
\end{lemma}
\begin{proof}
Preso $x \in V$ come nella definizione originale, consideriamo la proiezione ortogonale $x'$ di $x$ su $\langle R \rangle$, in modo che
$(\alpha, x) = (\alpha, x')$ per ogni $\alpha \in R$.
Scriviamo $x' = \sum_{\beta \in R} c_{\beta} \beta$ con $p_{\beta}$, allora
\[
	(\alpha, x') =
	\sum_{\beta \in R} c_{\beta} (\alpha, \beta) =
	\sum_{\beta \in R} \frac{1}{2} c_{\beta}(\beta, \beta) \langle \alpha, \beta^{\vee} \rangle
\]
Approssimando sufficientemente $\frac{1}{2} c_{\beta}(\beta, \beta)$ come $\frac{p_{\beta}}{q_{\beta}}$
con $p_{\beta}, q_{\beta} \in \Zb$ abbiamo che
$\sum_{\beta \in R} \frac{p_{\beta}}{q_{\beta}} \langle \alpha, \beta^{\vee} \rangle$
è nullo per nessun $\alpha \in R$ e positivo per tutti e soli gli $\alpha \in R^+$.
Detto $q = \prod_{\beta \in R} q_{\beta}$, chiamiamo $\lambda = \sum_{\beta \in R} p_{\beta} \frac{q}{q_{\beta}} \beta^{\vee}$,
allora $\lambda \in X^{\vee}$, $\langle \alpha, \lambda \rangle \neq 0$ per ogni $\alpha \in R$ e
$\langle \alpha, \lambda \rangle > 0$ se e solo se $\alpha \in R^+$.

Viceversa, sia $\lambda \in X^{\vee}$ come nella definizione alternativa.
Tramite l'accoppiamento possiamo vedere $\lambda$ come un elemento di $\Hom(\langle R \rangle, \Zb)$,
dunque $\Id \otimes_{\Zb} \lambda$, che identifichiamo con $\lambda$, è un elemento di $\Hom(V, \Rb) = V^{\vee}$.
Per il teorema di rappresentazione di Riesz esiste $x \in V$ con
$\langle \alpha, \lambda \rangle = (\alpha, x)$ che dunque soddisfa tutte le proprietà richieste.
\end{proof}

\medskip
Definiamo infine un ordinamento parziale su $X$ che utilizzeremo soprattutto nell'ultimo capitolo:

\subsection\label{ordpos}
Un sistema di radici positive $R^+$ induce un ordinamento parziale su $X$:
dati $x,y \in X$, diciamo che $x \ge y$ se esistono $n_{\alpha} \in \Nb$ al variare di $\alpha \in R^+$ tali che
\[
	x = y + \sum_{\alpha \in D} n_{\alpha} \alpha
\]

Per~\ref{aMinbInR} e dato che le radici sono in numero finito, l'insieme $D$ è l'insieme degli elementi minimali di $R^+$ secondo il precedente ordinamento.
