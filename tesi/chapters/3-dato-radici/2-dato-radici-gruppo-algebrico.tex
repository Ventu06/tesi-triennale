In questa sezione mostreremo come caratteri, radici, cocaratteri e coradici di un gruppo algebrico lineare
formino un dato di radici e inducano un sistema di radici.
Vedremo anche lo stretto legame fra sistemi di radici positive e sottogruppi di Borel.

\medskip
Sia $G$ un gruppo algebrico lineare connesso riduttivo e sia $T$ un suo toro massimale.
Indichiamo con $X$ il gruppo dei caratteri $X^*(T)$ e con $X^{\vee}$ il gruppo dei cocaratteri $X_*(T)$.
Siano $R$ e $R^{\vee}$ gli insiemi delle radici e delle coradici di $G$, definiti rispettivamente in~\ref{rootDef} e in~\ref{corootDef}.
Ricordiamo inoltre la definizione di $V$ e $V^{\vee}$ data in~\ref{XVXvVv}.

\medskip
Verifichiamo che le radici sono in bigezione con le coradici.

% 7.4.3
\begin{lemma}
La mappa da $R$ in $R^{\vee}$ data da $\alpha \mapsto \alpha^{\vee}$ è una bigezione.
\end{lemma}
\begin{proof}
Per definizione di $R^{\vee}$ basta verificare l'iniettività.
Se per assurdo $\alpha, \beta \in R$ e $\alpha^{\vee} = \beta^{\vee}$, allora
\[
	s_{\alpha}s_{\beta}(x) = x + \langle x, \alpha^{\vee} \rangle (\alpha - \beta) \qquad (x \in X)
\]
Se $x$ è un autovettore di questa mappa, allora $\langle x, \alpha^{\vee} \rangle (\alpha - \beta) \in \Span(\{x\})$,
quindi $\langle x, \alpha^{\vee} \rangle = 0$ o $(\alpha - \beta) \in \Span(\{x\})$, e visto che
$\langle \alpha - \beta, \alpha^{\vee} \rangle = \langle \alpha, \alpha^{\vee} \rangle - \langle \beta, \beta^{\vee} \rangle = 0$,
anche in tal caso $\langle x, \alpha^{\vee} \rangle = 0$.
Segue che $1$ è l'unico autovalore dell'endomorfismo considerato
(nel caso di autovettori non reali, è sufficiente estendere l'accoppiamento a $\Cb$ nello stesso modo in cui è stato esteso ad $\Rb$).
Dato che il gruppo di Weyl è finito, la mappa in questione ha ordine finito, quindi è diagonalizzabile e perciò $s_{\alpha}s_{\beta} = e$, da cui concludiamo
$\alpha - \beta = \frac{1}{2} \langle \alpha, \alpha^{\vee} \rangle (\alpha - \beta) = \frac{1}{2}(s_{\alpha}s_{\beta}(\alpha) - \alpha) = 0$.
\end{proof}

\medskip
I seguenti risultati sono di fondamentale importanza in quanto permettono di collegare gruppi algebrici lineari e sistemi di radici.

\begin{proposition}
La quadrupla $\Psi = (X, R, X^{\vee}, R^{\vee})$ è un dato di radici.
\end{proposition}
\begin{proof}
$\Psi$ soddisfa le condizioni (a) e (b) di~\ref{rootdatum} ed anche (DR 1) per~\ref{V-Vc_pairing} (i).
Per concludere non ci resta che mostrare che soddisfa anche (DR 2):
per la prima parte, data $\alpha \in R$, si ha che $s_{\alpha} \in W$ per definizione,
gruppo che ricordiamo permutare gli elementi di $R$, quindi $s_{\alpha}(R) = R$.

Mostriamo che $s_{\alpha}^{\vee}(R^{\vee}) = R^{\vee}$.
Date $\alpha, \gamma \in R$, per quanto detto si avrà
$s_{\alpha}(\gamma) = \gamma - \langle \gamma, \alpha^{\vee} \rangle \alpha = \delta \in R$.
Detta $\xi = s_{\alpha}^{\vee}(\gamma^{\vee}) = \gamma^{\vee} - \langle \alpha, \gamma^{\vee} \rangle \alpha^{\vee}$,
se mostriamo che $\xi = \delta^{\vee}$ possiamo concludere che $s_{\alpha}^{\vee}(R^{\vee}) = R^{\vee}$.
Per~\ref{V-Vc_pairing} (i) $\xi = \delta^{\vee}$ se e solo se $\langle \delta, \xi \rangle = 2$ e
$\langle x, \xi \rangle = 2 (x,x)^{-1} (x, \delta)$.
La prima uguaglianza segue da
\begin{align*}
	\langle \delta, \xi \rangle
	&= \langle \gamma - \langle \gamma, \alpha^{\vee} \rangle \alpha, \gamma^{\vee} - \langle \alpha, \gamma^{\vee} \rangle \alpha^{\vee} \rangle\\
	&= \langle \gamma, \gamma^{\vee} \rangle
		+ \langle \gamma, \alpha^{\vee} \rangle \langle \alpha, \gamma^{\vee} \rangle \langle \alpha, \alpha^{\vee} \rangle
		- \langle \alpha, \gamma^{\vee} \rangle \langle \gamma, \alpha^{\vee} \rangle
		- \langle \gamma, \alpha^{\vee} \rangle \langle \alpha, \gamma^{\vee} \rangle\\
	&= 2 + (2-1-1) \langle \gamma, \alpha^{\vee} \rangle \langle \alpha, \gamma^{\vee} \rangle\\
	&= 2
\end{align*}
Per quanto riguarda la seconda, si ha
\begin{align*}
	\langle x, \xi \rangle
	&= \langle x, \gamma^{\vee} \rangle - \langle \alpha, \gamma^{\vee} \rangle \langle x, \alpha^{\vee} \rangle\\
	&= 2 (x,x)^{-1} (x, \gamma) - 2 \langle \alpha, \gamma^{\vee} \rangle (x,x)^{-1} (x, \alpha)\\
	&= 2 (x,x)^{-1} (x, \gamma - \langle \alpha, \gamma^{\vee} \rangle \alpha)\\
	&= 2 (x,x)^{-1} (x, \delta)
\end{align*}
\end{proof}

\medskip
Vediamo che $(V,R)$ è un sistema di radici associato a $\Psi$:
la condizione sul prodotto scalare abbiamo visto essere soddisfatta,
mentre il seguente lemma fornisce le condizioni rimanenti.

% 7.4.4
\begin{lemma}\label{Rrid}
Se $\alpha \in R$, $c \in \Qb$ e $c\alpha \in R$, allora $c = \pm 1$.
\end{lemma}
\begin{proof}
Sappiamo che $G_{\alpha} = G_{c\alpha}$, ma per~\ref{GaRoots} le radici di $G_{\alpha}$ sono $\pm \alpha$, da cui $c = \pm 1$.
\end{proof}

\medskip
Nel successivo paragrafo utilizzeremo un sottogruppo di Borel per scegliere un sottoinsieme delle radici.
La successiva proposizione ci mostra come il sottogruppo scelto sia un sistema di radici positive,
concludendo il collegamento fra gruppi algebrici lineari e sistemi di radici.

\subsection
Sia $B$ un sottogruppo di Borel di $G$ contenente $T$ e sia $\alpha \in R = R(G,T)$.
Applicando~\ref{ZGScapB} (ii) a $G_{\alpha} = Z_G(\Ker(\alpha)^0)$ si trova che $B' = G_{\alpha} \cap B$ è un sottogruppo di Borel di $G_{\alpha}$.
Sia $T' = G_{\alpha} \cap T$ il toro massimale di $G_{\alpha}$, avente $\pm \alpha$ come radici per~\ref{GaRoots}.
Vediamo da~\ref{UaTBperGSS1} (ii) che $L(B')$ è somma diretta di $L(T')$ e uno spazio-peso $1$-dimensionale, corrispondente a una delle due radici.
In questo modo si può scegliere una radice da ogni coppia $\pm \alpha$; sia $R^+(B)$ l'insieme di radici ottenuto lasciando variare $\alpha$ in $R$.

% 7.4.6
\begin{proposition}
$R^+(B)$ è un sistema di radici positive.
\end{proposition}
\begin{proof}
Per~\ref{RapprStab} possiamo considerare una rappresentazione $\phi : G \to GL(A)$ e un vettore non nullo $a \in A$ tali che
$B$ sia lo stabilizzatore della linea $ka$.
In questo modo esiste un carattere $\chi$ di $B$, quindi anche di $T$, tale che $\phi(b)(a) = \chi(b)a$.
Sia $\alpha \in R^+(B)$ e scegliamo $t \in T \cap G_{\alpha}, x \in \Gb_a$ tali che
$t u_{-\alpha}(x) t^{-1} u_{-\alpha}(x)^{-1} = u_{-\alpha}(((-\alpha)(t)-1)x) \notin B$,
dunque $t u_{-\alpha}(x) t^{-1} u_{-\alpha}(x)^{-1} \notin \Stab(a)$.

Consideriamo una funzione lineare $l$ su $A$ tale che $l(a) \neq l(\phi(t u_{-\alpha}(x) t^{-1} u_{-\alpha}(x)^{-1})(a))$
e poniamo $F(g) = l(\phi(g)(a))$ ($g \in G$), allora si ha $F(gb) = \chi(b)F(g)$ ($b \in B, g \in G$).
Notiamo che la restrizione di $F$ a $(G_{\alpha}, G_{\alpha})$ è non costante in quanto
\[
	F(t u_{-\alpha}(x) t^{-1} u_{-\alpha}(x)^{-1}) = l(\phi(t u_{-\alpha}(x) t^{-1} u_{-\alpha}(x)^{-1})(a)) \neq l(a) = F(e)
\]
quindi per~\ref{Gachiage0} $\langle \chi, \alpha^{\vee} \rangle > 0$.
Allora, ricordando~\ref{V-Vc_pairing} (i), $(\alpha, \chi) = (\chi, \alpha) = \frac{1}{2}(\alpha,\alpha) \langle \chi, \alpha^{\vee} \rangle > 0$.
Dovendo quest'ultima disuguaglianza valere per ogni $\alpha \in R^+(B)$, abbiamo che $R^+(B)$ è un sistema di radici positive di $R$.
\end{proof}
