Lo scopo di questa tesi è dimostrare in primo luogo la decomposizione di Bruhat e successivamente il teorema di Borel-Weil.

\medskip
Innanzitutto introduciamo alcuni concetti necessari a enunciare questi risultati.
Consideriamo un gruppo algebrico lineare $G$, cioè una varietà algebrica affine su un campo $k$ algebricamente chiuso sulla quale è definita una struttura di gruppo
le cui operazioni sono morfismi.
Si può dimostrare che $G$ è isomorfo a un sottogruppo chiuso di $\GLb_n$, utilizzeremo questo isomorfismo per guidare l'intuito sui vari concetti
che ora verranno introdotti.

Chiamiamo radicale unipotente di $G$ e indichiamo con $R_u(G)$ il sottogruppo chiuso, connesso, normale e costituito da elementi unipotenti
massimale per queste proprietà.
Aggiungiamo alle ipotesi che $G$ sia connesso e che sia riduttivo, cioè che $R_u(G) = \{e\}$.

Un sottogruppo di Borel $B$ di $G$ è un sottogruppo chiuso, connesso, risolubile e massimale.
Tale sottogruppo esiste sempre ed è unico a meno di coniugio.
Nel caso $G = \GLb_n$, $B$ coincide, a meno di coniugio, con il sottogruppo delle matrici triangolari superiori.

Un un toro massimale $T$ di $G$ è un sottogruppo di $G$ isomorfo a un gruppo di matrici diagonali $\Db_n$ e massimale per questa proprietà.
Tale sottogruppo esiste sempre, è unico a meno di coniugio e possiamo assumere sia contenuto in $B$.
Nel caso $G = \GLb_n$, $T$ coincide, a meno di coniugio, con il sottogruppo delle matrici diagonali.

Definiamo il gruppo di Weyl $W$ di $(G,T)$ come $N_G(T) / Z_G(T)$, cioè come quoziente del normalizzatore di $T$
per il centralizzatore di $T$.
$W$ è un gruppo finito e agisce fedelmente sulle radici, permutandole.
Per ogni elemento $w \in W$ indichiamo con $\dot{w} \in N_G(T)$ un suo rappresentante.

Enunciamo ora la decomposizione di Bruhat:

\begin{theorem*}[Decomposizione di Bruhat]{\ \\}
$G$ è unione disgiunta delle classi laterali doppie $C(w) = B \dot{w} B$:
\[
	G = \bigsqcup_{w \in W} C(w) = \bigsqcup_{w \in W} B \dot{w} B
\]
\end{theorem*}

Cerchiamo ora di capire l'importanza da tale scrittura studiando più in dettaglio il sottogruppo di Borel,
poi ripercorriamo i passi principali della dimostrazione.

Prima però introduciamo un concetto molto utile allo studio del sottogruppo di Borel, cioè quello di radice.
Definiamo i caratteri di $T$ come i morfismi moltiplicativi $T \to k^*$ e indichiamo il loro insieme con $X^*(T)$.
Consideriamo l'azione di $T$ sull'algebra di Lie $\gf$ di $G$ data dalla rappresentazione aggiunta:
chiamiamo radici i caratteri $\alpha$ di $T$ per i quali esiste $X \in \gf$ con $t.X = \alpha(t) X$ per ogni $t \in T$
e indichiamo il loro insieme con $R$.

Ad ogni radice $\alpha \in R$ possiamo associare un elemento $s_{\alpha} \in W$, detto riflessione.
Scegliendo un insieme di radici positive $R^+$ e considerando il relativo sottoinsieme $D$ di radici minimali,
cioè non esprimibili come somma di radici positive, riusciamo a scrivere ogni radice di $R$ univocamente come somma di elementi di $D$
e ogni elemento di $W$ come composizione di riflessioni $s_{\alpha}$ con $\alpha \in D$.
In questo modo si riescono a ordinare parzialmente gli elementi di $W$ in una gerarchia, dove da un lato c'è l'identità e dall'altro
l'elemento $w_0$ che scambia gli insiemi $R^+$ e $R \setminus R^+$.

Torniamo al sottogruppo di Borel, che a questo punto è relativamente facile da studiare:
possiamo decomporre $B$ come $T \times U$, dove $U$ è il radicale unipotente di $B$.
Nel caso $G = \GLb_n$, $U$ coincide con il sottogruppo delle matrici triangolari superiori le cui entrate nella diagonale sono tutte pari ad $1$.
Ad ogni radice $\alpha \in R$ possiamo associare un sottogruppo $U_{\alpha}$ di $G$ isomorfo al gruppo additivo $\Gb_a = (k,+)$,
in modo che, preso un isomorfismo $u_{\alpha} : \Gb_a \to U_{\alpha}$, si abbia $t u_{\alpha}(x) t^{-1} = u_{\alpha}(\alpha(t)x)$.
Nel caso $G = \GLb_n$, ad ognuna delle $\frac{1}{2}n(n-1)$ celle non sulla diagonale è associata una radice e i relativi sottogruppi
$U_{\alpha}$ sono costituiti dalle matrici aventi $1$ sulla diagonale e $0$ altrove tranne che su una specifica cella fuori dalla diagonale.
Facendo agire $T$ per coniugio su $U$ troviamo una decomposizione di $U$ come prodotto di $U_{\alpha}$ con $\alpha \in R^+$.
Infine è possibile scrivere il commutatore di due elementi $u_{\alpha}(x) \in U_{\alpha}$, $u_{\beta}(y) \in U_{\beta}$
come funzione regolare utilizzando i vari $u_{\gamma}$ al variare di $\gamma \in R^+$ in un modo deducibile
esclusivamente dall'insieme delle radici.
Se $G = \GLb_n$, tutti i precedenti passaggi sono relativamente intuitivi e facilmente verificabili a mano.
In questo modo abbiamo caratterizzato completamente la struttura di gruppo di $B$,
riconducendola a un prodotto di gruppi additivi $(k,+)$ e moltiplicativi $(k,*)$ che interagiscono fra loro
nel modo descritto dalle radici, oggetto combinatorico finito che si può studiare senza eccessive difficoltà.

Il discorso appena fatto non solo lascia intuire l'importanza della decomposizione di Bruhat, ma ci permette anche di dare un'idea della sua dimostrazione.
Sappiamo che gli elementi di $W$ permutano i gruppi $U_{\alpha}$ ($\alpha \in R$), i quali, insieme a $T$, generano $G$.
Mettendo in relazione la struttura di $W$ con il prodotto di alcune classi laterali doppie $C(s).C(w)$ si riesce a vedere
che $\bigcup_{w \in W} C(w)$ è stabile per l'azione di $T$ e di ogni $U_{\alpha}$, da cui $\bigcup_{w \in W} C(w) = G$.
Inoltre, sfruttando la gerarchia definita su $W$, si trova che $C(w) \cap C(w') = \emptyset$ per $w \neq w'$,
dimostrando che $G = \bigsqcup_{w \in W} C(w)$.

\medskip
Questa prima parte si articola nei primi quattro capitoli, la cui referenza principale è rappresentata da~\cite{lag_spr}.

Nel primo capitolo si introducono i gruppi algebrici lineari con tutti i concetti collegati, fra cui quelli presentati nel precedente paragrafo,
poi si enunciano, senza dimostrazione, le loro principali proprietà.
Questo capitolo è pensato come un riepilogo di una parte di teoria che dovrebbe essere già nota, nel quale a volte si cercano di fornire
le idee che stanno alla base di un risultato, ma senza mai scendere troppo nel dettaglio, inoltre è utile per fissare le notazioni.
Per approfondire ulteriormente alcune parti di questo capitolo, in particolare quelle riguardanti l'algebra di Lie, si rimanda anche a~\cite{lag_hmp}.

I successivi tre capitoli sono invece molto più dettagliati e i risultati ottenuti vengono dimostrati rigorosamente.

Il secondo capitolo tratta i sistemi di radici, oggetti puramente combinatorici che, almeno fino a questo punto, non hanno relazioni con
i gruppi algebrici o con la teoria precedentemente introdotta.
Fra le nozioni discusse nel primo paragrafo, qui possiamo trovare le riflessioni, le radici semplici, e la gerarchia su $W$.
Per approfondire maggiormente questi argomenti si rimanda a~\cite{ilart_hmp}.

Il terzo capitolo rappresenta un ponte fra i sistemi di radici e i gruppi algebrici: si introduce il dato di radici, inizialmente come oggetto
combinatorico a sé stante, poi si associa ad esso un sistema di radici e infine si mostra come ad ogni gruppo algebrico lineare sia
associato un dato di radici.
In questo modo sarà possibile applicare tutta la teoria sviluppata nel secondo capitolo ai nostri gruppi.

Il quarto capitolo ha lo scopo di dimostrare la decomposizione di Bruhat e per raggiungere tale obbiettivo si avvale ampiamente
della teoria sviluppata nel secondo capitolo, come mostrato nella discussione precedente.

\medskip
Il teorema di Borel-Weil risolve il problema della classificazione delle rappresentazioni irriducibili dei gruppi algebrici lineari riduttivi
a meno di isomorfismo, fornendo un rappresentante canonico per ognuna di esse.

Dato un gruppo algebrico lineare riduttivo $G$, una rappresentazione di $G$ è un'azione razionale di $G$ su uno spazio vettoriale $M$ costituita da mappe lineari.
Dato uno spazio vettoriale $M$ su cui è definita una rappresentazione di $G$, diremo che $M$ è un $G$-modulo.
Un $G$-modulo $M$ si dice semplice e la relativa rappresentazione irriducibile se $M$ è non nullo e
gli unici $G$-sottomoduli di $M$, cioè i sottospazi vettoriali di $M$ che siano anche $G$-moduli con l'azione indotta,
sono $0$ e $M$.

Definiamo i cocaratteri di $T$ come i morfismi moltiplicativi $k^* \to T$ e indichiamo il loro insieme con $X_*(T)$.
Ad ogni radice $\alpha$ possiamo associare una coradice $\alpha^{\vee} \in X_*(T)$.
Definito l'accoppiamento $\langle \cdot, \cdot \rangle$ fra $X^*(T)$ e $X_*(T)$, una funzione bilineare che, fra le altre proprietà,
soddisfa $\langle \alpha, \alpha^{\vee} \rangle = 2$, possiamo introdurre l'insieme dei pesi dominanti
\[
	X(T)_+ = \{ \chi \in X^*(T) \mid \langle \chi, \alpha^{\vee} \rangle \ge 0 \quad \forall \alpha \in R^+ \}
\]
I pesi dominanti sono particolarmente importanti in quanto in bigezione con le rappresentazioni irriducibili di $G$ a meno di isomorfismo.
Preso $\chi \in X^*(T)$ sia
\[
	H^0(\chi) = \{ f \in k[G] \mid f(gb) = \chi(b)^{-1} f(g) \quad \forall g \in G, b \in B^- \}
\]
dove $B^-$ è il sottogruppo di Borel di $G$ dato da $B^- = \dot{w_0} B \dot{w_0}^{-1}$.
Chiamiamo $B^+ = B$, $U^+$ il radicale unipotente di $B^+$ e $U^-$ il radicale unipotente di $B^-$.
Nel caso in cui $G = \GLb_n$ e $B^+$ è il sottogruppo delle matrici triangolari superiori, $B^-$ coincide con il
sottogruppo delle matrici triangolari inferiori.

Dato un $G$-modulo $M$, indichiamo con $\soc_G M$ la somma diretta di tutti i $G$-sottomoduli semplici di $M$.
Si verifica facilmente che $\soc_G M$ è un sottomodulo di $M$.
Preso $\chi \in X^*(T)$ sia
\[
	L(\chi) = \soc_G H^0(\chi)
\]
In caratteristica $0$ è possibile dimostrare che $\soc_G M = M$ per ogni $G$-modulo $M$, quindi anche $L(\chi) = H^0(\chi)$.

A questo punto possiamo enunciare il teorema di Borel-Weil:
\begin{theorem*}[Borel-Weil]{\ \\}
L'insieme $\{L(\chi) \mid \chi \in X(T)_+ \}$ fornisce un sistema di rappresentanti per le classi di isomorfismo dei $G$-moduli semplici.
\end{theorem*}

Non è difficile mostrare che ogni $L(\chi)$ non nullo è un $G$-modulo semplice e che ogni $G$-modulo semplice è isomorfo a un qualche $L(\chi)$ non nullo.
La parte interessante della dimostrazione del teorema di Borel-Weil riguarda il fatto che $H^0(\chi) \neq 0$ se e solo se $\chi \in X(T)_+$,
lemma che notiamo mettere in relazione indirettamente le radici di $G$ con le rappresentazioni irriducibili.
Mostrare che $H^0(\chi) \neq 0$ implica $\chi \in X(T)_+$ è abbastanza facile, mentre l'altra freccia
è decisamente meno banale e richiede l'uso della decomposizione di Bruhat.

Illustriamo brevemente l'idea della dimostrazione: l'obbiettivo è costruire un morfismo non nullo $f \in H^0(\chi)$.
Si inizia definendo $f$ sull'aperto denso $C(w_0) w_0^{-1} = U^+ B^-$ e la si estende su $C(s_{\alpha} w_0) w_0^{-1} = U^+ \dot{s_{\alpha}} B^-$ per ogni $\alpha \in D$,
dove $w_0$ è la ``riflessione totale'' e le $s_{\alpha}$ per $\alpha \in D$ sono le riflessioni associate alle radici semplici, tutte definite nel primo paragrafo.
A questo punto si riesce ad estendere la funzione $f$ su tutto $G$ grazie al teorema di estensione di Hartogs e concludere che $H^0(\chi) \neq 0$.

L'ipotesi di peso dominante è necessaria per estendere $f$ su $U^+ \dot{s_{\alpha}} B^-$:
notando che $U^+ \dot{s_{\alpha}} B^- \subset \dot{s_{\alpha}} U^+ B^-$, è sufficiente estendere $f$ ristretta a
$U^+ B^- \cap \dot{s_{\alpha}} U^+ B^-$ su $\dot{s_{\alpha}} U^+ B^-$;
si vede poi che
\begin{align*}
	\dot{s_{\alpha}} U^+ B^- &\simeq U_{\sim \alpha}^+ \times k \times T \times U^-\\
	U^+ B^- \cap \dot{s_{\alpha}} U^+ B^- &\simeq U_{\sim \alpha}^+ \times k^* \times T \times U^-
\end{align*}
dove $U_{\sim \alpha}^+ = \prod_{\beta \in R^+ \setminus \{\alpha\}} U_{\beta}$.
Presi $(u_1, x, t, u) \in U_{\sim \alpha}^+ \times k^* \times T \times U^-$ troviamo
\[
	f(u_1 \dot{s_{\alpha}} u_{\alpha}(x) t u) = \chi(t)^{-1} (-x)^{\langle \chi, \alpha^{\vee} \rangle}
\]
e l'ipotesi $\langle \chi, \alpha^{\vee} \rangle \ge 0$ ci permette di definire $f$ per $x=0$,
estendendola su $\dot{s_{\alpha}} U^+ B^-$.

\medskip
Questa seconda parte si articola negli ultimi due capitoli e la referenza principale è rappresentata da~\cite{rag_jnt}.

Il quinto capitolo, in modo simile al primo, è costituito da definizioni ed enunciati senza dimostrazione,
pensato come un insieme di prerequisiti utili da consultare e per fissare le notazioni in vista del sesto capitolo.
A differenza del primo, la quantità di teoria richiesta è molto più ridotta.

Il sesto capitolo ha come scopo la dimostrazione del teorema di Borel-Weil.
Dopo alcune proprietà generali delle rappresentazioni, si procede direttamente alla dimostrazione,
illustrata brevemente nel precedente paragrafo.

Infine si mostra una seconda forma del teorema di Borel-Weil, nella quale i rappresentanti per le classi di isomorfismo
delle rappresentazioni irriducibili sono costruiti in modo più geometrico e tangibile.
