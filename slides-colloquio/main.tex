\documentclass[10pt]{beamer}


\usepackage[italian]{babel}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{amssymb}
\usepackage{biblatex}
\usepackage{csquotes}

\uselanguage{Italian}
\languagepath{Italian}

\usetheme{Madrid}

\DeclareMathOperator\Id{Id}
\DeclareMathOperator\Hom{Hom}
\DeclareMathOperator\Ker{Ker}
\DeclareMathOperator\Img{Im}
\DeclareMathOperator\Ad{Ad}
\DeclareMathOperator\Span{Span}
\DeclareMathOperator\Der{Der}
\DeclareMathOperator\diag{diag}
\DeclareMathOperator\Stab{Stab}
\DeclareMathOperator\orb{orb}
\DeclareMathOperator\ord{ord}
\DeclareMathOperator\res{res}
\DeclareMathOperator\ind{ind}
\DeclareMathOperator\soc{soc}
\DeclareMathOperator\Sym{Sym}

\newcommand{\ootimes}{\mathbin{\overline{\otimes}}}
\newcommand{\oepsilon}{\overline{\epsilon}}

\newcommand{\Ab}{\mathbb{A}}
\newcommand{\Cb}{\mathbb{C}}
\newcommand{\Db}{\mathbb{D}}
\newcommand{\Gb}{\mathbb{G}}
\newcommand{\Nb}{\mathbb{N}}
\newcommand{\Pb}{\mathbb{P}}
\newcommand{\Qb}{\mathbb{Q}}
\newcommand{\Rb}{\mathbb{R}}
\newcommand{\Zb}{\mathbb{Z}}

\newcommand{\GLb}{\mathbf{GL}}
\newcommand{\SLb}{\mathbf{SL}}
\newcommand{\PSLb}{\mathbf{PSL}}
\newcommand{\Ub}{\mathbf{U}}

\newcommand{\Dc}{\mathcal{D}}
\newcommand{\Oc}{\mathcal{O}}
\newcommand{\Wc}{\mathcal{W}}

\renewcommand{\bf}{\mathfrak{b}}
\newcommand{\df}{\mathfrak{d}}
\newcommand{\gf}{\mathfrak{g}}
\newcommand{\hf}{\mathfrak{h}}
\newcommand{\tf}{\mathfrak{t}}
\newcommand{\zf}{\mathfrak{z}}
\newcommand{\glf}{\mathfrak{gl}}


\title{Il Teorema di Borel-Weil}
\author[Eduardo Venturini]{%
	Candidato: Eduardo Venturini%
	\texorpdfstring{\\}{}%
	Relatore: prof. Andrea Maffei%
}
\institute[SNS]{Scuola Normale Superiore}
\date{29 Aprile 2022}


\begin{filecontents}{bibliography.bib}
@book{mum,
	author = {David Mumford},
	title = {The Red Book of Varieties and Schemes},
	year = {1999},
	series = {Lecture Notes in Mathematics},
	edition = {2}
}

@book{spr,
	author = {T. A. Springer},
	title = {Linear Algebraic Groups},
	year = {1998},
	series = {Modern Birkhäuser Classics},
	edition = {2}
}

@book{jnt,
	author = {J. C. Jantzen},
	title = {Representations of Algebraic Groups},
	year = {2003},
	series = {Mathematical Surveys and Monographs},
	edition = {2}
}

@article{kam,
	author={Joel Kamnitzer},
	title={Lectures on geometric constructions of the irreducible representations of $\text{GL}_n$},
	year={2009},
	journal={arXiv}
}
\end{filecontents}

\addbibresource{bibliography.bib}


\begin{document}


\frame{\titlepage}


\begin{frame}
\frametitle{Rappresentazioni}

\pause
Sia $G$ un gruppo e $M$ uno spazio vettoriale su un campo $k$.

\pause
\begin{block}{Definizione - Rappresentazione}
Una rappresentazione di $G$ su $M$ è un omomorfismo $G \to \GLb(M)$.

\pause
Alternativamente, una rappresentazione di $G$ su $M$ è un'azione
$\Phi : G \times M \to M$ tale che:
\begin{itemize}
	\item $\Phi(g)$ è lineare
	\item $\Phi(e)(v) = e.v = v$
	\item $\Phi(gg')(v) = gg'.v = g.(g'.v) = \Phi(g)(\Phi(g')(v))$.
\end{itemize}
\end{block}

\pause
Un $G$-modulo è uno spazio vettoriale su cui è definita un'azione di $G$.

\end{frame}


\begin{frame}
\frametitle{Moduli semplici}

\pause
\begin{block}{Definizione - $G$-Sottomodulo}
Un $G$-sottomodulo è un sottospazio vettoriale di un $G$-modulo
chiuso per l'azione di $G$ indotta.
\end{block}

\pause
\begin{block}{Definizione - Modulo Semplice}
Un $G$-modulo $M$ si dice semplice e la relativa rappresentazione irriducibile
se è non nullo e i suoi unici $G$-sottomoduli sono $0$ e $M$.
\end{block}

\end{frame}


\begin{frame}
\frametitle{Il teorema di Borel-Weil}

\pause
\begin{alertblock}{Problema}
Dato un gruppo $G$, possiamo classificare tutti i $G$-moduli semplici a meno di isomorfismo?
\end{alertblock}

\pause
\begin{exampleblock}{Soluzione per gruppi di matrici}
Il teorema di Borel-Weil fornisce un insieme di rappresentanti per le classi di
equivalenza dei $G$-moduli semplici sotto le seguenti assunzioni:
\begin{itemize}
	\item $G$ è gruppo di matrici con determinate proprietà;
	\item l'azione di $G$ su $M$ si può esprimere come un insieme di funzioni regolari,
		cioè funzioni razionali	definite ovunque nelle componenti della matrice e del vettore.
\end{itemize}
\end{exampleblock}

\pause
In questa presentazione enunceremo il teorema di Borel-Weil, ne illustreremo brevemente la dimostrazione
e ne mostreremo le conseguenze.

\pause
Per semplicità ci restringiamo al caso in cui $k$ è un campo algebricamente chiuso di caratteristica $0$
e $G = \GLb_n(k)$, tuttavia le idee utilizzate si possono facilmente generalizzare a classi di gruppi più ampie.

\end{frame}


\begin{frame}
\frametitle{Tori e caratteri}

\pause
\begin{block}{Definizione - Toro Massimale}
Il toro massimale $T$ di $G$ è il sottogruppo di $G$ costituito dalle matrici diagonali.
\end{block}

\pause
\begin{block}{Definizione - Caratteri}
Un carattere di $\chi$ di $T$ è un omomorfismo razionale da $T$ nel gruppo moltiplicativo $k^*$.
\end{block}

\pause
Sia $X = X(T)$ l'insieme dei caratteri.

\medskip

\pause
I caratteri di $T$ sono le funzioni
\[
	\diag(g_{11}, \dots, g_{nn}) \mapsto g_{11}^{a_1} \dots g_{nn}^{a_n}
\]
con $(a_1, \dots, a_n) \in \Zb^n$.

\medskip

\pause
$X$ è un gruppo abeliano isomorfo a $\Zb^n$.

Siano $\epsilon_i(\diag(g_{11}, \dots, g_{nn})) = g_{ii}$ per $1 \le i \le n$.

$(\epsilon_1, \dots, \epsilon_n )$ costituiscono una base dei caratteri.

\end{frame}


\begin{frame}
\frametitle{Pesi dominanti e anticipazione sul teorema di Borel-Weil}

\pause
\begin{block}{Definizione - Pesi Dominanti}
Un carattere $\chi = a_1 \epsilon_1 + \dots + a_n \epsilon_n \in X$ è un peso dominante se $a_1 \ge a_2 \ge \dots \ge a_n$.
\end{block}

Sia $X_+$ l'insieme dei pesi dominanti.

\pause
\begin{exampleblock}{Enunciato parziale di Borel-Weil}
I pesi dominanti sono in bigezione con i $G$-moduli semplici a meno di isomorfismo.
\end{exampleblock}

\pause
La forma completa del teorema di Borel-Weil ci fornisce un modulo semplice per ogni peso dominante.
Procediamo a costruire tali rappresentanti.

\end{frame}


\begin{frame}
\frametitle{Costruzione di rappresentanti canonici}

\pause
Sia $B^+$ il gruppo delle matrici triangolari superiori e $B^-$ quello delle triangolari inferiori.

\pause
Dato un carattere $\chi : T \to k^*$, possiamo estenderne il dominio a $B^-$ utilizzando la proiezione
$B^- \to T$, la quale è un omomorfismo di gruppi.

\pause
Indichiamo con $k[G]$ le funzioni regolari di $G$, cioè le funzioni razionali
nelle componenti delle matrici definite ovunque.

\pause
\begin{block}{Definizione - $H^0(\chi)$}
Sia
\[
	H^0(\chi) = \{ f \in k[G] \mid f(gb) = \chi(b)^{-1}f(g) \quad \forall g \in G, b \in B^- \}
\]
con la struttura di $G$-modulo data dalla traslazione sinistra.
\end{block}

\pause
Dato un $G$-modulo $M$, sia $\soc_G M$ il sottomodulo di $M$
dato dalla somma diretta di tutti i sottomoduli semplici di $M$.

\pause
\begin{block}{Definizione - $L(\chi)$}
Sia $L(\chi) = \soc_G H^0(\chi)$
\end{block}

\end{frame}


\begin{frame}
\frametitle{Teorema di Borel-Weil e dimostrazione}

\pause
\begin{exampleblock}{Teorema - Borel-Weil}
L’insieme $\{ L(\chi) \mid \chi \in X_+ \}$ è un sistema di rappresentanti
per le classi di isomorfismo dei $G$-moduli semplici.
\end{exampleblock}

\pause
\begin{exampleblock}{Lemma 1 - Dimostrazione di Borel-Weil}
Sia $\chi \in X$ tale che $H^0(\chi) \neq 0$, allora $L(\chi)$ è semplice.
\end{exampleblock}

\pause
\begin{exampleblock}{Lemma 2 - Dimostrazione di Borel-Weil}
Ogni $G$-modulo semplice è isomorfo ad esattamente un $L(\chi)$ con $\chi \in X$ tale che $H^0(\chi) \neq 0$.
\end{exampleblock}

\pause
\begin{exampleblock}{Lemma 3 - Dimostrazione di Borel-Weil}
Per ogni $\chi \in X$, $H^0(\chi) \neq 0$ se e solo se $\chi$ è dominante.
\end{exampleblock}

\pause
Introduciamo alcuni concetti per dimostrare il lemma 3.

\end{frame}


%\begin{frame}
%\frametitle{Dimostrazione del Lemma 2 - Esistenza}

%\pause
%\begin{exampleblock}{Lemma 2 - Dimostrazione di Borel-Weil}
%Ogni $G$-modulo semplice è isomorfo ad esattamente un $L(\chi)$ con $\chi \in X$ tale che $H^0(\chi) \neq 0$.
%\end{exampleblock}

%\pause
%\begin{exampleblock}{Lemma}
%Ogni $G$-modulo semplice ha dimensione finita.
%\end{exampleblock}

%\pause
%\begin{exampleblock}{Lemma}
%Sia $M$ un $G$-modulo con $\dim(M) < +\infty$, allora esiste $\chi \in X$ tale che $\Hom_G(M, H^0(\chi)) \neq 0$.
%\end{exampleblock}

%\pause
%\begin{alertblock}{Dimostrazione Lemma 2 - Esistenza}
%Detto $M$ il $G$-modulo semplice, prendiamo $\phi \in \Hom_G(M, H^0(\chi))$ con $\phi \neq 0$.
%$\Ker(\phi)$ è semplice, quindi nullo, inoltre $\Img(\phi) \simeq M$ è semplice, quindi
%$\Img(\phi) \subset \soc_G H^0(\chi) = L(\chi)$, da cui $M \simeq L(\chi)$.
%\end{alertblock}

%\end{frame}


%\begin{frame}
%\frametitle{Dimostrazione del Lemma 2 - Unicità}

%\pause
%\begin{exampleblock}{Lemma 2 - Dimostrazione di Borel-Weil}
%Ogni $G$-modulo semplice è isomorfo ad esattamente un $L(\chi)$ con $\chi \in X$ tale che $H^0(\chi) \neq 0$.
%\end{exampleblock}

%\pause
%\begin{exampleblock}{Lemma}
%Sia $\chi \in X$ tale che $H^0(\chi) \neq 0$, allora $L(\chi)^{U^+} = L(\chi)_{\chi}$ e $\dim(L(\chi)^{U^+}) = 1$.
%\end{exampleblock}

%\pause
%\begin{alertblock}{Dimostrazione Lemma 2 - Unicità}
%Se $L(\chi) \simeq L(\chi')$, allora $L(\chi)_{\chi} = L(\chi)^{U^+} \simeq L(\chi')^{U^+} = L(\chi')_{\chi'} \simeq L(\chi)_{\chi'}$,
%assurdo in quanto l'intersezione di spazi-peso distinti è banale.
%\end{alertblock}

%\end{frame}


\begin{frame}
\frametitle{Cocaratteri}

\pause
\begin{block}{Definizione - Cocaratteri}
Un cocarattere $\lambda$ di $T$ è un omomorfismo razionale da $k^*$ in $T$.
\end{block}

Sia $X^{\vee} = X^{\vee}(T)$ l'insieme dei cocaratteri.

\medskip

\pause
I cocaratteri di $T$ sono le funzioni
\[
	x \mapsto \diag(x^{a_1}, \dots, x^{a_n})
\]
con $(a_1, \dots, a_n) \in \Zb^n$.

\medskip

\pause
$X^{\vee}$ è un gruppo abeliano isomorfo a $\Zb^n$.

Siano $\theta_i(x) = \diag(1, \dots, 1, x, 1, \dots, 1)$ per $1 \le i \le n$.

$(\theta_1, \dots, \theta_n)$ costituiscono una base dei cocaratteri.


\end{frame}


\begin{frame}
\frametitle{Accoppiamento}

\pause
$X^{\vee}$ è naturalmente isomorfo a $\Hom(X, \Zb)$.

\pause
\begin{block}{Definizione - Accoppiamento}
Sia $\langle \cdot, \cdot \rangle : X \times X^{\vee} \to \Zb$
l'accoppiamento dato dalla dualità di $X$ e $X^{\vee}$.
\end{block}

\pause
Siano $\chi = a_1 \epsilon_1 + \dots + a_n \epsilon_n$ e $\lambda = b_1 \theta_1 + \dots + b_n \theta_n$, allora
\[
	\langle \chi, \lambda \rangle = a_1 b_1 + \dots + a_n b_n
\]

Dati $\chi \in X$, $\lambda \in X^{\vee}$, si ha $\chi \circ \lambda(x) = x^{\langle \chi, \lambda \rangle}$.

\end{frame}


\begin{frame}
\frametitle{Pesi e Spazi-Peso}

\pause
\begin{exampleblock}{Teorema - Decomposizione di $T$-Moduli}
Ogni $T$-modulo $M$ si può esprimere come somma diretta di $T$-moduli di dimensione $1$.
\end{exampleblock}

\pause
Ogni $G$-modulo è un $T$-modulo in modo naturale.

\pause
L'azione di $T$ su uno spazio vettoriale di dimensione $1$ è data dalla
moltiplicazione per un carattere:
\[
	g.v \mapsto \chi(g)v
\]

\medskip

\pause
Sia $M_{\chi}$ il sottospazio di $M$ sul quale $T$ agisce moltiplicativamente
attraverso il carattere $\chi$.

\pause
\begin{block}{Definizione - Pesi e Spazi-Peso}
Dato un $T$-modulo $M$, gli spazi-peso di $M$ sono i sottospazi $M_{\chi}$ non nulli
e i pesi di $M$ sono i caratteri $\chi$ relativi agli spazi-peso.
\end{block}

\end{frame}


\begin{frame}
\frametitle{Radici}

\pause
Sia $\gf$ l'algebra di Lie di $G$.
Possiamo definire una struttura di $G$-modulo su $\gf$ attraverso la rappresentazione aggiunta $\Ad$.

\pause
\begin{block}{Definizione - Radici}
Le radici di $G$ rispetto $T$ sono i pesi non nulli della rappresentazione $\Ad$ di $T$ su $\gf$.
\end{block}

Sia $R$ l'insieme delle radici.

\medskip

\pause
L'algebra di Lie $\gf$ di $G$ è $\glf_n(k)$ e la rappresentazione aggiunta è data dal coniugio, quindi
\[
	R = \{\epsilon_i - \epsilon_j \mid 1 \le i,j \le n, i \neq j \}
\]
\medskip

Lo spazio-peso associato alla radice $\epsilon_i - \epsilon_j$ è $k e_{ij}$,
dove $e_{ij}$ è la matrice nulla ovunque tranne per un $1$ nella riga $i$ e colonna $j$.

\end{frame}


\begin{frame}
\frametitle{Radici positive e ordinamento}

\pause
\begin{block}{Definizione - Sottogruppo di Borel}
Il sottogruppo di Borel $B^+$ di $G$ è il sottogruppo di $G$ costituito dalle matrici triangolari superiori.
\end{block}

\pause
\begin{block}{Definizione - Radici Positive}
Le radici positive sono i pesi non nulli associati alla rappresentazione $\Ad$ sull'algebra di Lie di $B^+$.
\end{block}

Sia $R^+$ l'insieme delle radici positive.

\medskip

\pause
Le radici positive di $G$ sono
\[
	R^+ = \{\epsilon_i - \epsilon_j \mid 1 \le i < j \le n \}
\]

\pause
Definiamo un ordinamento parziale su $X$:
\[
	\chi_1 \ge \chi_2 \iff \exists (a_1, \dots, a_n) \in \Nb^n : \quad \chi_1 - \chi_2 = \sum_{\alpha_j \in R^+} a_j \alpha_j
\]


\end{frame}


\begin{frame}
\frametitle{Coradici}

\pause
\begin{block}{Definizione - Coradici}
Per ogni radice $\alpha = \epsilon_i - \epsilon_j \in R$ sia $\alpha^{\vee} = \theta_i - \theta_j \in X^{\vee}$.
I cocaratteri $\alpha^{\vee}$ così ottenuti si dicono coradici e costituiscono l'insieme $R^{\vee}$.
\end{block}

\pause
\begin{block}{Definizione - Riflessione}
Per ogni radice $\alpha$ sia $s_{\alpha} : X \to X$ la funzione definita da $s_{\alpha}(x) = x - \langle x, \alpha^{\vee} \rangle \alpha$.
$s_{\alpha}$ si dice riflessione.
\end{block}

\pause
Si noti che $\langle \alpha, \alpha^{\vee} \rangle = 2$ e $s_{\alpha}(\alpha) = -\alpha$.

\medskip

\pause
Per ogni $\alpha = \epsilon_i - \epsilon_j$ sia $\dot{s_{\alpha}} = \Id - e_{ii} - e_{jj} + e_{ij} - e_{ji}$,
cioè restringendo $\dot{s_{\alpha}}$ ai sottospazi $i$-esimo e $j$-esimo
$\dot{s_{\alpha}}|_{ij} = \left( \begin{smallmatrix} 0 & 1 \\ -1 & 0 \end{smallmatrix} \right)$

$\dot{s_{\alpha}} \in N_G(T)$, quindi agisce sui caratteri permutandoli.
Si verifica che si può restringere quest'azione alle radici, ottenendo $s_{\alpha}$.


\pause
\begin{block}{Definizione alternativa - Pesi Dominanti}
Un carattere $\chi \in X$ è un peso dominante se $\langle \chi, \alpha^{\vee} \rangle \ge 0$ per ogni $\alpha \in R^+$.
\end{block}

\end{frame}


\begin{frame}
\frametitle{Dimostrazione del Lemma 3 - $H^0(\chi) \neq 0 \Longrightarrow \chi \in X_+$}

\pause
\begin{exampleblock}{Lemma 3 - Dimostrazione di Borel-Weil}
Per ogni $\chi \in X$, $H^0(\chi) \neq 0$ se e solo se $\chi$ è dominante.
\end{exampleblock}

\pause
\begin{exampleblock}{Lemma}
Sia $\chi \in X$ tale che $H^0(\chi) \neq 0$, allora $\chi$ è un peso di $H^0(\chi)$
e ogni peso $\tau$ di $H^0(\chi)$ soddisfa $\tau \le \chi$.
\end{exampleblock}

\pause
\begin{alertblock}{Dimostrazione Lemma 3 - $H^0(\chi) \neq 0 \Longrightarrow \chi \in X_+$}
\begin{itemize}
	\pause
	\item Data $\alpha \in R^+$ e preso un rappresentante $\dot{s_{\alpha}} \in N_G(T)$ di $s_{\alpha}$, si verifica che
		$\dot{s_{\alpha}}.(H^0(\chi))_{\chi} \subset (H^0(\chi))_{s_{\alpha}.\chi}$.
	\pause
	\item $s_{\alpha}.\chi$ è un peso di $H^0(\chi)$, quindi $s_{\alpha}.\chi \le \chi$.
	\pause
	\item $0 \le \chi - s_{\alpha}.\chi = \langle \chi, \alpha^{\vee} \rangle \alpha$,
		perciò $\langle \chi, \alpha^{\vee} \rangle \ge 0$.
\end{itemize}
\end{alertblock}

\pause
Per mostrare l'altra implicazione si procede costruendo una funzione regolare non banale
appartenente a $H^0(\chi)$.

Prima però sono necessari alcuni risultati aggiuntivi.

\end{frame}


\begin{frame}
\frametitle{Radici semplici e gruppo di Weyl}

\pause
\begin{block}{Definizione - Radici Semplici}
Le radici semplici sono gli elementi minimali di $R^+$.
\end{block}

Sia $D$ l'insieme delle radici semplici.

\medskip

\pause
Le radici semplici di $G$ sono
\[
	D = \{\epsilon_i - \epsilon_{i+1} \mid 1 \le i < n \}
\]

\pause
Le riflessioni semplici sono le riflessioni associate alle radici semplici,
cioè $s_{\alpha}$ con $\alpha \in D$.

\pause
\begin{block}{Definizione - Gruppo di Weyl}
Il gruppo di Weyl $W$ di $G$ è $W = N_G(T) / Z_G(T)$.
\end{block}

\pause
$W$ agisce fedelmente su $R$, quindi si può identificare con un sottogruppo delle permutazioni di $R$.
Si verifica inoltre che $W \simeq S_n$, in particolare $s_{\alpha} \in W$ per $\alpha \in R$.

\pause
Dato un elemento $w \in W$, indichiamo con $\dot{w}$ un suo rappresentante in $N_G(T)$.

\end{frame}


\begin{frame}
\frametitle{Decomposizione di Bruhat}

\pause
Sia $U^+$ la parte unipotente di $B^+$, cioè il sottogruppo di $B^+$ costituito dalle matrici triangolari superiori
con le entrate della diagonale uguali ad $1$.

\pause
\begin{exampleblock}{Lemma - Decomposizione di Bruhat}
$G$ è unione disgiunta delle classi laterali doppie $U^+ \dot{w} B^-$ per $w \in W$.
\end{exampleblock}

\pause
\begin{exampleblock}{Lemma - Codimensione delle classi laterali doppie}
La classe laterale doppia $U^+ B^-$ corrispondente a $w = \Id$ è un aperto denso di $G$ e ha codimensione $0$.
Le classi laterali doppie $U^+ \dot{s_{\alpha}} B^-$ corrispondenti alle riflessioni semplici hanno codimensione $1$.
Le altre classi laterali doppie hanno codimensione maggiore di $1$.
\end{exampleblock}

\pause
\begin{exampleblock}{Teorema - Estensione di Hartogs}
Se $K$ è un sottoinsieme aperto di $G$ il cui complementare ha codimensione maggiore di $1$,
allora ogni funzione regolare su $K$ si estende a una funzione regolare su $G$.
\end{exampleblock}

\end{frame}


\begin{frame}
\frametitle{Dimostrazione del Lemma 3 - $H^0(\chi) \neq 0 \Longleftarrow \chi \in X_+$}

\pause
\begin{exampleblock}{Lemma 3 - Dimostrazione di Borel-Weil}
Per ogni $\chi \in X$, $H^0(\chi) \neq 0$ se e solo se $\chi$ è dominante.
\end{exampleblock}

\pause
Per ogni $\alpha = \epsilon_i - \epsilon_j \in R$ siano $U_{\alpha} = \Id + k e_{ij}$
e $u_{\alpha} : k \to U_{\alpha}$ con $u_{\alpha}(x) = \Id + x e_{ij}$.
Sia inoltre $U_{\alpha}^+ = \prod_{\beta>0, \beta \neq \alpha} U_{\beta}$.

\pause
\begin{alertblock}{Dimostrazione Lemma 3 - $H^0(\chi) \neq 0 \Longleftarrow \chi \in X_+$}
\begin{itemize}
	\pause
	\item Poniamo $f_{\chi} : U^+ T U^-$ con $f_{\chi}(u_1tu_2) = \chi(t)^{-1}$.
	\pause
	\item $f \in H^0(\chi)$ se e solo se $f(gb) = \chi(b)^{-1}f(g)$ ($g \in G, b \in B^-$),
		quindi è sufficiente estendere $f_{\chi}$ su $G$ affinché $H^0(\chi) \neq 0$.
	\pause
	\item Per il teorema di Hartogs è sufficiente estendere $f_{\chi}$ su $U^+ \dot{s_{\alpha}} B^-$, ma
		$U^+ \dot{s_{\alpha}} B^- \subset
		\dot{s_{\alpha}} U^+ B^- =
		U_{\alpha}^+ \dot{s_{\alpha}} U_{\alpha} T U^- \simeq
		U_{\alpha}^+ \times k \times T \times U^-$
		per ogni $\alpha \in D$.
	\pause
	\item Si verifica che $U^+ B^- \cap \dot{s_{\alpha}} U^+ B^- \simeq U_{\alpha}^+ \times k^* \times T \times U^-$
		e che per $u_1 \in U_{\alpha}^+, x \in k^*, t \in T, u \in U^-$ si ha
		$f_{\chi}(u_1 \dot{s_{\alpha}} u_{\alpha}(x) t u) = \chi(t)^{-1} (-x)^{\langle \chi, \alpha^{\vee} \rangle}$.
	\pause
	\item Ci basta estendere $f_{\chi}$ in $x=0$, possibile in quanto $\langle \chi, \alpha^{\vee} \rangle \ge 0$.
\end{itemize}
\end{alertblock}

\end{frame}


\begin{frame}
\frametitle{Rappresentanti alternativi per i $G$-moduli semplici}

\pause
Si consideri l'azione di $B^-$ su $G \times k$ data da $b.(g,x) = (gb^{-1}, \chi(b)x)$ e sia
$E_{\chi} = G \times_B^- k = \frac{G \times k}{B^-}$.
Sia $\pi : E_{\chi} \to G/B^-$ la proiezione sulla prima componente.

\medskip

\pause
$\pi$ è un fibrato vettoriale da $E_{\chi}$ su $G/B^-$ di rango $1$.

\medskip

\pause
$E_{\chi}$ è un $G$-modulo con l'azione data dalla traslazione sinistra sulla prima componente.
Considerando una struttura di $G$-modulo su $G/B^-$ mediante la traslazione sinistra,
$\pi$ è una mappa equivariante.

\pause
\begin{block}{Definizione - $\Gamma(G/B^-, E_{\chi})$}
Sia $\Gamma(G/B^-, E_{\chi})$ l'insieme delle sezioni del fibrato $E_{\chi}$, cioè
\[
	\Gamma(G/B^-, E_{\chi}) = \{ \sigma : G/B^- \to E_{\chi} \mid \pi \circ \sigma = \Id \}
\]
\end{block}

\pause
$\Gamma(G/B^-, E_{\chi})$ ha una struttura di spazio vettoriale.

\medskip

\pause
$\Gamma(G/B^-, E_{\chi})$ è un $G$-modulo mediante l'azione $(g.s)(hB^-) = g.s(g^{-1}hB^-)$.

\end{frame}


\begin{frame}
\frametitle{Rappresentanti alternativi per i $G$-moduli semplici}

\pause
Sia $\xi : H^0(\chi) \to \Gamma(G/B^-, E_{\chi})$ con $\xi(f)(gB^-) = [(g,f(g))]$.

\pause
\begin{exampleblock}{Teorema - $H^0(\chi) \simeq \Gamma(G/B^-, E_{\chi})$}
La funzione $\xi$ è un isomorfismo di $G$-moduli fra $H^0(\chi)$ e $\Gamma(G/B^-, E_{\chi})$.
\end{exampleblock}

\medskip

\pause
Sia $V_{\chi} = \soc_G \Gamma(G/B^-, E_{\chi})$.

\pause
\begin{exampleblock}{Teorema - Borel-Weil (formulazione alternativa)}
L’insieme $\{V_{\chi} \mid \chi \in X_+ \}$ fornisce un sistema di rappresentanti
per le classi di isomorfismo dei $G$-moduli semplici.
\end{exampleblock}

\end{frame}


\begin{frame}
\frametitle{$\GLb_2(\Cb)$-moduli}

\pause
Supponiamo $k = \Cb$ e $G=\GLb_2(\Cb)$.

\pause
In questo caso $H^0(\chi)$ è già semplice per $\chi \in X_+$, quindi coincide con $L(\chi)$.

\medskip

\pause
Sia $\Sym^a = \Sym^a(\Cb^2)$ il $G$-modulo costituito dai polinomi simmetrici di grado $a$ in due variabili.

\pause
\begin{exampleblock}{Lemma}
$L((a,0)) \simeq \Sym^a$, in particolare $\Sym^a$ è semplice.
\end{exampleblock}

\pause
Sia $\Cb_{(1,1)}$ il $G$-modulo dato dall'azione $G \times \Cb \to \Cb$, $g.x = \det(g)x$.

$\Cb_{(1,1)}$ è un $G$-modulo semplice con peso associato $(1,1)$, quindi $L((1,1)) \simeq \Cb_{(1,1)}$.

\pause
\begin{exampleblock}{Lemma}
$L((1,1)) \otimes L((a, b)) \simeq L((1+a,1+b))$.
\end{exampleblock}

\pause
Concludiamo
\[
	L((a,b)) \simeq
	L((a-b,0)) \otimes L((1,1))^{\otimes b} \simeq
	\Sym^{a-b} \otimes \Cb_{(1,1)}^{\otimes b}
\]

\end{frame}


\begin{frame}
\frametitle{Bibliografia}

\nocite{*}
\printbibliography[heading=bibintoc]

\end{frame}


\end{document}
